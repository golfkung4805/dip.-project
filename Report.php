<?php
    if (!isset($_SESSION)) session_start();
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Report</title>
    <!-- Favicon -->
    <link rel="icon" href="dist/img/AdminLTELogo.png" type="image/x-icon">
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Font Awesome Icons -->
    <style>
        .activity {
            background-color: #C6EEC3;
        }

        .danger {
            background-color: #ff6a6a;
        }

        table {
            border-collapse: collapse;
            text-align: center;
            vertical-align: middle !important;
        }

        .report-schedule, .report-schedule th, .report-schedule td ,
        .report-detail-subject, .report-detail-subject th, .report-detail-subject td {
            border: 1px solid black;
        }

        .report-schedule, .report-schedule th {
            padding: 5px;
        }

        .report-schedule, .report-schedule td {
            height: 61px;
        }

        .report-detail-subject td{
            padding: 0px;
        }

        @media only screen and (min-width: 768px) {
            .print-schedule {
                bottom: 5.5rem;
                right: 2rem;
            }
        }

        @media print {
            @page {
                size: landscape;
                margin-top: 0cm;
                margin-right: .5cm;
                margin-bottom: 0;
                margin-left: .5cm;
                
            }

            body { margin-top: .5cm; font-size: 16px;}

            /* .schedule-select {
                display: none;
            }

            .schedule {
                display: none;
            }

            .print-schedule {
                display: none;
            }

            .main-footer {
                display: none;
            } */
        }

        /* .print-schedule {
            bottom: 1.25rem;
            position: fixed;
            right: 1.25rem;
            z-index: 1032;
        } */
    </style>
</head>

<body>
    <div class="container-fluid">

    </div>
    

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Cookie -->
    <script src="dist/js/js.cookie.min.js"></script>
    <script>
        $(document).ready(function () {
            group_code = null;
            room_code = null;
            teacher_code = null;
            select_year = null;

            if(Cookies.get('staReport') === "StudentSchedule"){
                urls = "ReportScheduleStudent.php";
                group_code = Cookies.get('STDCK_group_code');
                select_year = Cookies.get('STDCK_term');
            }else if(Cookies.get('staReport') === "TeacherSchedule"){
                urls = "ReportScheduleTeacher.php";
                teacher_code = Cookies.get('TeachCK_teach_code');
                select_year = Cookies.get('TeachCK_term');
            }else if(Cookies.get('staReport') === "RoomSchedule"){
                urls = "ReportScheduleRoom.php";
                room_code = Cookies.get('RoomCK_room_code');
                select_year = Cookies.get('RoomCK_term');
            }
            $.ajax({
                type: "POST",
                url: urls,
                data:  {    group_code : group_code,
                            room_code : room_code,
                            teacher_code : teacher_code,
                            term: select_year  },
                success: function (ReportSchedule) {
                    $(".container-fluid").html(ReportSchedule);
                    window.print();
                    Cookies.remove('staReport');
                    window.close();
                },
                error: function(error) {
                    console.error(error.responseText);
                    Cookies.remove('staReport');
                    window.close();
                }
            });
        });
    </script>
</body>
    

</html>