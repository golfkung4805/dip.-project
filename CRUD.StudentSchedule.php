<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    if($action==="GetDepartment"){
        $sql = "SELECT * FROM tb_dept 
                ORDER BY dept_name";
        $stmt = $pdo->query($sql);
        $GetDepartment[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['dept_code']===$_POST['dept_code']){
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name'] , "selected"=>true); 
            }else{
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name']); 
            }
            
        }
        $stmt = null;
        
        echo json_encode($GetDepartment,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetGroup"){
        $dept_code = $_POST['dept_code'];
        $sql = "SELECT * FROM tb_study_group 
                WHERE dept_code = :dept_code
                ORDER BY group_code";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('dept_code',$dept_code,PDO::PARAM_STR);
        $stmt->execute();

        $GetGroup[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['group_code']===$_POST['group_code']){
                $GetGroup[] = array("id"=>$row['group_code'], "text"=>$row['group_code']." | ".$row['group_subname'] , "selected"=>true); 
            }else{
                $GetGroup[] = array("id"=>$row['group_code'], "text"=>$row['group_code']." | ".$row['group_subname']); 
            }
            
        }
        
        $stmt = null;
        
        echo json_encode($GetGroup,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetYear"){
        $Year = date("Y")+543;

        $NowYear = substr($Year,0,2);

        $group_code = $_POST['group_code'];
        $select_year = $_POST['select_year'];
        $subYear = intval(substr($group_code,0,2));

        $subClass = substr($group_code,2,1);

        if($subClass==="2"){
            $loop = 3; // ปวช เรียน 3 ปี
        }else if($subClass==="3"){
            $loop = 2; // ปวช เรียน 2 ปี
        }
        $Output[] = array("id"=>"", "text"=>""); 
        for($i=1;$i<=$loop;$i++){
            if($subYear<10){
                $returns = "1/".$NowYear."0".$subYear;
                if($select_year===$returns){
                    $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                }else{
                    $Output[] = array("id"=>$returns, "text"=>$returns); 
                }
                $returns = "2/".$NowYear."0".$subYear;
                if($select_year===$returns){
                    $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                }else{
                    $Output[] = array("id"=>$returns, "text"=>$returns); 
                }

                if($i === 1 && $loop === 2){
                    $returns = "S/".$NowYear."0".$subYear;
                    if($select_year===$returns){
                        $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                    }else{
                        $Output[] = array("id"=>$returns, "text"=>$returns); 
                    } 
                }
            }else{
                $returns = "1/".$NowYear.$subYear;
                if($select_year===$returns){
                    $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                }else{
                    $Output[] = array("id"=>$returns, "text"=>$returns); 
                } 

                $returns = "2/".$NowYear.$subYear;
                if($select_year===$returns){
                    $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                }else{
                    $Output[] = array("id"=>$returns, "text"=>$returns); 
                } 

                if($i === 1 && $loop === 2){
                    $returns = "S/".$NowYear.$subYear;
                    if($select_year===$returns){
                        $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                    }else{
                        $Output[] = array("id"=>$returns, "text"=>$returns); 
                    } 
                }
                
            }

            $subYear++;
            if($subYear>99){
                $subYear = intval(substr($subYear,-2));
                $NowYear++;
            }
        }

        echo json_encode($Output,JSON_UNESCAPED_UNICODE);
        exit;
    }
?>