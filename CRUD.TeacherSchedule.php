<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    if($action==="GetDepartment"){
        $sql = "SELECT * FROM tb_dept 
                ORDER BY dept_name";
        $stmt = $pdo->query($sql);
        $GetDepartment[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['dept_code']===$_POST['dept_code']){
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name'] , "selected"=>true); 
            }else{
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name']); 
            }
            
        }
        $stmt = null;
        
        echo json_encode($GetDepartment,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetTeach"){
        $dept_code = $_POST['dept_code'];
        $sql = "SELECT * FROM tb_teacher 
                WHERE dept_code = :dept_code";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('dept_code',$dept_code,PDO::PARAM_STR);
        $stmt->execute();

        $GetTeach[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['teacher_code']===$_POST['teach_code']){
                $GetTeach[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name'], "selected"=>true); 
            }else{
                $GetTeach[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name']); 
            }
            
        }
        
        $stmt = null;
        
        echo json_encode($GetTeach,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetYear"){
        $sql = "SELECT term 
                FROM tb_schedule 
                GROUP BY term";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $Output[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['term']===$_POST['select_year']){
                $Output[] = array("id"=>$row['term'], "text"=>$row['term'], "selected"=>true); 
            }else{
                $Output[] = array("id"=>$row['term'], "text"=>$row['term']); 
            }
            
        }
        
        $stmt = null;

        echo json_encode($Output,JSON_UNESCAPED_UNICODE);
        exit;
    }
?>