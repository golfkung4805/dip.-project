    <div class="modal fade" id="modalTutorial">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">วิธีการใช้งานหน้าหลัก</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>วิธีใช้บล๊าๆๆ&hellip;</p>
                <p>วิธีใช้บล๊าๆๆ&hellip;</p>
                <p>วิธีใช้บล๊าๆๆ&hellip;</p>
                <p>วิธีใช้บล๊าๆๆ&hellip;</p>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default bg-maroon btn-never-show-tutorial">ปิดและไม่ต้องแสดงอีก <i class="fa fa-window-close"></i></button> -->
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด <i class="fa fa-times"></i></button>
            </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> หน้าแรก </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active">หน้าแรก</li>
                            <li class="breadcrumb-item"></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-purple">
                            <div class="card-header">
                                <h3 class="card-title">จัดตารางเรียน</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col"></div>
                                    <div class="col-md-3">
                                        <div class="card card-outline card-primary">
                                            <div class="card-header card-menu-home">
                                                <a href="?view=StudentSchedule">
                                                    <i class="fas fa-calendar-alt fa-3x"></i>
                                                    <h3 class="card-title">  ดูตารางเรียนนักศึกษา  </h3>
                                                </a>
                                            </div>
                                            <!-- /.card-header -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                    <!-- /.col-md-3 -->
                                    <div class="col-md-3">
                                        <div class="card card-outline card-primary">
                                            <div class="card-header card-menu-home">
                                                <a href="?view=TeacherSchedule">
                                                    <i class="fas fa-calendar-alt fa-3x"></i>
                                                    <h3 class="card-title">  ดูตารางสอนอาจารย์  </h3>
                                                </a>
                                            </div>
                                            <!-- /.card-header -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                    <!-- /.col-md-3 -->
                                    <div class="col-md-3">
                                        <div class="card card-outline card-primary">
                                            <div class="card-header card-menu-home">
                                                <a href="?view=RoomSchedule">
                                                    <i class="fas fa-calendar-alt fa-3x"></i>
                                                    <h3 class="card-title">  ดูตารางการใช้ห้อง  </h3>
                                                </a>
                                            </div>
                                            <!-- /.card-header -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                    <!-- /.col-md-3 -->
                                    <div class="col"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col-md-12 -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>