<?php
    if (!isset($_SESSION)) session_start();

    if(!empty($_SESSION['Role']) && $view !== "ForgotPassword"){
        echo "<script>window.location=\"http://localhost/SR/admin/\"</script>";
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> <?php echo $Title; ?> </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="icon" href="dist/img/AdminLTELogo.png" type="image/x-icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,400i,700&display=swap" rel="stylesheet">
    <!-- SweetAlert2 -->
    <!-- <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.css">
    <!-- pace-progress -->
    <link rel="stylesheet" href="plugins/pace-progress/themes/black/pace-theme-flat-top.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
    <!-- My Style CSS -->
    <!-- <link rel="stylesheet" href="dist/css/SR.CSS.css"> -->
    <link rel="stylesheet" href="dist/css/ScrollBar.css">
    <?php include("dist/css/SR.CSS.php"); ?>

    <style>
        .swal2-popup.swal2-toast .swal2-title{
            font-size:1.5em
        }
    </style>
</head>

<body class="hold-transition login-page pace-primary" style="font-family: 'Kanit', sans-serif;">
    <?php if($view === "Error401"){ ?>
        <?php include( $MainContent ); ?>
    <?php }else{ ?>
    <div class="login-box">
        <div class="login-logo">
            <!-- <a href="<?php echo $_SERVER_NAME; ?>"><b>Admin</b>MEC@SRMS</a> -->
        </div>
        <!-- /.login-logo -->
        <?php include( $MainContent ); ?>
    </div>
    <?php } ?>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Cookie -->
    <script src="dist/js/js.cookie.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- pace-progress -->
    <script src="plugins/pace-progress/pace.min.js"></script>
    <!-- SweetAlert2 -->
    <script src="plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="plugins/toastr/toastr.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>

    <!-- My Style JS -->
    <!-- <script src="dist/js/SR.JS.js"></script> -->
    <?php include("dist/js/SR.JS.php"); ?>
</body>

</html>