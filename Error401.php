    
    <div class="error-page">
        <h2 class="headline text-warning"> 401</h2>

        <div class="error-content">
            <h3><i class="fas fa-exclamation-triangle text-warning"></i> อุ๊ปส์! ข้อผิดพลาดที่ไม่ได้รับอนุญาต.</h3>

            <p class="m-0">คุณไม่ได้รับอนุญาตให้เข้าถึงหน้านี้ </p>
            <a href="<?php echo $_SERVER_NAME; ?>">return to homepage.</a> or try <a href="<?php echo $_SERVER_NAME; ?>?view=Login">logging in.</a>

        </div>
        <!-- /.error-content -->
    </div>
