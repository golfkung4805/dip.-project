    <div class="card">
        <div class="card-body login-card-body">
            <img src="dist/img/logo.png"width="80" class="mx-auto d-block">
            <p class="login-box-msg">ขั้นตอนสุดท้ายของการเปลี่ยนรหัสผ่าน, <br>เปลี่ยนรหัสผ่านของคุณตอนนี้.</p>

            <form id="FormRecoverPassword">
                <div class="input-group mb-3">
                    <input type="text" name="username" id="username" class="form-control" placeholder="รหัสอาจารย์" readonly>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="text" name="name" id="name" class="form-control" placeholder="รหัสอาจารย์" readonly>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-address-book"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" id="new_password" name="new_password" class="form-control" placeholder="รหัสผ่านใหม่">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="ยืนยันรหัสผ่าน">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" id="token" name="token">
                        <button type="submit" class="btn btn-primary btn-block">เปลี่ยนรหัสผ่าน</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <p class="mt-3 mb-1">
                <a href="<?php echo $_SERVER_NAME; ?>?view=Login">เข้าสู่ระบบ</a>
            </p>
            <p class="mb-1">
                <a class="btn btn-light btn-block" href="<?php echo $_SERVER_NAME; ?>">กลับหน้าหลัก</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>