    <div class="card">
        <div class="card-body login-card-body">
            <img src="dist/img/logo.png"width="80" class="mx-auto d-block">
            <p class="login-box-msg">คุณลืมรหัสผ่าน ?<br>คุณสามารถขอรหัสผ่านใหม่ได้ที่นี่</p>

            <form id="FormForgotPassword">
                <div class="input-group mb-3">
                    <input type="text" name="username" id="username" class="form-control" placeholder="รหัสอาจารย์">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="text" name="tomail" id="tomail" class="form-control" placeholder="อีเมล">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">ขอรหัสผ่านใหม่ </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <p class="mt-3 mb-1">
                <a href="<?php echo $_SERVER_NAME; ?>?view=Login">เข้าสู่ระบบ</a>
            </p>
            <p class="mb-0">
                <!-- <a href="register.html" class="text-center">Register a new membership</a> -->
            </p>
            <p class="mb-1">
                <a class="btn btn-light btn-block" href="<?php echo $_SERVER_NAME; ?>">กลับหน้าหลัก</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>