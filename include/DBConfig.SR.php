<?php
    if (!isset($_SESSION)) session_start();
    
    if($_SERVER['SERVER_NAME'] === ""){
        $server = "";
        $username = "";
        $password = "";
        $db = "";
    }else{
        $server = "localhost";
        $username = "root";
        $password = "";
        $db = "sr_system";
    }
    
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4",
        PDO::ATTR_EMULATE_PREPARES => FALSE
    ];
    try{
        $pdo = new PDO("mysql:host=$server;dbname=$db", $username, $password, $options);
    }catch(PDOException $e){
        echo $e->getMessage();
    }
?>