
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> ข้อมูลแผนก </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?php echo $_SERVER_NAME; ?>">หน้าหลัก</a></li>
                                <li class="breadcrumb-item active"> ข้อมูลแผนก </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Modal -->
            <div class="modal fade" id="modalDepartment">
                <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content">
                        <form id="modalFormDepartment">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modal-title">เพิ่มแผนก</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>รหัสแผนก</label>
                                            <input type="text" class="form-control" id="code_dep" name="code_dep" placeholder="กรอกรหัสแผนก ...">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>ชื่อแผนก</label>
                                            <input type="text" class="form-control" id="name_dep" name="name_dep" placeholder="กรอกชื่อแผนก ...">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>หัวหน้าแผนก</label>
                                            <select class="form-control select-chief-department" id="chief_dep" name="chief_dep" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <input type="hidden" id="id_dep" name="id_dep">
                                <input type="hidden" id="action" name="action" value="Insert">
                                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                                <button type="submit" class="btn btn-primary btn-submit">บันทึกข้อมูลแผนก</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /. Modal -->
            
            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <table id="TableDepartment" class="TableDepartment table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ลำดับ</th>
                                                <th class="text-center">รหัสแผนก</th>
                                                <th>ชื่อแผนก</th>
                                                <th>ชื่อหัวหน้าแผนก</th>
                                                <th class="text-center">ตัวเลือก</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" height="70"></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="text-center">ลำดับ</th>
                                                <th class="text-center">รหัสแผนก</th>
                                                <th>ชื่อแผนก</th>
                                                <th>ชื่อหัวหน้าแผนก</th>
                                                <th class="text-center">ตัวเลือก</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col-md-12-->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
