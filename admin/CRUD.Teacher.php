<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    // $code_teach = "a1a";
    // $prefix_teach = "a1";
    // $first_name_teach = "a1";
    // $last_name_teach = "a1";
    // $department_teach = "a1";

    // $sql = "INSERT INTO tb_teacher (teacher_code, gender, first_name, last_name, department) VALUE(:code_teach, :prefix_teach, :first_name_teach, :last_name_teach, :department_teach)";
    // $stmt = $pdo->prepare($sql);
    // $stmt->bindValue('code_teach',$code_teach,PDO::PARAM_STR);
    // $stmt->bindValue('prefix_teach',$prefix_teach,PDO::PARAM_STR);
    // $stmt->bindValue('first_name_teach',$first_name_teach,PDO::PARAM_STR);
    // $stmt->bindValue('last_name_teach',$last_name_teach,PDO::PARAM_STR);
    // $stmt->bindValue('department_teach',$department_teach,PDO::PARAM_STR);
    // $stmt->execute();
    

    function loadTableTeacher($dept){
        // $con=mysqli_connect('localhost','root','','db_testing');
        global $pdo;

        $request=$_POST;
        $col =array(
            0   =>  'teacher_id',
            1   =>  'teacher_codee',
            2   =>  'gender-first_name-last_name',
            3   =>  'dept_name',
            4   =>  'teacher_id'
        );  //create column LIKE table in database

        //Search
        $sql = "SELECT *,tb_teacher.teacher_code as teacher_codee FROM tb_teacher 
                LEFT JOIN tb_dept ON tb_teacher.dept_code = tb_dept.dept_code
                WHERE 1=1 ";
        if($dept == "NULL"){
            $sql .= "AND tb_teacher.dept_code IS NULL";
        }   
        if($dept != "ALL" AND $dept != "NULL"){
            $sql .= "AND tb_teacher.dept_code = '$dept' ";
        }
        if(!empty($request['search']['value'])){
            $sql.=" AND (teacher_id LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR tb_teacher.teacher_code LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR CONCAT(gender, ' ' ,first_name, ' ' ,last_name) LIKE '%".$request['search']['value']."%'";
            $sql.=" OR dept_name LIKE '%".$request['search']['value']."%' )";
            
        }
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $totalData = $stmt->rowCount();
        $totalFilter=$totalData;
        $stmt = null;
        //Order
        $sql.=" ORDER BY ".$col[$request['order'][0]['column']]."   ".$request['order'][0]['dir']."  LIMIT ".
            $request['start']."  ,".$request['length']."  ";

        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $data=array();
        $count = $request['start']+1;
        foreach($stmt as $row){
            
            $subdata=array();
            // $subdata[]=$row['id']; //id
            $subdata[]="<div class=\"text-center\">".$count."</div>";
            $subdata[]="<div class=\"text-center\">".$row['teacher_codee']."</div>"; //teacher_code
            $subdata[]="<div>".$row['gender']." ".$row['first_name']." ".$row['last_name']."</div>"; //first_name- last_name-department               $row[0] is id in table on database
            $subdata[]="<div class=\"text-left\">".$row['dept_name']."</div>"; //gender
            // $subdata[]="<div class=\"text-center\"><button class=\"btn btn-outline-warning btn-update mr-3\" data-id=\"$row[teacher_id]\">แก้ไข</button> <button class=\"btn btn-outline-danger btn-delete\" data-id=\"$row[teacher_id]\">ลบ</button></div>";
            if($_SESSION['dept_code'] == $row['dept_code'] || $_SESSION['Role'] == "ADMIN"){
                $subdata[]="<div class=\"text-center\"><button class=\"btn btn-outline-warning btn-update mr-3\" data-id=\"$row[teacher_id]\">แก้ไข</button> <button class=\"btn btn-outline-danger btn-delete\" data-id=\"$row[teacher_id]\">ลบ</button></div>";
            }else{
                $subdata[]="<div class=\"text-center\"><button class=\"btn btn-outline-warning btn-update mr-3\" data-id=\"$row[teacher_id]\">แก้ไข</button> </div>";
            }
            $data[]=$subdata;
            $count++;
        }

        $json_data=array(
            "draw"              =>  intval($request['draw']),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalFilter),
            "data"              =>  $data
        );
        return $json_data;
    }

    if($action==="LoadTableTeacher"){
        $dataTable = loadTableTeacher($_POST['dept']);
        echo json_encode($dataTable,JSON_UNESCAPED_UNICODE ); 
        exit;
    }

    if($action==="getDetailTeacher"){
        $teacher_id = filter_input(INPUT_POST,'teacher_id');
        $sql = "SELECT * FROM tb_teacher 
                WHERE teacher_id = :teacher_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':teacher_id',$teacher_id,PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt = null;

        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetDepartmentAll"){
        $dept_code = "";
        if(isset($_POST['dept_code'])){
            $dept_code = $_POST['dept_code'];
        }

        $sql = "SELECT * FROM tb_dept 
                ORDER BY dept_name";
        $stmt = $pdo->query($sql);
        $GetDepartment[] = array("id"=>"", "text"=>""); 
        $GetDepartment[] = array("id"=>"ALL", "text"=>"แสดงทั้งหมด"); 
        $GetDepartment[] = array("id"=>"NULL", "text"=>"แสดงอาจารย์ที่ไม่มีแผนก"); 
        foreach($stmt as $row){
            if($row['dept_code']===$dept_code){
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name'], "selected"=>true); 
            }else{
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name']); 
            }
        }
        $stmt = null;

        echo json_encode($GetDepartment,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetDepartment"){
        $dept_code = "";
        if(isset($_POST['dept_code'])){
            $dept_code = $_POST['dept_code'];
        }

        $sql = "SELECT * FROM tb_dept 
                ORDER BY dept_name";
        $stmt = $pdo->query($sql);
        // $GetDepartment[] = array("id"=>"", "text"=>""); 
        $GetDepartment[] = array("id"=>"0", "text"=>"ไม่มีแผนก"); 
        foreach($stmt as $row){
            if($row['dept_code']===$dept_code){
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name'], "selected"=>true); 
            }else{
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name']); 
            }
        }
        $stmt = null;

        echo json_encode($GetDepartment,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetSelectDepartment"){
        $sql = "SELECT * FROM tb_dept 
                ORDER BY dept_name";
        $stmt = $pdo->query($sql);
        $GetDepartment[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name']); 
        }
        $stmt = null;
        
        echo json_encode($GetDepartment,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="Insert"){
        $code_teach = filter_input(INPUT_POST,'code_teach');
        $prefix_teach = filter_input(INPUT_POST,'prefix_teach');
        $first_name_teach = filter_input(INPUT_POST,'first_name_teach');
        $last_name_teach = filter_input(INPUT_POST,'last_name_teach');
        $dept_code = filter_input(INPUT_POST,'get_dept_code');
        if($dept_code=="0"){
            $dept_code = NULL;
        }
        $sql = "SELECT teacher_code 
                FROM tb_teacher 
                WHERE teacher_code = :teacher_code";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':teacher_code',$code_teach,PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $alert = array("type"=>"warning", "title"=>"รหัสอาจารย์ $code_teach มีในระบบแล้ว"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $name = $first_name_teach." ".$last_name_teach;
            $sql = "SELECT CONCAT(first_name,' ',last_name)as name FROM tb_teacher WHERE CONCAT(first_name,' ',last_name) = :name";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':name',$name,PDO::PARAM_STR);
            $stmt->execute();
            if($stmt->rowCount() > 0){
                $alert = array("type"=>"warning", "title"=>"ชื่ออาจารย์ ".$first_name_teach." ".$last_name_teach." มีในระบบแล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else{
                $sql = "INSERT INTO tb_teacher (teacher_code, gender, first_name, last_name, dept_code, teacher_password) 
                        VALUE(:code_teach, :prefix_teach, :first_name_teach, :last_name_teach, :dept_code, :passwd_teach)";
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue('code_teach',$code_teach,PDO::PARAM_STR);
                $stmt->bindValue('prefix_teach',$prefix_teach,PDO::PARAM_STR);
                $stmt->bindValue('first_name_teach',$first_name_teach,PDO::PARAM_STR);
                $stmt->bindValue('last_name_teach',$last_name_teach,PDO::PARAM_STR);
                $stmt->bindValue('dept_code',$dept_code,PDO::PARAM_STR);
                $stmt->bindValue('passwd_teach',$code_teach,PDO::PARAM_STR);
                $stmt->execute();
                $stmt = null;

                $alert = array("type"=>"success", "title"=>"เพิ่มชื่ออาจารย์สำเร็จ"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }
    }
    
    if($action==="Update"){
        // $code_teach = filter_input(INPUT_POST,'code_teach');
        $prefix_teach = filter_input(INPUT_POST,'prefix_teach');
        $first_name_teach = filter_input(INPUT_POST,'first_name_teach');
        $last_name_teach = filter_input(INPUT_POST,'last_name_teach');
        $id_teacher = filter_input(INPUT_POST,'id_teacher');

        $chk_dept_code = filter_input(INPUT_POST,'get_dept_code');
        if($chk_dept_code=="0"){
            $dept_code = NULL;
        }else{
            $dept_code = $chk_dept_code;
        }
        
        // $sql = "SELECT teacher_code,
        //         (SELECT count(teacher_code) FROM tb_teacher WHERE teacher_code = :teacher_code) as CheckCode
        //         FROM tb_teacher 
        //         WHERE teacher_id = :teacher_id";
        // $stmt = $pdo->prepare($sql);
        
        // $stmt->bindValue(':teacher_code',$code_teach,PDO::PARAM_STR);
        // $stmt->bindValue(':teacher_id',$id_teacher,PDO::PARAM_INT);
        // $stmt->execute();

        // if($stmt->rowCount() > 0){
        //     $row = $stmt->fetch();
            // if($row['teacher_code'] === $code_teach || $row['CheckCode'] <= 0){
                $name = $first_name_teach.' '.$last_name_teach;
                $sql = "SELECT CONCAT(first_name,' ',last_name)as name,
                            (SELECT count(CONCAT(first_name,' ',last_name)) FROM tb_teacher WHERE CONCAT(first_name,' ',last_name) = :name)as CheckName
                        FROM tb_teacher WHERE teacher_id = :id_teacher";
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue(':name',$name,PDO::PARAM_STR);
                $stmt->bindValue(':id_teacher',$id_teacher,PDO::PARAM_INT);
                $stmt->execute();
                $row = $stmt->fetch();
                if($stmt->rowCount() > 0){
                    if($row['name'] === $name || $row['CheckName'] <= 0){
                        // ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        // if($chk_dept_code == "0"){
                        //     $sql = "UPDATE tb_teacher SET   gender = :prefix_teach, 
                        //                                         first_name = :first_name_teach, 
                        //                                         last_name = :last_name_teach
                        //         WHERE teacher_id = :id_teacher";
                        // }else{
                            $sql = "UPDATE tb_teacher SET   gender = :prefix_teach, 
                                                        first_name = :first_name_teach, 
                                                        last_name = :last_name_teach,
                                                        dept_code = :dept_code 
                                WHERE teacher_id = :id_teacher";
                        // }
                        
                        $stmt = $pdo->prepare($sql);
                        // $stmt->bindValue('code_teach',$code_teach,PDO::PARAM_STR);
                        $stmt->bindValue('prefix_teach',$prefix_teach,PDO::PARAM_STR);
                        $stmt->bindValue('first_name_teach',$first_name_teach,PDO::PARAM_STR);
                        $stmt->bindValue('last_name_teach',$last_name_teach,PDO::PARAM_STR);
                        // if($chk_dept_code != "0"){
                            $stmt->bindValue('dept_code',$dept_code,PDO::PARAM_STR);
                        // }
                        $stmt->bindValue('id_teacher',$id_teacher,PDO::PARAM_INT);
                        $stmt->execute();
                        $stmt = null;

                        $alert = array("type"=>"success", "title"=>"แก้ไขชื่ออาจารย์สำเร็จ"); 
                        echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                        exit;
                        // ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    }else{
                        $alert = array("type"=>"warning", "title"=>"ชื่ออาจารย์ ".$first_name_teach." ".$last_name_teach." มีในระบบแล้ว"); 
                        echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                        exit;
                    }
                }else{
                    $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
                    echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                    exit;
                }
            // }else{
            //     $alert = array("type"=>"warning", "title"=>"รหัสอาจารย์ $code_teach มีในระบบแล้ว"); 
            //     echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            //     exit;
            // }
        // }else{
        //     $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
        //     echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
        //     exit;
        // }
    }

    if($action==="Delete"){
        $id_teacher = filter_input(INPUT_POST,'id_teacher');

        $sql = "SELECT * FROM tb_teacher 
                WHERE teacher_id = :id_teacher";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('id_teacher',$id_teacher,PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount()===0){
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $sql = "DELETE FROM tb_teacher 
                    WHERE teacher_id = :id_teacher";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('id_teacher',$id_teacher,PDO::PARAM_INT);
            $stmt->execute();
            $stmt = null;

            $alert = array("type"=>"success", "title"=>"ลบชื่ออาจารย์สำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }
?>