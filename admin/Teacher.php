
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> ข้อมูลอาจารย์ </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?php echo $_SERVER_NAME; ?>">หน้าหลัก</a></li>
                                <li class="breadcrumb-item active"> ข้อมูลอาจารย์ </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Modal -->
            <div class="modal fade" id="modalTeacher">
                <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content">
                        <form id="modalFormTeacher">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modal-title">เพิ่มอาจารย์</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>รหัสอาจารย์</label>
                                            <input type="text" class="form-control" id="code_teach" name="code_teach" placeholder="กรอกรหัสอาจารย์ ...">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>คำนำหน้า</label>
                                            <select class="form-control select-prefix-teacher" id="prefix_teach" name="prefix_teach" style="width: 100%;">
                                                <option></option>
                                                <option value="นาย">นาย</option>
                                                <option value="นาง">นาง</option>
                                                <option value="นางสาว">นางสาว</option>
                                                <option value="ว่าที่ร้อยตรี">ว่าที่ร้อยตรี</option>
                                                <option value="ว่าที่ร้อยตรีหญิง">ว่าที่ร้อยตรีหญิง</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>ชื่อ</label>
                                            <input type="text" class="form-control" id="first_name_teach" name="first_name_teach" placeholder="กรอกชื่อ ...">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>สกุล</label>
                                            <input type="text" class="form-control" id="last_name_teach" name="last_name_teach" placeholder="กรอกชื่อสกุล ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>แผนกวิชา</label>
                                            <select class="form-control select-department-teacher" id="dept_code" name="dept_code" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>ตำแหน่ง</label>
                                            <input type="text" class="form-control" id="job_teach" name="job_teach" placeholder="กรอกตำแหน่ง ...">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>วุฒิการศึกษา</label>
                                            <input type="text" class="form-control" id="qualification_teach" name="qualification_teach" placeholder="กรอกวุฒิการศึกษา ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>ประเภทวิชา</label>
                                            <input type="text" class="form-control" id="subject_type_teach" name="subject_type_teach" placeholder="กรอกประเภทวิชา ...">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>หน้าที่พิเศษ</label>
                                            <input type="text" class="form-control" id="special_duty_teach" name="special_duty_teach" placeholder="กรอกหน้าที่พิเศษ ...">
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <input type="hidden" name="action" id="action" value="Insert">
                                <input type="hidden" name="id_teacher" id="id_teacherr">
                                <input type="hidden" name="get_dept_code" id="get_dept_code">
                                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                                <button type="submit" class="btn btn-primary">บันทึกข้อมูลอาจารย์</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /. Modal -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row form-group m-0">
                                        <label class="col-lg-1 col-form-label text-right">แผนก</label>
                                        <div class="col-lg-4">
                                            <select class="form-control select_dept" id="select_dept" name="select_dept" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table id="TableTeacher" class="TableTeacher table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ลำดับ</th>
                                                <th class="text-center">รหัสอาจารย์</th>
                                                <th>ชื่อ-สกุล</th>
                                                <th>แผนกวิชา</th>
                                                <th class="text-center">ตัวเลือก</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="text-center">ลำดับ</th>
                                                <th class="text-center">รหัสอาจารย์</th>
                                                <th>ชื่อ-สกุล</th>
                                                <th>แผนกวิชา</th>
                                                <th class="text-center">ตัวเลือก</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col-md-12-->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>

