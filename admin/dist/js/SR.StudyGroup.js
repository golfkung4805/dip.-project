$(document).ready(function () {
    
    function LoadDataTable(getParamDept){

        $('#TableStudyGroup').DataTable().destroy();

        if(getParamDept!=="NULL"){
            var TableStudyGroup = $('#TableStudyGroup').DataTable({
                "retrieve": true,
                "lengthChange": true,
                "searching": true,
                "info": true,
                // "sScrollX": true,
                // "sScrollXInner": "100%",
                

                "processing": true,
                "serverSide":true,
                "ajax":{
                    url: "CRUD.StudyGroup.php",
                    data: { action:"LoadTableStudyGroup",
                            group: getParamDept 
                    },
                    type: "POST",
                }
            });
        
            // var TableStudent = $('#TableStudent').DataTable({
            //     "retrieve": true,
            //     "lengthChange": true,
            //     "searching": true,
            //     "info": true,
            //     "sScrollX": true,
            //     "sScrollXInner": "100%",
            // });

            $('#TableStudyGroup_filter').append('<button class="btn btn-primary btn-insert ml-3">เพิ่มกลุ่มเรียน</button>');

            TableStudyGroup.ajax.reload();
            // TableStudent.ajax.reload();
            
            $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
        }else{
            $('#TableStudyGroup tbody').empty(); // TODO ทำให้ข้อมูลใน tbody ว่าง
            $('#TableStudyGroup').DataTable({
                "ordering": false,
                "lengthMenu": [[ -1 ], [ "All" ]],
                "data": [] // TODO กำหนดให้ข้อมูลในตารางว่าง
            });
            $('.sorting_asc').removeClass('sorting_asc'); // TODO ลบ class sorting_asc (ตัวเรียงมากไปน้อย-น้อยไปมาก) ทั้งหมดออก
            $('.sorting_disabled').removeAttr('tabindex'); // TODO ลบ attr tabindex (ตอนเอาเมาส์ไปชี้ที่ เรียงมากไปน้อย-น้อยไปมาก) ทั้งหมดออก
            $('.form-control-sm').attr("disabled","disabled");
            $('.dataTables_empty').html('<div class="text-center"><p style="font-size: 24px;">กรุณาเลือกแผนกวิชา</p></div><i class="fas fa-search fa-3x" style="display: block; text-align: -webkit-center; margin-bottom: 15px;"></i>');
        
            $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
        }
    }

    function select2dept_reset(){
        var GetDepartment = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudyGroup.php",
                data: { action:"GetDepartment",
                        //dept_code:dept_code        
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            
            return data;
        }();

        $('.select_dept').empty();
        $('.select_dept').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนกวิชา",
            allowClear: true,
            data: GetDepartment
        });
    }
    
    function select2reset(getParamDept,teacher_code){
        // console.log(getParamDept);
        var GetDepartment = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudyGroup.php",
                data: { action:"GetDepartment",
                        dept_code:getParamDept        
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();

        var GetTeacher = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudyGroup.php",
                data: { action:"GetTeacher",
                        teacher_code:teacher_code        
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();
        // console.log(GetDepartment);
        $('.select-department').empty();
        $('.select-department').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนกวิชา",
            allowClear: true,
            disabled: true,
            data: GetDepartment
        });

        $('.select-advisors').empty();
        $('.select-advisors').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกอ.ที่ปรึกษา",
            allowClear: true,
            data: GetTeacher
        });
    }

    select2dept_reset();
    LoadDataTable("NULL");

    $('#select_dept').change(function () {
        if($(this).val()!==""){
            getParamDept = $(this).val();
            // console.log(getParamDept);
            LoadDataTable(getParamDept);
        }else{
            LoadDataTable("NULL");
        }
    });

    $(document).on('click', '.btn-insert', function(){
        $('#action').val("Insert");  
        
        $('#group_code').removeAttr("readonly");
        $('#modal-title').text('เพิ่มข้อมูลกลุ่มเรียน');
        $('#modalStudyGroup').modal('toggle');
        $('#modalFormStudyGroup')[0].reset();
        select2reset(getParamDept,"INSERT");
        $('#get_dept_code').val($('#dept_code').val());
    });

    $(document).on('click', '.btn-update', function(){  
        var group_id = $(this).data('id');
        
        $('#group_code').attr("readonly", "readonly");
        $('#action').val("Update");
        $('#modal-title').text('แก้ไขข้อมูลกลุ่มเรียน');
        $.ajax({
            type: "POST",
            url: "CRUD.StudyGroup.php",
            data: { group_id:group_id,
                    action: "getDetailStudyGroup"
            },
            dataType: "json",
            success: function (data) {
                $('#group_code').val(data[0].group_code);
                $('#group_subname').val(data[0].group_subname);
                $('#group_name').val(data[0].group_name);
                select2reset(getParamDept,data[0].teacher_code);
                $('#group_id').val(data[0].group_id);
            }
        });
        $('#modalStudyGroup').modal('toggle');
    });

    $('#modalFormStudyGroup').on('submit', function (event) {
        event.preventDefault();
        CheckLevel = $('#group_code').val().substring(2, 3);

        if($('#group_code').val().trim()===""){
            ToastAlert('warning','กรุณากรอกรหัสกลุ่มเรียน');
            $('#group_code').focus();
        }else if(isNaN($('#group_code').val())){
            ToastAlert('warning','กรุณากรอกรหัสกลุ่มเรียนเป็นตัวเลขเท่านั้น');
            $('#group_code').focus();
        }else if($('#group_code').val().length > 11 ){
            ToastAlert('warning','กรอกตัวเลขได้สูงสุด 11 ตัวเท่านั้น');
            $('#group_code').focus();
        }else if(CheckLevel !== "2" && CheckLevel !== "3"){
            SweetAlert('warning', "กรุณากรอกรหัสหลักที่ 3 ใหม่", "ตัวอย่าง 61[2]xxxxx คือระดับ ปวช. 61[3]xxxxx คือระดับ ปวส.", 'ตกลง', null);
            $('#group_code').focus();
        }else if($('#group_subname').val().trim()===""){
            ToastAlert('warning','กรุณากรอกชื่อย่อกลุ่มเรียน');
            $('#group_subname').focus();
        }else if($('#group_subname').val().length > 50 ){
            ToastAlert('warning','กรอกตัวอักษรได้สูงสุด 50 ตัวเท่านั้น');
            $('#group_subname').focus();
        }else if($('#group_name').val().trim()===""){
            ToastAlert('warning','กรุณากรอกชื่อกลุ่มเรียน');
            $('#group_name').focus();
        }else if($('#group_name').val().length > 100 ){
            ToastAlert('warning','กรอกตัวอักษรได้สูงสุด 100 ตัวเท่านั้น');
            $('#group_name').focus();
        }else if($('#dept_code').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกแผนกวิชา');
            $('#dept_code').select2('open');
        }else if($('#teacher_code').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกอาจารย์ที่ปรึกษา');
            $('#teacher_code').select2('open');
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.StudyGroup.php",
                data: new FormData(this),
                dataType: "json", 
                processData: false,
                contentType: false,
                cache: false,
                beforeSend:function(){ 
                    $('.modal-content').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (data) {
                    Swal.fire({
                        type: data.alert.type,
                        title: data.alert.title,
                        confirmButtonText: 'ตกลง'
                    });
                    if(data.alert.type==="success"){
                        LoadDataTable(getParamDept);
                        $('#modalStudyGroup').modal('hide');
                        $('#modalFormStudyGroup')[0].reset();  
                        $('.loading').remove();
                    }else{
                        $('.loading').remove();
                    }
                },
                error: function(error) {
                    console.error(error.responseText);
                    Swal.fire({
                        type: "error",
                        title: "เกิดข้อผิดผลาด",
                        confirmButtonText: 'ตกลง'
                    });
    
                    $('#modalStudyGroup').modal('hide');
                    $('#modalFormStudyGroup')[0].reset();  
                    $('.loading').remove();
                }
            });
        }
        
    });


    $(document).on('click', '.btn-delete', function () {
        var group_id = $(this).data('id');
        Swal.fire({
            title: 'คุณต้องการลบกลุ่มเรียนนี้?',
            text: "เมื่อลบแล้วไม่สามารถกู้ข้อมูลคืนได้!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน!',
            cancelButtonText: 'ยกเลิก'
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "CRUD.StudyGroup.php",
                    data: { group_id:group_id,
                            action:"Delete"
                    },
                    dataType: "json",
                    beforeSend:function(){ 
                        Swal.fire({
                            type: 'info',
                            title: 'กำลังประมวลผล',
                            confirmButtonText: 'กรุณารอสักครู่. . .'
                        });
                    },
                    success: function (result) {
                        Swal.fire({
                            type: result.alert.type,
                            title: result.alert.title,
                            confirmButtonText: 'ตกลง'
                        });
                        LoadDataTable(getParamDept);
                    },
                    error: function(error) {
                        console.error(error.responseText);
                        Swal.fire({
                            type: "error",
                            title: "เกิดข้อผิดผลาด",
                            confirmButtonText: 'ตกลง'
                        });
                        $('#modalTeacher').modal('hide');
                        $('.loading').remove();
                    }
                });
            }
        });
    });
    
});