$(function () {
    var FakeRealtime = null

    function LoadDataStudentSchedule(dept_code,group_code,select_year){
        $('#TableEducationProgram').DataTable().destroy();
        $('#TableSchedule').DataTable().destroy();
        if(dept_code !== null && group_code !== null && select_year !== null){

            $.ajax({
                type: "POST",
                url: "StudentScheduleTable.php",
                data:  { group_code : group_code,
                        term: select_year  },
                beforeSend:function(){ 
                    $('.loading').remove();
                    $('.schedule').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (StudentScheduleTable) {
                    $(".schedule").html(StudentScheduleTable);
                    $('.loading').remove();
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });

            $.ajax({
                type: "POST",
                url: "CRUD.StudentSchedule.php",
                dataType: "json",
                data:  { action : "GetPeriod",
                        group_code : group_code,
                        term: select_year  },
                success: function (result) {
                    period = 0;
                    sumPeriod = 0;
                    for(i = 0; i<=result.length-1;i++){
                        period = period + parseInt(result[i]['period']);
                        sumPeriod = sumPeriod + parseInt(result[i]['sumPeriod']);
                    }
                    if(period===sumPeriod){
                        BadgeColor = "success";
                    }else{
                        BadgeColor = "danger";
                    }
                    $('.perPeriod').html("<span class=\"badge badge-"+BadgeColor+"\" style=\"font-size: inherit;\">"+period+"</span>");
                    $('.sumPeriod').html("<span class=\"badge badge-primary\" style=\"font-size: inherit;\">"+sumPeriod+"</span>");
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });

            var TableEducationProgram = $('#TableEducationProgram').DataTable({
                "retrieve": true,
                "lengthChange": true,
                "searching": true,
                "info": true,
                // "sScrollX": true,
                // "sScrollXInner": "100%",
                

                "processing": true,
                "serverSide":true,
                "ajax":{
                    url: "CRUD.StudentSchedule.php",
                    data: { action:"LoadTableEducationProgram",
                            dept_code: dept_code,
                            group_code: group_code,
                            select_year: select_year 
                    },
                    type: "POST",
                }
            });

            var TableSchedule = $('#TableSchedule').DataTable({
                "lengthChange": true,
                "searching": true,
                "info": true,
                // "sScrollX": true,
                // "sScrollXInner": "100%"
                

                "processing": true,
                "serverSide":true,
                "ajax":{
                    url: "CRUD.StudentSchedule.php",
                    data: { action:"LoadTableSchedule",
                            dept_code: dept_code,
                            group_code: group_code,
                            select_year: select_year 
                    },
                    type: "POST",
                }
            });

            $('.btn-insert').remove();
            $('#TableSchedule_filter').append('<button class="btn btn-primary btn-insert ml-3">เพิ่มวิชาเรียน</button>');
            TableEducationProgram.ajax.reload();
            TableSchedule.ajax.reload();

            $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
            $('#TableSchedule_wrapper .row:eq(1)').addClass('overflow-auto');
        }else{ 
            // if(dept_code === null && group_code === null && select_year === null){
            //     var text = "แผนกวิชาและกลุ่มเรียนและปีการศึกษา";
            // }else if(group_code === null && select_year === null){
            //     var text = "กลุ่มเรียนและปีการศึกษา";
            // }else if(select_year === null){
            //     var text = "ปีการศึกษา";
            // }

            if(dept_code === null || group_code === null || select_year === null){
                var text = "แผนกวิชาและกลุ่มเรียนและปีการศึกษา";
            }
            
            $('#TableEducationProgram tbody').empty(); // TODO ทำให้ข้อมูลใน tbody ว่าง
            $('#TableEducationProgram').DataTable({
                "ordering": false,
                "lengthMenu": [[ -1 ], [ "All" ]],
                "data": [] // TODO กำหนดให้ข้อมูลในตารางว่าง
            });

            $('#TableSchedule tbody').empty(); // TODO ทำให้ข้อมูลใน tbody ว่าง
            $('#TableSchedule').DataTable({
                "ordering": false,
                "lengthMenu": [[ -1 ], [ "All" ]],
                "data": [] // TODO กำหนดให้ข้อมูลในตารางว่าง
            });

            $(".schedule").load("StudentScheduleSampleTable.php");

            $('.perPeriod').html("<span class=\"badge badge-danger\" style=\"font-size: inherit;\">กรุณาเลือกข้อมูลครบ</span>");
            $('.sumPeriod').html("<span class=\"badge badge-danger\" style=\"font-size: inherit;\">กรุณาเลือกข้อมูลครบ</span>");

            $('.sorting_asc').removeClass('sorting_asc'); // TODO ลบ class sorting_asc (ตัวเรียงมากไปน้อย-น้อยไปมาก) ทั้งหมดออก
            $('.sorting_disabled').removeAttr('tabindex'); // TODO ลบ attr tabindex (ตอนเอาเมาส์ไปชี้ที่ เรียงมากไปน้อย-น้อยไปมาก) ทั้งหมดออก
            $('.form-control-sm').attr("disabled","disabled");
            $('.dataTables_empty').html('<div class="text-center"><p style="font-size: 24px;">กรุณาเลือก'+text+'ให้ถูกต้อง</p></div><i class="fas fa-search fa-3x" style="display: block; text-align: -webkit-center; margin-bottom: 15px;"></i>');

            $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
            $('#TableSchedule_wrapper .row:eq(1)').addClass('overflow-auto');
        }
    }

    // $('#TableSchedule').DataTable();

    dataNull = [{ id: "", text: '' }];

    function select_group(){
        $('.select_group').empty();
        $('.select_group').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนกวิชาก่อน",
            disabled: true,
            data: dataNull
        });
    }

    function select_year(status){
        var NotSelect = "กรุณาเลือกกลุ่มเรียนก่อน";
        if(status==="NotSelect"){
            NotSelect = "กรุณาเลือกแผนกและกลุ่มเรียนก่อน"
        }

        $('.select_year').empty();
        $('#select_year').select2({
            theme: 'bootstrap4',
            placeholder: NotSelect,
            disabled: true,
            data: dataNull
        });
    }

    function select2dept_reset(dept_code){
        var GetDepartment = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetDepartment",
                        dept_code:dept_code
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
            
        }();

        if(Cookies.get('CK_Chack_Status')!=="ADMIN" && Cookies.get('CK_Chack_Status')!=="61310"){
            Cookies.set('SchCK_dept_code', dept_code);
            $('.select_dept').empty();
            $('.select_dept').select2({
                theme: 'bootstrap4',
                placeholder: "กรุณาเลือกแผนกวิชาก่อน",
                disabled: true,
                data: GetDepartment
            });
        }else{
            $('.select_dept').empty();
            $('.select_dept').select2({
                theme: 'bootstrap4',
                placeholder: "กรุณาเลือกแผนกวิชา",
                allowClear: true,
                data: GetDepartment
            });
        }
        
        if(typeof Cookies.get('SchCK_dept_code')==="undefined"){
            select_group();
            select_year("NotSelect");
        }else{
            select2group_reset(dept_code);
            select_year();
        }
    }

    function select2group_reset(dept_code){
        if(typeof Cookies.get('SchCK_group_code')==="undefined"){
            SchCK_group_code = "NULL";
        }else{
            SchCK_group_code = Cookies.get('SchCK_group_code');
        }
        var GetGroup = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetGroup",
                        dept_code:dept_code,
                        group_code:SchCK_group_code   
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();

        $('.select_group').empty();
        $('.select_group').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกกลุ่มเรียน",
            allowClear: true,
            disabled: false,
            data: GetGroup
        });

        if(SchCK_group_code==="NULL"){
            select_year();
        }else{
            select2year_reset(Cookies.get('SchCK_group_code'));
        }
        
    }

    function select2year_reset(group_code){
        if(typeof Cookies.get('SchCK_term')==="undefined"){
            SchCK_term = "NULL";
        }else{
            SchCK_term = Cookies.get('SchCK_term');
        }
        var GetYear = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetYear",
                        group_code:group_code,
                        select_year:SchCK_term    
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();
        
        $('.select_year').empty();
        $('.select_year').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกปีการศึกษา",
            allowClear: true,
            disabled: false,
            data: GetYear
        });
    }
   
    if(Cookies.get('CK_Chack_Status')!=="ADMIN"){
        SchCK_dept_code = Cookies.get('CK_Chack_Status');
        select2dept_reset(SchCK_dept_code);

        if(FakeRealtime !== null){
            clearInterval(FakeRealtime);
        }
    }else{
        //  ! ตัวเก่า
        if(typeof Cookies.get('SchCK_dept_code')!=="undefined"){
            select2dept_reset(Cookies.get('SchCK_dept_code'));

            if(FakeRealtime !== null){
                clearInterval(FakeRealtime);
            }
        }else{
            Cookies.remove('SchCK_dept_code');
            Cookies.remove('SchCK_group_code');
            Cookies.remove('SchCK_term');
            
            select2dept_reset("NULL");
            LoadDataStudentSchedule(null,null,null);

            if(FakeRealtime !== null){
                clearInterval(FakeRealtime);
            }
        }

    }


    if(Cookies.get('CK_Chack_Status')!=="ADMIN"){
        SchCK_dept_code = Cookies.get('CK_Chack_Status');
        select2group_reset(SchCK_dept_code);

        if(FakeRealtime !== null){
            clearInterval(FakeRealtime);
        }
    }else{
        //  ! ตัวเก่า
        if(typeof Cookies.get('SchCK_group_code')!=="undefined"){
            select2group_reset(Cookies.get('SchCK_dept_code')); 

            if(FakeRealtime !== null){
                clearInterval(FakeRealtime);
            }
        }else{
            Cookies.remove('SchCK_group_code');
            Cookies.remove('SchCK_term');

            LoadDataStudentSchedule(null,null,null);

            if(FakeRealtime !== null){
                clearInterval(FakeRealtime);
            }
        }

    }

    if(typeof Cookies.get('SchCK_term')!=="undefined"){
        select2year_reset(Cookies.get('SchCK_group_code')); 
        LoadDataStudentSchedule(Cookies.get('SchCK_dept_code'),Cookies.get('SchCK_group_code'),Cookies.get('SchCK_term'));

        var SchCK_dept_code = Cookies.get('SchCK_dept_code');
        var SchCK_group_code = Cookies.get('SchCK_group_code');
        var SchCK_term = Cookies.get('SchCK_term');

        
        if(!window.matchMedia("(max-width: 1024px)").matches){
            if(FakeRealtime !== null){
                clearInterval(FakeRealtime);
            }
            FakeRealtime = setInterval( LoadDataStudentSchedule, 10000, SchCK_dept_code, SchCK_group_code, SchCK_term );
        }
    }else{
        Cookies.remove('SchCK_term');

        LoadDataStudentSchedule(null,null,null);

        if(FakeRealtime !== null){
            clearInterval(FakeRealtime);
        }
    }

    // $('#select_dept').select2('focus');
    $('#select_dept').change(function () { 
        var dept_code = $(this).val();
        if(dept_code !== ""){
            Cookies.set('SchCK_dept_code', dept_code);
            select2group_reset(dept_code); 
            LoadDataStudentSchedule(dept_code,null,null);

            if(FakeRealtime !== null){
                clearInterval(FakeRealtime);
            }
        }else{
            // $(".schedule").load("StudentScheduleSampleTable.php");
            Cookies.remove('SchCK_dept_code');
            Cookies.remove('SchCK_group_code');
            Cookies.remove('SchCK_term');
            select_group();
            select_year("NotSelect");
            LoadDataStudentSchedule(null,null,null);
            
            if(FakeRealtime !== null){
                clearInterval(FakeRealtime);
            }
        }
    });

    $('#select_group').change(function () { 
        var dept_code = $('#select_dept').val();
        var group_code = $(this).val();
        if(group_code !== ""){
            Cookies.set('SchCK_group_code', group_code);
            select2year_reset(group_code); 
            LoadDataStudentSchedule(dept_code,group_code,null);

            if(FakeRealtime !== null){
                clearInterval(FakeRealtime);
            }
        }else{
            // $(".schedule").load("StudentScheduleSampleTable.php");
            Cookies.remove('SchCK_dept_code');
            Cookies.remove('SchCK_group_code');
            select_year();
            LoadDataStudentSchedule(dept_code,null,null);
            
            if(FakeRealtime !== null){
                clearInterval(FakeRealtime);
            }
        }
    });
    
    $('#select_year').change(function () { 
        var select_year = $(this).val();
        if(select_year !== ""){
            Cookies.set('SchCK_term', select_year);
            var dept_code = $('#select_dept').val();
            var group_code = $('#select_group').val();
            LoadDataStudentSchedule(dept_code,group_code,select_year);
            // clearInterval(FakeRealtime);
            
            if(!window.matchMedia("(max-width: 1024px)").matches){
                if(FakeRealtime !== null){
                    clearInterval(FakeRealtime);
                }
                FakeRealtime = setInterval( LoadDataStudentSchedule, 10000, dept_code, group_code, select_year );
            }
        
        }else{
            // $(".schedule").load("StudentScheduleSampleTable.php");

            Cookies.remove('SchCK_term');
            LoadDataStudentSchedule(dept_code,group_code,null);
            
            if(FakeRealtime !== null){
                clearInterval(FakeRealtime);
            }
        }
    });

    // ! ==============================================================================================================

    
    $(document).on('click', '.btn-insert', function(){
        $('#action').val("Insert");  
        $('#group_code').val(Cookies.get('SchCK_group_code'));  
        $('#term').val(Cookies.get('SchCK_term'));  
        $('#modalSchedule').modal('toggle');
        $('#modalFormSchedule')[0].reset();

    
        $('#sch_date').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกวัน",
            allowClear: true
        });

        select2StartTime_reset("INSERT");
        select2SubjectInPlan_reset("INSERT");
        select_Teacher();
        // select2Teacher_reset("INSERT");
        select2Room_reset("INSERT");
    });

    $(document).on('click', '.btn-update', function(){  
        var sch_id = $(this).data('id');
        $('#action').val("Update");
        $('#group_code').val(Cookies.get('SchCK_group_code'));  
        $('#term').val(Cookies.get('SchCK_term'));  
        $('#modal-title').text('แก้ไขข้อมูลกลุ่มเรียน');
        $.ajax({
            type: "POST",
            url: "CRUD.StudentSchedule.php",
            data: { sch_id:sch_id,
                    action: "GetDetailSchedule"
            },
            dataType: "json",
            success: function (data) {
                $('#sch_date').val(data[0].sch_date).change();
                select2StartTime_reset(data[0].sch_start_time);
                select2EndTime_reset(data[0].sch_end_time);
                select2SubjectInPlan_reset(data[0].subject_code);
                select2Teacher_reset(data[0].teacher_code);
                select2Room_reset(data[0].room_code);
                $('#sch_id').val(sch_id);
                // select2Room_reset("INSERT");
                // $('#room_code').val(data[0].room_code).change();
            },
            error: function(error) {
                console.error(error.responseText);
                // SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                // $('#modalSchedule').modal('hide');
                // $('#modalFormSchedule')[0].reset();  
                // $('.loading').remove();
            }
        });
        $('#modalSchedule').modal('toggle');
    });

    $('#modalFormSchedule').on("submit", function (e) {
        e.preventDefault();
        if($('#sch_date').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกวันสอน');
            $('#sch_date').select2('open');
        }else if($('#sch_start_time').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกเวลาเริ่มคาบ');
            $('#sch_start_time').select2('open');
        }else if($('#sch_end_time').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกเวลาจบคาบ');
            $('#sch_end_time').select2('open');
        }else if($('#subject_code').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกวิชาเรียน');
            $('#subject_code').select2('open');
        }else if($('#teacher_code').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกอาจารย์สอน');
            $('#teacher_code').select2('open');
        }else if($('#room_code').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกห้องเรียน');
            $('#room_code').select2('open');
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.StudentSchedule.php",
                data: new FormData(this),
                dataType: "json", 
                processData: false,
                contentType: false,
                cache: false,
                beforeSend:function(){ 
                    $('.modal-content').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (data) {
                    if($('#action').val()==="Update"){
                        SweetAlert(data.alert.type, data.alert.title, data.alert.text, 'ตกลง', "ถ้าเกิดข้อผิดพลาดในการคำนวณคาบโปรดแจ้งพร้อมบอกวิธีการป้อน");
                    }else{
                        SweetAlert(data.alert.type, data.alert.title, data.alert.text, 'ตกลง', "รีเฟรชบราวเซอร์เพื่ออัพเดจข้อมูลล่าสุด");
                    }
                    
                    if(data.alert.type==="success"){
                        $('#modalSchedule').modal('hide');
                        $('#modalFormSchedule')[0].reset();  
                        LoadDataStudentSchedule(Cookies.get('SchCK_dept_code'),Cookies.get('SchCK_group_code'),Cookies.get('SchCK_term'));
                    }
                    
                    $('.loading').remove();
                    
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                    // $('#modalSchedule').modal('hide');
                    // $('#modalFormSchedule')[0].reset();  
                    $('.loading').remove();
                }
            });
        }

        
    });
    
    $(document).on('click', '.btn-delete', function () {
        var sch_id = $(this).data('id');
        Swal.fire({
            title: 'คุณต้องการลบวิชานี้?',
            text: "เมื่อลบแล้วไม่สามารถกู้ข้อมูลคืนได้!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน!',
            cancelButtonText: 'ยกเลิก'
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "CRUD.StudentSchedule.php",
                    data: { sch_id:sch_id,
                            action:"Delete"
                    },
                    dataType: "json",
                    beforeSend:function(){ 
                        SweetAlert('info', 'กำลังประมวลผล', null, 'กรุณารอสักครู่. . .', null);
                    },
                    success: function (result) {
                        SweetAlert(result.alert.type, result.alert.title, null, 'ตกลง', null);
                        LoadDataStudentSchedule(Cookies.get('SchCK_dept_code'),Cookies.get('SchCK_group_code'),Cookies.get('SchCK_term'));
                    },
                    error: function(error) {
                        console.error(error.responseText);
                        SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                    }
                });
            }
        });
    });

    $('#sch_date').select2({
        theme: 'bootstrap4',
        placeholder: "กรุณาเลือกวัน",
        allowClear: true
    });


    function select_EndTime(){
        $('#sch_end_time').empty();
        $('#sch_end_time').select2({
            theme: 'bootstrap4',
                placeholder: "กรุณาเลือกเวลาเริ่มคาบก่อน",
                disabled: true,
                data: dataNull
        });
    }

    function select_Teacher(){
        $('#teacher_code').empty();
        $('#teacher_code').select2({
            theme: 'bootstrap4',
                placeholder: "กรุณาเลือกวิชาเรียนก่อน",
                disabled: true,
                data: dataNull
        });
    }
    
    function select2StartTime_reset(sch_start_time){
        var GetStartTime = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetStartTime",
                        sch_start_time:sch_start_time },
                dataType: "json",
                success: function (result) {
                    data = result;
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });
            return data;
        }();

        $('#sch_start_time').empty();
        $('#sch_start_time').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกเวลาเริ่มคาบ",
            allowClear: true,
            data: GetStartTime
        });

        select_EndTime();
    }

    function select2EndTime_reset(sch_end_time){  
        startTime = $('#sch_start_time').val();   
        var GetEndTime = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetEndTime",
                        startTime:startTime,
                        sch_end_time:sch_end_time },
                dataType: "json",
                success: function (result) {
                    data = result;
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });
            return data;
        }();

        $('#sch_end_time').empty();
        $('#sch_end_time').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกเวลาเริ่มคาบ",
            allowClear: true,
            disabled: false,
            data: GetEndTime
        });

    }

    function select2SubjectInPlan_reset(subject_code){
        var GetSubjectInPlan = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetSubjectInPlan",
                        subject_code:subject_code,
                        group_code:Cookies.get('SchCK_group_code'),
                        term:Cookies.get('SchCK_term')
                         },
                dataType: "json",
                success: function (result) {
                    data = result;
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });
            return data;
        }();

        $('#subject_code').empty();
        $('#subject_code').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกวิชาเรียน",
            allowClear: true,
            data: GetSubjectInPlan
        });
    }

    function select2Teacher_reset(teacher_code){
        subject_code = $('#subject_code').val();
        var GetTeacher = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetTeacher",
                        teacher_code:teacher_code
                         },
                dataType: "json",
                success: function (result) {
                    data = result;
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });
            return data;
        }();
        if(teacher_code === "NULL"){
            disabled = false;
            allowClear = true;
        }else{
            disabled = true;
            allowClear = false;
        }
        $('#teacher_code').empty();
        $('#teacher_code').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกอาจารย์สอน",
            allowClear: true,
            disabled: false,
            data: GetTeacher
        });
    }

    function select2Room_reset(room_code){
        var GetRoom = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetRoom",
                        room_code:room_code
                         },
                dataType: "json",
                success: function (result) {
                    data = result;
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });
            return data;
        }();

        $('#room_code').empty();
        $('#room_code').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกห้องเรียน",
            allowClear: true,
            data: GetRoom
        });
    }

    $('#sch_start_time').change(function(){
        var startTime = $(this).val();
        if(startTime !== ""){
            select2EndTime_reset("NULL");
        }else{
            select_EndTime();
        }
    });

    $('#subject_code').change(function(){
        var subject_code = $(this).val();
        var group_code = $('#group_code').val();
        var term = $('#term').val();
        if(subject_code !== ""){
            $.ajax({
                type: "POST",
                url: "CRUD.StudentSchedule.php",
                data: { action:"GetCodeTeacher",
                        subject_code:subject_code,
                        group_code:group_code,
                        term:term
                },
                dataType: "json", 
                // processData: false,
                // contentType: false,
                cache: false,
                success: function (data) {
                    if(data.length > 0){
                        teacher_code = data[0]['teacher_code'];
                    }else{
                        teacher_code = "NULL";
                    }
                    select2Teacher_reset(teacher_code);
                },
                error: function(error) {
                    console.error(error.responseText);
                }
            });
        }else{
            select_Teacher();
        }
    });










});