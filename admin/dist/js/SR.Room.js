$(document).ready(function () {
    function LoadDataTable(action){
        TableRoom = $('#TableRoom').DataTable({
            "retrieve": true,
            "lengthChange": true,
            "searching": true,
            "info": true,
            // "sScrollX": true,
            // "sScrollXInner": "100%",

            "processing": true,
            "serverSide": true,
            "ajax":{
                url: "CRUD.Room.php",
                data: { action:"LoadTableRoom" },
                type: "POST",
            }
        });
        if(action==="New"){
            $('#TableRoom_filter').append('<button class="btn btn-primary btn-insert ml-3">เพิ่มห้องเรียน</button>');
        }
        TableRoom.ajax.reload();

        $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
    }

    LoadDataTable("New");

    $(document).on('click', '.btn-insert', function(){
        $('#action').val("Insert");  
        $('#modal-title').text('เพิ่มข้อมูลห้องเรียน');
        $('#modalRoom').modal('toggle');
        $('#modalFormRoom')[0].reset();
    });

    $(document).on('click', '.btn-update', function(){  
        var room_id = $(this).data("id");
        $('#action').val("Update");
        $('#modal-title').text('แก้ไขข้อมูลห้องเรียน');
        $.ajax({
            type: "POST",
            url:"CRUD.Room.php",
            data: { room_id:room_id,
                action:"getDetailRoom"
            },
            dataType: "json",
            success: function (data) {
                $('#code_room').val(data[0].room_code);
                $('#name_room').val(data[0].room_name);
                $('#id_room').val(data[0].room_id);
            },
            error: function(error) {
                console.error(error.responseText);
                Swal.fire({
                    type: "error",
                    title: "เกิดข้อผิดผลาด",
                    confirmButtonText: 'ตกลง'
                });
                $('#modalTeacher').modal('hide');
            }
        });
        $('#modalRoom').modal('toggle');
    });

    $('#modalFormRoom').on("submit",function (event) { 
        event.preventDefault();
        if($('#code_room').val().trim()===""){
            ToastAlert('warning','กรุณากรอกชื่อย่อห้องเรียน');
            $('#code_room').focus();
        }else if($('#code_room').val().length > 50 ){
            ToastAlert('warning','กรอกตัวอักษรได้สูงสุด 50 ตัวเท่านั้น');
            $('#code_room').focus();
        }else if($('#name_room').val().trim()===""){
            ToastAlert('warning','กรุณากรอกชื่อห้องเรียน');
            $('#name_room').focus();
        }else if($('#name_room').val().length > 100 ){
            ToastAlert('warning','กรอกตัวอักษรได้สูงสุด 100 ตัวเท่านั้น');
            $('#name_room').focus();
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.Room.php",
                data: new FormData(this),
                dataType: "json", 
                processData: false,
                contentType: false,
                cache: false,
                beforeSend:function(){ 
                    $('.modal-content').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (data) {
                    Swal.fire({
                        type: data.alert.type,
                        title: data.alert.title,
                        confirmButtonText: 'ตกลง'
                    })
                    if(data.alert.type === "success"){
                        LoadDataTable();
                        $('#modalRoom').modal('hide');
                        $('#modalFormRoom')[0].reset();
                        $('.loading').remove();
                    }else{
                        $('.loading').remove();
                    }
                },
                error: function(){
                    $('.loading').remove();
                    console.error(error.responseText);
                    Swal.fire({
                        type: "error",
                        title: "เกิดข้อผิดผลาด",
                        confirmButtonText: 'ตกลง'
                    });
                    $('#modalRoom').modal('hide');
                    $('#modalFormRoom')[0].reset();
                    
                }
            });
        }
    });

    $(document).on('click', '.btn-delete', function(){
        var room_id = $(this).data('id');
        Swal.fire({
            title: 'คุณต้องการลบแผนกนี้?',
            text: "เมื่อลบแล้วไม่สามารถกู้ข้อมูลคืนได้!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน!',
            cancelButtonText: 'ยกเลิก'
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "CRUD.Room.php",
                    data: { room_id:room_id,
                            action:"Delete"
                    },
                    dataType: "json",
                    beforeSend:function(){ 
                        Swal.fire({
                            type: 'info',
                            title: 'กำลังประมวลผล',
                            confirmButtonText: 'กรุณารอสักครู่. . .'
                        });
                    },
                    success: function (result) {
                        Swal.fire({
                            type: result.alert.type,
                            title: result.alert.title,
                            confirmButtonText: 'ตกลง'
                        });
                        LoadDataTable();
                    },
                    error: function(error) {
                        console.error(error.responseText);
                        Swal.fire({
                            type: "error",
                            title: "เกิดข้อผิดผลาด",
                            confirmButtonText: 'ตกลง'
                        });
                        $('#modalTeacher').modal('hide');
                        $('.loading').remove();
                    }
                });
            }
        });
    });
});