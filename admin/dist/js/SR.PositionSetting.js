$(document).ready(function () {

    function select2reset(){
        // console.log(getParamDept);
        var GetTeacher = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.PositionSetting.php",
                data: { action:"GetTeacher"   
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();
        // console.log(GetDepartment);
        $('.select-director').empty();
        $('.select-director').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกผู้อํานวยการ",
            allowClear: true,
            data: GetTeacher
        });

        $('.select-dacademic').empty();
        $('.select-dacademic').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกรองผู้อำนวยการฝ่ายวิชาการ",
            allowClear: true,
            data: GetTeacher
        });

        $('.select-program').empty();
        $('.select-program').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกหัวหน้างานหลักสูตรฯ:",
            allowClear: true,
            data: GetTeacher
        });
    }

      
    $.ajax({
        type: "POST",
        url: "CRUD.PositionSetting.php",
        data: { action: "getPosition"
        },
        dataType: "json",
        success: function (data) {
            select2reset();
            $('#director').val(data[0].teacher_code).change();
            $('#dacademic').val(data[1].teacher_code).change();
            $('#program').val(data[2].teacher_code).change();

            $('#director_id').val(data[0].pos_position);
            $('#dacademic_id').val(data[1].pos_position);
            $('#program_id').val(data[2].pos_position);
        }
    });
 
    $('#modalFormPositionSetting').on('submit', function (event) {
        event.preventDefault();
        if($('#director').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกผู้อํานวยการ');
            $('#director').select2('open');
        }else if($('#dacademic').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกรองผู้อำนวยการฝ่ายวิชาการ');
            $('#dacademic').select2('open');
        }else if($('#program').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกหัวหน้างานหลักสูตรฯ');
            $('#program').select2('open');
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.PositionSetting.php",
                data: new FormData(this),
                // dataType: "json", 
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    SweetAlert('success', 'แก้ไขตำแหน่งงานสำเสร็จ', null, 'ตกลง', null);
                    // $('#director').val(data[0].teacher_code).change();
                    // $('#dacademic').val(data[1].teacher_code).change();
                    // $('#program').val(data[2].teacher_code).change();
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });
        }
        
    });
});