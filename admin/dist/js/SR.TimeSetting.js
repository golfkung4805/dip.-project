$(function () {
    $('.loading').remove();
    //Timepicker
    $('#time_start').datetimepicker({
      format: 'HH:mm'
    })
     //Timepicker
     $('#time_end').datetimepicker({
        format: 'HH:mm'
    })
      //Timepicker
     $('#time_class').datetimepicker({
        format: 'H:mm'
    })
    
    $('#modalFormTimeSetting').on('submit', function (event) {
        event.preventDefault();
        if($('.time_start').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกเวลาเริ่มเรียน');
            $('.time_start').val('08:00');
            $('#time_start_target').click();
        }else if($('.time_end').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกเวลาสิ้นสุดเรียน');
            $('.time_end').val('20:00');
            $('#time_end_target').click();
        }else if($('.time_class').val().trim()===""){
            $('.time_class').val('01:00');
            ToastAlert('warning','กรุณาเลือกเวลาคาบเรียน');
            $('#time_class_target').click();
        }else if($('.time_start').val() >= $('.time_end').val()){
            // $('.time_end').val('01:00');
            ToastAlert('warning','เวลาสิ้นสุดไม่สามารถน้อยกว่าหรือเท่ากับเวลาเริ่มได้');
            $('#time_end_target').click();
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.TimeSetting.php",
                data: new FormData(this),
                dataType: "json", 
                processData: false,
                contentType: false,
                cache: false,
                beforeSend:function(){ 
                    $('.schedule').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (data) {
                    SweetAlert(data.alert.type, data.alert.title, null, 'ตกลง', null);
                    $(".schedule").load("TimeSettingSampleTable.php");
                    $('.loading').remove();
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                    $('.loading').remove();
                }
            });
        }
        
    });
});