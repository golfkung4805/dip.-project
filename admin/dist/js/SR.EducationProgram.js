$(document).ready(function () {
    dataNull = [{ id: "", text: '' }];

    function select_group(){
        $('.select_group').empty();
        $('.select_group').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนกวิชาก่อน",
            disabled: true,
            data: dataNull
        });
    }

    function select_year(status){
        var NotSelect = "กรุณาเลือกกลุ่มเรียนก่อน";
        if(status==="NotSelect"){
            NotSelect = "กรุณาเลือกแผนกและกลุ่มเรียนก่อน"
        }

        $('.select_year').empty();
        $('#select_year').select2({
            theme: 'bootstrap4',
            placeholder: NotSelect,
            disabled: true,
            data: dataNull
        });
    }

    function select2dept_reset(dept_code){
        var GetDepartment = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.EducationProgram.php",
                data: { action:"GetDepartment",
                        dept_code:dept_code
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();
        
        if(Cookies.get('CK_Chack_Status')!=="ADMIN"){
            Cookies.set('EProCK_dept_code', dept_code);
            $('.select_dept').empty();
            $('.select_dept').select2({
                theme: 'bootstrap4',
                placeholder: "กรุณาเลือกแผนกวิชาก่อน",
                disabled: true,
                data: GetDepartment
            });
        }else{
            $('.select_dept').empty();
            $('.select_dept').select2({
                theme: 'bootstrap4',
                placeholder: "กรุณาเลือกแผนกวิชา",
                allowClear: true,
                data: GetDepartment
            });
        }
        
        if(typeof Cookies.get('EProCK_dept_code')==="undefined"){
            select_group();
            select_year("NotSelect");
        }else{
            select2group_reset(dept_code);
            select_year();
        }
        // $('.select_dept').empty();
        // $('.select_dept').select2({
        //     theme: 'bootstrap4',
        //     placeholder: "กรุณาเลือกแผนกวิชา",
        //     allowClear: true,
        //     data: GetDepartment
        // });
        // select_group();
        // select_year("NotSelect");
    }

    function select2group_reset(dept_code){
        if(typeof Cookies.get('EProCK_group_code')==="undefined"){
            EProCK_group_code = "NULL";
        }else{
            EProCK_group_code = Cookies.get('EProCK_group_code');
        }
        var GetGroup = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetGroup",
                        dept_code:dept_code,
                        group_code:EProCK_group_code   
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();

        $('.select_group').empty();
        $('.select_group').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกกลุ่มเรียน",
            allowClear: true,
            disabled: false,
            data: GetGroup
        });

        if(EProCK_group_code==="NULL"){
            select_year();
        }else{
            select2year_reset(Cookies.get('EProCK_group_code'));
        }
        
    }

    function select2year_reset(group_code){
        if(typeof Cookies.get('EProCK_term')==="undefined"){
            EProCK_term = "NULL";
        }else{
            EProCK_term = Cookies.get('EProCK_term');
        }
        var GetYear = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetYear",
                        group_code:group_code,
                        select_year:EProCK_term    
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();
        
        $('.select_year').empty();
        $('.select_year').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกปีการศึกษา",
            allowClear: true,
            disabled: false,
            data: GetYear
        });
    }

    if(Cookies.get('CK_Chack_Status')!=="ADMIN"){
        EProCK_dept_code = Cookies.get('CK_Chack_Status');
        select2dept_reset(EProCK_dept_code);

    }else{
        //  ! ตัวเก่า
        if(typeof Cookies.get('EProCK_dept_code')!=="undefined"){
            select2dept_reset(Cookies.get('EProCK_dept_code'));
        }else{
            Cookies.remove('EProCK_dept_code');
            Cookies.remove('EProCK_group_code');
            Cookies.remove('EProCK_term');
            
            select2dept_reset("NULL");
            LoadDataTable(null,null,null);
        }

    }


    if(Cookies.get('CK_Chack_Status')!=="ADMIN"){
        EProCK_dept_code = Cookies.get('CK_Chack_Status');
        select2group_reset(EProCK_dept_code);
    }else{
        //  ! ตัวเก่า
        if(typeof Cookies.get('EProCK_group_code')!=="undefined"){
            select2group_reset(Cookies.get('EProCK_dept_code')); 
        }else{
            Cookies.remove('EProCK_group_code');
            Cookies.remove('EProCK_term');
            LoadDataTable(null,null,null);
        }

    }

    if(typeof Cookies.get('EProCK_term')!=="undefined"){
        select2year_reset(Cookies.get('EProCK_group_code')); 
        LoadDataTable(Cookies.get('EProCK_dept_code'),Cookies.get('EProCK_group_code'),Cookies.get('EProCK_term'));
    }else{
        Cookies.remove('EProCK_term');
        LoadDataTable(null,null,null);
    }

    function LoadDataTable(dept_code,group_code,select_year){
        $('#TableEducationProgram').DataTable().destroy();
        if(dept_code !== null && group_code !== null && select_year !== null){
            var TableEducationProgram = $('#TableEducationProgram').DataTable({
                "retrieve": true,
                "lengthChange": true,
                "searching": true,
                "info": true,
                // "sScrollX": true,
                // "sScrollXInner": "100%",
                

                "processing": true,
                "serverSide":true,
                "ajax":{
                    url: "CRUD.EducationProgram.php",
                    data: { action:"LoadTableEducationProgram",
                            dept_code: dept_code,
                            group_code: group_code,
                            select_year: select_year 
                    },
                    type: "POST",
                }
            });
        
            // var TableStudent = $('#TableStudent').DataTable({
            //     "retrieve": true,
            //     "lengthChange": true,
            //     "searching": true,
            //     "info": true,
            //     "sScrollX": true,
            //     "sScrollXInner": "100%",
            // });

            $('#TableEducationProgram_filter').append('<button class="btn btn-primary btn-insert ml-3">เพิ่มวิชา</button>');

            TableEducationProgram.ajax.reload();
            // TableStudent.ajax.reload();
            $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
        }else{
            
            // if(dept_code === null && group_code === null && select_year === null){
            //     var text = "แผนกวิชาและกลุ่มเรียนและปีการศึกษา";
            // }else if(group_code === null && select_year === null){
            //     var text = "กลุ่มเรียนและปีการศึกษา";
            // }else if(select_year === null){
            //     var text = "ปีการศึกษา";
            // }
            
            if(dept_code === null || group_code === null || select_year === null){
                var text = "แผนกวิชาและกลุ่มเรียนและปีการศึกษา";
            }

            $('#TableEducationProgram tbody').empty(); // TODO ทำให้ข้อมูลใน tbody ว่าง
            $('#TableEducationProgram').DataTable({
                "ordering": false,
                "lengthMenu": [[ -1 ], [ "All" ]],
                "data": [] // TODO กำหนดให้ข้อมูลในตารางว่าง
            });
            $('.sorting_asc').removeClass('sorting_asc'); // TODO ลบ class sorting_asc (ตัวเรียงมากไปน้อย-น้อยไปมาก) ทั้งหมดออก
            $('.sorting_disabled').removeAttr('tabindex'); // TODO ลบ attr tabindex (ตอนเอาเมาส์ไปชี้ที่ เรียงมากไปน้อย-น้อยไปมาก) ทั้งหมดออก
            $('.form-control-sm').attr("disabled","disabled");
            $('.dataTables_empty').html('<div class="text-center"><p style="font-size: 24px;">กรุณาเลือก'+text+'ให้ถูกต้อง</p></div><i class="fas fa-search fa-3x" style="display: block; text-align: -webkit-center; margin-bottom: 15px;"></i>');

            $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
        }
    }

    // select in modal
    function select2reset(subject_code){
        var getSelectDept = $('#select_dept').val();
        var GetSelectGroup = $('#select_group').val();

        var GetStudyGroup = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.EducationProgram.php",
                data: { action:"GetGroup",
                        dept_code:getSelectDept,
                        group_code:GetSelectGroup     
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();

        var GetDepartment = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.EducationProgram.php",
                data: { action:"GetDepartment",
                        dept_code:getSelectDept        
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();

        // var GetTeacher = function () {
        //     var data = null;
        //     $.ajax({
        //         async: false,
        //         type: "POST",
        //         url:"CRUD.EducationProgram.php",
        //         data: { action:"GetTeacher",
        //                 // teacher_code:teacher_code        
        //         },
        //         dataType: "json",
        //         success: function (result) {
        //             data = result;
        //         }
        //     });
        //     return data;
        // }();

        var GetSubject = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.EducationProgram.php",
                data: { action:"GetSubject", 
                        check:"ALL",
                        subject_code:subject_code          
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();

        $('.select-study-group').empty();
        $('.select-study-group').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกกลุ่มเรียน",
            // allowClear: true,
            data: GetStudyGroup
        });

        $('.select-department').empty();
        $('.select-department').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนกวิชา",
            // allowClear: true,
            data: GetDepartment
        });
        // ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // ! เพิ่ม select2 pagination เพื่อเพิ่มความเร็ว query ข้อมูล
        // ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $('.select-subject-code').empty();
        $('.select-subject-code').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกวิชา",
            allowClear: true,
            data: GetSubject
        });
    }

    //  select in page
    // select2dept_reset();
    // LoadDataTable(null,null,null);
    // ! กรณี FIX แผนกไว้
    // select2group_reset(61610); 

    $('#select_dept').change(function () { 
        var dept_code = $(this).val();
        if(dept_code !== ""){
            Cookies.set('EProCK_dept_code', dept_code);
            select2group_reset(dept_code); 
            LoadDataTable(dept_code,null,null);
        }else{
            Cookies.remove('EProCK_dept_code');
            Cookies.remove('EProCK_group_code');
            Cookies.remove('EProCK_term');
            select_group();
            select_year("NotSelect");
            LoadDataTable(null,null,null);
        }
    });
    // ! กรณี FIX แผนกไว้ ใช้ Cookies เช็ค
    // ! console.log(Cookies.get('foo'));

    $('#select_group').change(function () { 
        var dept_code = $('#select_dept').val();
        var group_code = $(this).val();
        // ! กรณี FIX แผนกไว้ ใช้ Cookies เช็ค
        // ! Cookies.set('foo', group_code)

        if(group_code !== ""){
            Cookies.set('EProCK_group_code', group_code);
            select2year_reset(group_code); 
            LoadDataTable(dept_code,group_code,null);
        }else{
            Cookies.remove('EProCK_dept_code');
            Cookies.remove('EProCK_group_code');
            select_year();
            LoadDataTable(dept_code,null,null);
        }
    });
    
    $('#select_year').change(function () { 
        var select_year = $(this).val();
        if(select_year !== ""){
            Cookies.set('EProCK_term', select_year);
            var dept_code = $('#select_dept').val();
            var group_code = $('#select_group').val();
            LoadDataTable(dept_code,group_code,select_year);
        }else{
            Cookies.remove('EProCK_term');
            LoadDataTable(dept_code,group_code,null);
        }
    });

    $(document).on('click', '.btn-insert', function(){
        var subYear = $('.select_year').val().split("/");

        $('#action').val("Insert");  
        $('#modal-title').text('เพิ่มข้อมูลแผนการเรียน');
        $('#modalSubject').modal('toggle');
        $('#modalFormSubject')[0].reset();

        $('#term').val(subYear[0]);
        $('#year').val(subYear[1]);
        select2reset("ALL");
    });

    $(document).on('click', '.btn-update', function(){  
        var plan_id = $(this).data('id');
        $('#action').val("Update");
        $('#modal-title').text('แก้ไขข้อมูลแผนการเรียน');
        $.ajax({
            type: "POST",
            url: "CRUD.EducationProgram.php",
            data: { plan_id:plan_id,
                    action:"getPlan",
            },
            dataType: "json",
            success: function (data) {
                var subYear = $('.select_year').val().split("/");
                select2reset(data[0].subject_code);
                $('#subject_code').val(data[0].subject_code);
                $('#term').val(subYear[0]);
                $('#year').val(subYear[1]);
                GetSubject(data[0].subject_code);
                $('#plan_id').val(data[0].plan_id);
            },
            error: function(error) {
                console.error(error.responseText);
                Swal.fire({
                    type: "error",
                    title: "เกิดข้อผิดผลาด",
                    confirmButtonText: 'ตกลง'
                });
                $('#modalSubject').modal('hide');
                $('.loading').remove();
            }
        });

        $('#modalSubject').modal('toggle');
    });

    function GetSubject(subject_code){
        $('#period').val("");
        $('#theory').val("");
        $('#practice').val("");
        $('#credit').val("");

        $.ajax({
            type: "POST",
            url:"CRUD.EducationProgram.php",
            data: { action:"GetSubject",
                    check: "NotALL",
                    subject_code:subject_code          
            },
            dataType: "json",
            success: function (data) {
                // $('#subject_name').val(data[0].subject_name);
                $('#period').val(parseInt(data[0].theory) + parseInt(data[0].practice));
                $('#theory').val(data[0].theory);
                $('#practice').val(data[0].practice);
                $('#credit').val(data[0].credit);
            }
        });
    }

    $('#subject_code').change(function () { 
        var subject_code = $(this).val();
        if(subject_code === ""){
            $('#subject_name').val("");
            $('#period').val("");
            $('#theory').val("");
            $('#practice').val("");
            $('#credit').val("");
        }else{
            GetSubject(subject_code);
        }
    });

    $('#modalFormSubject').on("submit", function (e) {
        e.preventDefault();
        var action = $('#action').val();
        var subject_code = $('#subject_code').val();
        var dept_code = $('#select_dept').val();
        var group_code =$('#select_group').val();
        var term = $('#select_year').val();
        var plan_id = $('#plan_id').val();
        
        if($('#subject_code').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกวิชา');
            $('#subject_code').select2('open');
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.EducationProgram.php",
                data: { action: action,
                        subject_code: subject_code,
                        dept_code: dept_code,
                        group_code: group_code,
                        term: term,
                        plan_id: plan_id
                },
                dataType: "json", 
                cache: false,
                beforeSend:function(){ 
                    $('.modal-content').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (data) {
                    Swal.fire({
                        type: data.alert.type,
                        title: data.alert.title,
                        confirmButtonText: 'ตกลง'
                    });
                    if(data.alert.type === "success"){
                        LoadDataTable(dept_code,group_code,term);
                        $('#modalSubject').modal('hide');
                        $('#modalFormSubject')[0].reset();  
                        $('.loading').remove();
                    }else{
                        $('.loading').remove();
                    }
                },
                error: function(error) {
                    console.error(error.responseText);
                    Swal.fire({
                        type: "error",
                        title: "เกิดข้อผิดผลาด",
                        confirmButtonText: 'ตกลง'
                    });
                    $('#modalSubject').modal('hide');
                    $('#modalFormSubject')[0].reset();  
                    $('.loading').remove();
                }
            });
        }
        
    });
    
    $(document).on('click', '.btn-delete', function(){
        var dept_code = $('#select_dept').val();
        var group_code =$('#select_group').val();
        var term = $('#select_year').val();

        var plan_id = $(this).data('id');
        Swal.fire({
            title: 'คุณต้องการลบวิชานี้?',
            text: "เมื่อลบแล้วไม่สามารถกู้ข้อมูลคืนได้!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน!',
            cancelButtonText: 'ยกเลิก'
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "CRUD.EducationProgram.php",
                    data: { plan_id:plan_id,
                            action:"Delete"
                    },
                    dataType: "json",
                    beforeSend:function(){ 
                        Swal.fire({
                            type: 'info',
                            title: 'กำลังประมวลผล',
                            confirmButtonText: 'กรุณารอสักครู่. . .'
                        });
                    },
                    success: function (result) {
                        Swal.fire({
                            type: result.alert.type,
                            title: result.alert.title,
                            confirmButtonText: 'ตกลง'
                        });
                        LoadDataTable(dept_code,group_code,term);
                    },
                    error: function(error) {
                        console.error(error.responseText);
                        Swal.fire({
                            type: "error",
                            title: "เกิดข้อผิดผลาด",
                            confirmButtonText: 'ตกลง'
                        });
                        $('#modalSubject').modal('hide');
                        $('.loading').remove();
                    }
                });
            }
        });
    });
});