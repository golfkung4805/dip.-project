$(document).ready(function () {
    // function LoadDataTable(getParamDept){
    //     var TableTeacher = $('#TableTeacher').DataTable({
    //         "retrieve": true,
    //         "lengthChange": true,
    //         "searching": true,
    //         "info": true,
    //         // "sScrollX": true,
    //         // "sScrollXInner": "100%",

    //         "processing": true,
    //         "serverSide":true,
    //         "ajax":{
    //             url: "CRUD.Teacher.php",
    //             data: { action:"LoadTableTeacher" },
    //             type: "POST",
    //         },
    //         error: function(error) {
    //             console.error(error.responseText);
    //         }
    //     });
    // }

    function LoadDataTable(getParamDept){

        $('#TableTeacher').DataTable().destroy();

        if(getParamDept!=="New"){
            var TableTeacher = $('#TableTeacher').DataTable({
                "retrieve": true,
                "lengthChange": true,
                "searching": true,
                "info": true,
                // "sScrollX": true,
                // "sScrollXInner": "100%",
                

                "processing": true,
                "serverSide":true,
                "ajax":{
                    url: "CRUD.Teacher.php",
                    data: { action:"LoadTableTeacher",
                            dept: getParamDept 
                    },
                    type: "POST",
                }
            });
        
            // var TableStudent = $('#TableStudent').DataTable({
            //     "retrieve": true,
            //     "lengthChange": true,
            //     "searching": true,
            //     "info": true,
            //     "sScrollX": true,
            //     "sScrollXInner": "100%",
            // });

            $('#TableTeacher_filter').append('<button class="btn btn-primary btn-insert ml-3">เพิ่มอาจารย์</button>');

            TableTeacher.ajax.reload();
            // TableStudent.ajax.reload();
            
            $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
        }else{
            $('#TableTeacher tbody').empty(); // TODO ทำให้ข้อมูลใน tbody ว่าง
            $('#TableTeacher').DataTable({
                "ordering": false,
                "lengthMenu": [[ -1 ], [ "All" ]],
                "data": [] // TODO กำหนดให้ข้อมูลในตารางว่าง
            });
            $('.sorting_asc').removeClass('sorting_asc'); // TODO ลบ class sorting_asc (ตัวเรียงมากไปน้อย-น้อยไปมาก) ทั้งหมดออก
            $('.sorting_disabled').removeAttr('tabindex'); // TODO ลบ attr tabindex (ตอนเอาเมาส์ไปชี้ที่ เรียงมากไปน้อย-น้อยไปมาก) ทั้งหมดออก
            $('.form-control-sm').attr("disabled","disabled");
            $('.dataTables_empty').html('<div class="text-center"><p style="font-size: 24px;">กรุณาเลือกแผนกวิชา</p></div><i class="fas fa-search fa-3x" style="display: block; text-align: -webkit-center; margin-bottom: 15px;"></i>');
        
            $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
        }
    }

    function select2dept_reset(){
        var GetDepartment = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.Teacher.php",
                data: { action:"GetDepartmentAll",
                        //dept_code:dept_code        
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                },
                error: function(error) {
                    console.error(error.responseText);
                }
            });
            
            return data;
        }();

        $('.select_dept').empty();
        $('.select_dept').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนกวิชา",
            allowClear: true,
            data: GetDepartment
        });
    }

    function select2reset(dept_code){
        var GetDepartment = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.Teacher.php",
                data: { action:"GetDepartment",dept_code:dept_code },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();

        // var GetGender = [
        //     {
        //        id: null,
        //        text: null
        //     },
        //     {
        //         id: "นาย",
        //         text: "นาย"
        //     },
        //     {
        //         id: "นาง",
        //         text: "นาง"
        //     },
        //     {
        //         id: "นางสาว",
        //         text: "นางสาว"
        //     },
        //     {
        //         id: "ว่าที่ร้อยตรี",
        //         text: "ว่าที่ร้อยตรี"
        //     },
        //     {
        //         id: "ว่าที่ร้อยตรีหญิง",
        //         text: "ว่าที่ร้อยตรีหญิง"
        //     }
        // ]

        $(".select-prefix-teacher").select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกคำนำหน้าชื่อ",
            allowClear: true,
            // data: GetGender
        });
        
        // เช็คว่าแผนกแสดงทั้งหมดไหมถ้าแสดงทั้งหมดให้เลือกแผนกของครูได้
        // if($('.select_dept').val()=="ALL"){
        //     sta = false;
        // }else{
        //     sta = true;
        //     // sta = false;
        // }
        // if($('#action').val()=="Update"){
        //     sta = true;
        // }
        // if(dept_code == "" || dept_code == null){
        //     sta = false;
        // }
        $('.select-department-teacher').empty();
        $('.select-department-teacher').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนก",
            allowClear: true,
            disabled: false,
            data: GetDepartment
        });
    }



    select2dept_reset();
    LoadDataTable("New");
    
    $('#select_dept').change(function () {
        if($(this).val()!==""){
            getParamDept = $(this).val();
            LoadDataTable(getParamDept);
        }else{
            LoadDataTable("New");
        }
    });

    $('.select-department-teacher').change(function () {
        $('#get_dept_code').val($(this).val());
    });

    $(document).on('click', '.btn-insert', function(){

        $('#code_teach').removeAttr("readonly");
        $('#action').val("Insert");
        $('#modal-title').text('เพิ่มข้อมูลอาจารย์');
        // $('#code_teach').removeAttr('disabled');
        $('#modalFormTeacher')[0].reset();
        select2reset($('.select_dept').val()); // TODO ใช้ฟังก์ชั่นให้ select2 ไม่ให้จำค่าเดิม
        $('#get_dept_code').val($('#dept_code').val());
        $('#modalTeacher').modal('toggle');
    });

    $(document).on('click', '.btn-update', function(){  
        var teacher_id = $(this).data('id');
        
        $('#code_teach').attr("readonly", "readonly");
        $('#modal-title').text('แก้ไขข้อมูลอาจารย์');
        $('#action').val("Update");
        // $('#code_teach').attr('disabled', 'disabled');
        $.ajax({
            type: "POST",
            url:"CRUD.Teacher.php",
            data: { teacher_id:teacher_id,
                    action:"getDetailTeacher"
            },
            dataType: "json",
            success: function (data) {
                // console.log(data);
                $('#code_teach').val(data[0].teacher_code);
                $('#prefix_teach').val(data[0].gender);
                $('#first_name_teach').val(data[0].first_name);
                $('#last_name_teach').val(data[0].last_name);
                $('#id_teacherr').val(data[0].teacher_id);
                select2reset(data[0].dept_code); // TODO ใช้ฟังก์ชั่นให้ select2 ไม่ให้จำค่าเดิม
                $('#get_dept_code').val(data[0].dept_code);
            },
            error: function(error) {
                console.error(error.responseText);
                Swal.fire({
                    type: "error",
                    title: "เกิดข้อผิดผลาด",
                    confirmButtonText: 'ตกลง'
                });
                $('#modalTeacher').modal('hide');
            }
        });
        $('#modalTeacher').modal('toggle');
    });

    $('#modalFormTeacher').on("submit", function (e) {
        e.preventDefault();
        if($('#code_teach').val().trim()===""){
            ToastAlert('warning','กรุณากรอกรหัสอาจารย์');
            $('#code_teach').focus();
        }else if(isNaN($('#code_teach').val())){
            ToastAlert('warning','กรุณากรอกรหัสอาจารย์เป็นตัวเลขเท่านั้น');
            $('#code_teach').focus();
        }else if($('#code_teach').val().length > 11 ){
            ToastAlert('warning','กรอกตัวเลขได้สูงสุด 11 ตัวเท่านั้น');
            $('#code_teach').focus();
        }else if($('#prefix_teach').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกคำนำหน้า');
            $('#prefix_teach').select2('open');
        }else if($('#first_name_teach').val().trim()===""){
            ToastAlert('warning','กรุณากรอกชื่ออาจารย์');
            $('#first_name_teach').focus();
        }else if($('#first_name_teach').val().length > 100 ){
            ToastAlert('warning','กรอกตัวอักษรได้สูงสุด 100 ตัวเท่านั้น');
            $('#first_name_teach').focus();
        }else if($('#last_name_teach').val().trim()===""){
            ToastAlert('warning','กรุณากรอนามสกุลอาจารย์');
            $('#last_name_teach').focus();
        }else if($('#last_name_teach').val().length > 100 ){
            ToastAlert('warning','กรอกตัวอักษรได้สูงสุด 100 ตัวเท่านั้น');
            $('#last_name_teach').focus();
        }else if($('#dept_code').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกแผนกวิชา');
            $('#dept_code').select2('open');
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.Teacher.php",
                data: new FormData(this),
                dataType: "json", 
                processData: false,
                contentType: false,
                cache: false,
                beforeSend:function(){ 
                    $('.modal-content').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (data) {
                    Swal.fire({
                        type: data.alert.type,
                        title: data.alert.title,
                        confirmButtonText: 'ตกลง'
                    });
                    if(data.alert.type === "success"){
                        LoadDataTable($('.select_dept').val());
                        $('#modalTeacher').modal('hide');
                        $('#modalFormTeacher')[0].reset();  
                        $('.loading').remove();
                    }else{
                        $('.loading').remove();
                    }
                    
                },
                error: function(error) {
                    console.error(error.responseText);
                    Swal.fire({
                        type: "error",
                        title: "เกิดข้อผิดผลาด",
                        confirmButtonText: 'ตกลง'
                    });
                    $('#modalTeacher').modal('hide');
                    $('#modalFormTeacher')[0].reset();  
                    $('.loading').remove();
                }
            });
        }
        
    });

    $(document).on('click', '.btn-delete', function(){
        var id_teacher = $(this).data('id');
        Swal.fire({
            title: 'คุณต้องการลบอาจารย์ท่านนี้?',
            text: "เมื่อลบแล้วไม่สามารถกู้ข้อมูลคืนได้!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน!',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "CRUD.Teacher.php",
                    data: { id_teacher:id_teacher,
                            action:"Delete"
                    },
                    dataType: "json",
                    beforeSend:function(){ 
                        Swal.fire({
                            type: 'info',
                            title: 'กำลังประมวลผล',
                            confirmButtonText: 'กรุณารอสักครู่. . .'
                        });
                    },
                    success: function (result) {
                        Swal.fire({
                            type: result.alert.type,
                            title: result.alert.title,
                            confirmButtonText: 'ตกลง'
                        });
                        LoadDataTable($('.select_dept').val());
                    },
                    error: function(error) {
                        console.error(error.responseText);
                        Swal.fire({
                            type: "error",
                            title: "เกิดข้อผิดผลาด",
                            confirmButtonText: 'ตกลง'
                        });
                        $('#modalTeacher').modal('hide');
                        $('.loading').remove();
                    }
                });
            }
        });
    });
});