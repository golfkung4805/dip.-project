$(document).ready(function () {
    function LoadDataTable(action){
        TableDepartment = $('#TableDepartment').DataTable({
            "retrieve": true,
            "lengthChange": true,
            "searching": true,
            "info": true,
            // "sScrollX": true,
            // "sScrollXInner": "100%",

            "processing": true,
            "serverSide": true,
            "ajax":{
                url: "CRUD.Department.php",
                data: { action:"LoadTableDepartment" },
                type: "POST",
            }
        });
        if(action==="New"){
            $('#TableDepartment_filter').append('<button class="btn btn-primary btn-insert ml-3">เพิ่มแผนก</button>');
        }
        TableDepartment.ajax.reload();
        
        $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
    }

    LoadDataTable("New");

    function select2reset(teacher_code){
        var GetTeacher = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.Department.php",
                data: { action:"GetTeacher",
                        teacher_code:teacher_code        
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();

        $('.select-chief-department').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกหัวหน้าแผนก",
            allowClear: true,
            data: GetTeacher
        });
    }

    $(document).on('click', '.btn-insert', function(){
        $('#action').val("Insert");  
        $('#code_dep').removeAttr("readonly");
        $('#modal-title').text('เพิ่มข้อมูลแผนก');
        $('#modalDepartment').modal('toggle');
        $('#modalFormDepartment')[0].reset();
        select2reset("INSERT");
    });

    $(document).on('click', '.btn-update', function(){  
        var dept_id = $(this).data("id");
        $('#code_dep').attr("readonly", "readonly");
        $('#action').val("Update");
        $('#modal-title').text('แก้ไขข้อมูลแผนก');
        $.ajax({
            type: "POST",
            url:"CRUD.Department.php",
            data: { dept_id:dept_id,
                action:"getDetailDepartment"
            },
            dataType: "json",
            success: function (data) {
                $('#code_dep').val(data[0].dept_code);
                $('#name_dep').val(data[0].dept_name);
                $('#chief_dep').val(data[0].teacher_code);
                $('#id_dep').val(data[0].dept_id);
                select2reset(data[0].teacher_code);
            },
            error: function(error) {
                console.error(error.responseText);
                Swal.fire({
                    type: "error",
                    title: "เกิดข้อผิดผลาด",
                    confirmButtonText: 'ตกลง'
                });
                $('#modalTeacher').modal('hide');
            }
        });
        $('#modalDepartment').modal('toggle');
    });

    $('#modalFormDepartment').on("submit",function (event) { 
        event.preventDefault();

        if($('#code_dep').val().trim()===""){
            ToastAlert('warning','กรุณากรอกรหัสแผนก');
            $('#code_dep').focus();
        }else if(isNaN($('#code_dep').val())){
            ToastAlert('warning','กรุณากรอกรหัสแผนกเป็นตัวเลขเท่านั้น');
            $('#code_dep').focus();
        }else if($('#code_dep').val().length > 11 ){
            ToastAlert('warning','กรอกรหัสแผนกได้สูงสุด 11 ตัวเท่านั้น');
            $('#code_dep').focus();
        }else if($('#name_dep').val().trim()===""){
            ToastAlert('warning','กรุณากรอกชื่อแผนก');
            $('#name_dep').focus();
        }else if($('#name_dep').val().length > 100 ){
            ToastAlert('warning','กรอกตัวอักษรได้สูงสุด 100 ตัวเท่านั้น');
            $('#name_dep').focus();
        }else if($('#chief_dep').val().trim()===""){
            ToastAlert('warning','กรุณาเลือกหัวหน้าแผนก');
            $('#chief_dep').select2('open');
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.Department.php",
                data: new FormData(this),
                dataType: "json", 
                processData: false,
                contentType: false,
                cache: false,
                beforeSend:function(){ 
                    $('.modal-content').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (data) {
                    Swal.fire({
                        type: data.alert.type,
                        title: data.alert.title,
                        confirmButtonText: 'ตกลง'
                    })
                    if(data.alert.type === "success"){
                        LoadDataTable();
                        $('#modalDepartment').modal('hide');
                        $('#modalFormDepartment')[0].reset();
                        $('.loading').remove();
                    }else{
                        $('.loading').remove();
                    }
                },
                error: function(){
                    console.error(error.responseText);
                    Swal.fire({
                        type: "error",
                        title: "เกิดข้อผิดผลาด",
                        confirmButtonText: 'ตกลง'
                    });
                    $('#modalTeacher').modal('hide');
                    $('#modalFormDepartment')[0].reset();
                    $('.loading').remove();
                }
            });
        }
        
    });

    $(document).on('click', '.btn-delete', function(){
        var dept_id = $(this).data('id');
        Swal.fire({
            title: 'คุณต้องการลบแผนกนี้?',
            text: "เมื่อลบแล้วไม่สามารถกู้ข้อมูลคืนได้!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน!',
            cancelButtonText: 'ยกเลิก'
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "CRUD.Department.php",
                    data: { dept_id:dept_id,
                            action:"Delete"
                    },
                    dataType: "json",
                    beforeSend:function(){ 
                        Swal.fire({
                            type: 'info',
                            title: 'กำลังประมวลผล',
                            confirmButtonText: 'กรุณารอสักครู่. . .'
                        });
                    },
                    success: function (result) {
                        Swal.fire({
                            type: result.alert.type,
                            title: result.alert.title,
                            confirmButtonText: 'ตกลง'
                        });
                        LoadDataTable();
                    },
                    error: function(error) {
                        console.error(error.responseText);
                        Swal.fire({
                            type: "error",
                            title: "เกิดข้อผิดผลาด",
                            confirmButtonText: 'ตกลง'
                        });
                        $('#modalTeacher').modal('hide');
                        $('.loading').remove();
                    }
                });
            }
        });
    });
});