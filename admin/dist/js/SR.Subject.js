$(document).ready(function () {
    function LoadDataTable(action){
        var TableSubject = $('#TableSubject').DataTable({
            "retrieve": true,
            "lengthChange": true,
            "searching": true,
            "info": true,
            // "sScrollX": true,
            // "sScrollXInner": "100%",
    
            "processing": true,
            "serverSide":true,
            "ajax":{
                url:"CRUD.Subject.php",
                data:{action:"LoadTableSubject"},
                type:"POST",
            }
        });
        if(action==="New"){
            $('#TableSubject_filter').append('<button class="btn btn-primary btn-insert ml-3">เพิ่มวิชาเรียน</button>');
        }
        TableSubject.ajax.reload();

        $('.dataTables_wrapper .row:eq(1)').addClass('overflow-auto');
    }

    LoadDataTable("New");
    
    $(document).on('click', '.btn-insert', function(){
        $('#action').val("Insert");  
        
        $('#code_subject').removeAttr("readonly");
        $('#modal-title').text('เพิ่มข้อมูลวิชาเรียน');
        $('#modalSubject').modal('toggle');
        $('#modalFormSubject')[0].reset();
    });

    $(document).on('click', '.btn-update', function(){  
        var id_subject = $(this).data('id');
        $('#code_subject').attr("readonly", "readonly");
        $('#modal-title').text('แก้ไขข้อมูลวิชาเรียน');
        $('#action').val("Update");
        $.ajax({
            type: "POST",
            url: "CRUD.Subject.php",
            data: { id_subject:id_subject,
                    action:"getDetailSubject",
            },
            dataType: "json",
            success: function (data) {
                // console.log(data);
                if(data.length === 0){
                    $('#modalFormSubject')[0].reset();
                    $('#id_subject').val("Error");
                    $('#modalSubject').modal('hide');
                    console.error("Error");
                    Swal.fire({
                        type: "error",
                        title: "เกิดข้อผิดผลาด",
                        confirmButtonText: 'ตกลง'
                    });
                }else{
                    // console.log(data);
                    $('#code_subject').val(data[0].subject_code);
                    $('#name_subject').val(data[0].subject_name);
                    $('#theory_subject').val(data[0].theory);
                    $('#practice_subject').val(data[0].practice);
                    $('#credit_subject').val(data[0].credit);
                    $('#id_subject').val(data[0].subject_id);
                }
                $('#modalSubject').modal('toggle');
            },
            error: function(error) {
                console.error(error.responseText);
                Swal.fire({
                    type: "error",
                    title: "เกิดข้อผิดผลาด",
                    confirmButtonText: 'ตกลง'
                });

                $('#modalSubject').modal('hide');
            }
        });
    });

    $('#modalFormSubject').on("submit", function (e) {
        e.preventDefault();

        if($('#code_subject').val().trim()===""){
            ToastAlert('warning','กรุณากรอกรหัสวิชาเรียน');
            $('#code_subject').focus();
        }else if($('#code_subject').val().length > 11 ){
            ToastAlert('warning','กรอกตัวอักษรได้สูงสุด 11 ตัวเท่านั้น');
            $('#code_subject').focus();
        }else if($('#name_subject').val().trim()===""){
            ToastAlert('warning','กรุณากรอกชื่อวิชาเรียน');
            $('#name_subject').focus();
        }else if($('#name_subject').val().length > 100 ){
            ToastAlert('warning','กรอกตัวอักษรได้สูงสุด 100 ตัวเท่านั้น');
            $('#name_subject').focus();
        }else if($('#theory_subject').val().trim()===""){
            ToastAlert('warning','กรุณากรอกจำนวนทฤษฏี');
            $('#theory_subject').focus();
        }else if(isNaN($('#theory_subject').val())){
            ToastAlert('warning','กรุณากรอกจำนวนทฤษฏีเป็นตัวเลขเท่านั้น');
            $('#theory_subject').val('');
            $('#theory_subject').focus();
        }else if($('#theory_subject').val().length > 1 ){
            ToastAlert('warning','จำนวนทฤษฏีกรอกได้สูงสุด 1 ตัวเท่านั้น');
            $('#theory_subject').focus();
        }else if($('#practice_subject').val().trim()===""){
            ToastAlert('warning','กรุณากรอกจำนวนปฏิบัติ');
            $('#practice_subject').focus();
        }else if(isNaN($('#practice_subject').val())){
            ToastAlert('warning','กรุณากรอกจำนวนปฏิบัติเป็นตัวเลขเท่านั้น');
            $('#practice_subject').val('');
            $('#practice_subject').focus();
        }else if($('#practice_subject').val().length > 1 ){
            ToastAlert('warning','จำนวนปฏิบัติกรอกได้สูงสุด 1 ตัวเท่านั้น');
            $('#practice_subject').focus();
        }else if($('#credit_subject').val().trim()===""){
            ToastAlert('warning','กรุณากรอกจำนวนหน่วยกิต');
            $('#credit_subject').focus();
        }else if(isNaN($('#credit_subject').val())){
            ToastAlert('warning','กรุณากรอกจำนวนหน่วยกิตเป็นตัวเลขเท่านั้น');
            $('#credit_subject').val('');
            $('#credit_subject').focus();
        }else if($('#credit_subject').val().length > 1 ){
            ToastAlert('warning','จำนวนหน่วยกิตกรอกได้สูงสุด 1 ตัวเท่านั้น');
            $('#credit_subject').focus();
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.Subject.php",
                data: new FormData(this),
                dataType: "json", 
                processData: false,
                contentType: false,
                cache: false,
                beforeSend:function(){ 
                    $('.modal-content').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (data) {
                    Swal.fire({
                        type: data.alert.type,
                        title: data.alert.title,
                        confirmButtonText: 'ตกลง'
                    });
                    if(data.alert.type === "success"){
                        LoadDataTable();
                        $('#modalSubject').modal('hide');
                        $('#modalFormSubject')[0].reset();  
                        $('.loading').remove();
                    }else{
                        $('.loading').remove();
                    }
                    
                },
                error: function(error) {
                    console.error(error.responseText);
                    Swal.fire({
                        type: "error",
                        title: "เกิดข้อผิดผลาด",
                        confirmButtonText: 'ตกลง'
                    });
                    $('#modalSubject').modal('hide');
                    $('#modalFormSubject')[0].reset();  
                    $('.loading').remove();
                }
            });
        }
    });
    
    $(document).on('click', '.btn-delete', function(){
        var id_subject = $(this).data('id');
        Swal.fire({
            title: 'คุณต้องการลบวิชานี้?',
            text: "เมื่อลบแล้วไม่สามารถกู้ข้อมูลคืนได้!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน!',
            cancelButtonText: 'ยกเลิก'
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "CRUD.Subject.php",
                    data: { id_subject:id_subject,
                            action:"Delete"
                    },
                    dataType: "json",
                    beforeSend:function(){ 
                        Swal.fire({
                            type: 'info',
                            title: 'กำลังประมวลผล',
                            confirmButtonText: 'กรุณารอสักครู่. . .'
                        });
                    },
                    success: function (result) {
                        Swal.fire({
                            type: result.alert.type,
                            title: result.alert.title,
                            confirmButtonText: 'ตกลง'
                        });
                        LoadDataTable();
                    },
                    error: function(error) {
                        console.error(error.responseText);
                        Swal.fire({
                            type: "error",
                            title: "เกิดข้อผิดผลาด",
                            confirmButtonText: 'ตกลง'
                        });
                        $('#modalSubject').modal('hide');
                    }
                });
            }
        });
    });
});