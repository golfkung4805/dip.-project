<?php
    switch ($view) {
        case 'ChartStudent':
            //echo "<script src=\"dist/js/SR.Js.js\"></script>\">";
            break;
        case 'Department':
            echo "<script src=\"dist/js/SR.Department.js\"></script>";
            break;
        case 'Teacher':
            echo "<script src=\"dist/js/SR.Teacher.js\"></script>";
            break;
        case 'StudyGroup':
            echo "<script src=\"dist/js/SR.StudyGroup.js\"></script>";
            break;
        case 'Subject':
            echo "<script src=\"dist/js/SR.Subject.js\"></script>";
            break;
        case 'Room':
            echo "<script src=\"dist/js/SR.Room.js\"></script>";
            break;
        case 'EducationProgram':
            echo "<script src=\"dist/js/SR.EducationProgram.js\"></script>";
            break;
        case 'StudentSchedule':
            echo "<script src=\"dist/js/SR.StudentSchedule.js\"></script>";
            break;
        case 'PositionSetting':
            echo "<script src=\"dist/js/SR.PositionSetting.js\"></script>";
            break;
        case 'TimeSetting':
            echo "<script src=\"dist/js/SR.TimeSetting.js\"></script>";
            echo "\n\t";
            echo "<!-- InputMask -->";
            echo "\n\t";
            echo "<script src=\"plugins/moment/moment.min.js\"></script>";
            echo "\n\t";
            echo "<script src=\"plugins/inputmask/min/jquery.inputmask.bundle.min.js\"></script>";
            echo "\n\t";
            echo "<!-- date-range-picker -->";
            echo "\n\t";
            echo "<script src=\"plugins/daterangepicker/daterangepicker.js\"></script>";
            echo "\n\t";
            echo "<!-- Tempusdominus Bootstrap 4 -->";
            echo "\n\t";
            echo "<script src=\"plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js\"></script>";
            break;
        default:
            //echo "<link rel=\"stylesheet\" href=\"dist/css/SR.StudentSchedule.css\">";
    }
?>
