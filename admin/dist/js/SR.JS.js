function ToastAlert(type, title){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 5000
    });

    Toast.fire({
        type: type,
        title: title,
    })
}

function SweetAlert(type, title, text, button, footer){
    Swal.fire({
        type: type,
        title: title,
        text: text,
        confirmButtonText: button,
        footer: footer
    });
}

$(document).ready(function () {
    $(document).on('click', '.btn-change-password', function(){
        $('#modalFormChangePassword')[0].reset();
        $('#modalChangePassword').modal('toggle');
    });

    $('#modalFormChangePassword').on("submit", function (e) {
        e.preventDefault();
        var id_teacher = $('.btn-change-password').data('id');
        var old_password = $('#old_password').val();
        var new_password = $('#new_password').val();
        var confirm_password = $('#confirm_password').val();

        if($('#old_password').val().trim()===""){
            ToastAlert('warning','กรุณากรอกรหัสผ่านเดิม');
            $('#old_password').focus();
        }else if($('#new_password').val().trim()===""){
            ToastAlert('warning','กรุณากรอกรหัสผ่านใหม่');
            $('#new_password').focus();
        }else if($('#new_password').val().length < 8){
            ToastAlert('warning','รหัสผ่านควรมี 8 ตัวขึ้นไป');
            $('#new_password').focus();
        }else if($('#confirm_password').val().trim()===""){
            ToastAlert('warning','กรุณากรอกยืนยันรหัสผ่าน');
            $('#confirm_password').focus();
        }else if($('#new_password').val() !== $('#confirm_password').val()){
            ToastAlert('warning','รหัสผ่านใหม่หรือยืนยันรหัสผ่านไม่ตรงกัน');
            $('#confirm_password').focus();
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.Index.php",
                data: {
                    action:"ChangePassword",
                    id_teacher:id_teacher,
                    old_passwd:old_password,
                    new_passwd:new_password,
                    confirm_passwd:confirm_password
                },
                dataType: "json", 
                // processData: false,
                // contentType: false,
                cache: false,
                beforeSend:function(){ 
                    $('.modal-content').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (data) {
                    if(data.alert.type === "success"){
                        SweetAlert(data.alert.type, data.alert.title, null, 'ตกลง', null);
                        $('#modalChangePassword').modal('hide');
                        $('#modalFormChangePassword')[0].reset();  
                        $('.loading').remove();
                    }else{
                        SweetAlert(data.alert.type, data.alert.title, null, 'ตกลง', null);
                        $('.loading').remove();
                    }
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                    // $('#modalChangePassword').modal('hide');
                    // $('#modalFormChangePassword')[0].reset();  
                    $('.loading').remove();
                }
            });
        }
    });

    $(document).on('click', '.btn-logout', function(){
        Swal.fire({
            title: 'คุณต้องการออกจากระบบหรือไม่?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ออกจากระบบ',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "CRUD.Index.php",
                    data: { action:"Logout" },
                    // dataType: "json",
                    beforeSend:function(){ 
                        SweetAlert('info', 'กำลังออกจากระบบ', null, 'กรุณารอสักครู่. . .', null);
                    },
                    success: function (result) {
                        Cookies.remove('CK_Chack_Status');
                        Cookies.remove('SchCK_dept_code');
                        Cookies.remove('SchCK_group_code');
                        Cookies.remove('SchCK_term');
                        Cookies.remove('EProCK_dept_code');
                        Cookies.remove('EProCK_group_code');
                        Cookies.remove('EProCK_term');

                        window.location="http://localhost/SR/";
                    },
                    error: function(error) {
                        console.error(error.responseText);
                        SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                    }
                });
            }
        });
    });

    if(typeof Cookies.get('CK_Chack_Status')==="undefined"){
        $.ajax({
            type: "POST",
            url: "CRUD.Index.php",
            data: { action:"Logout" },
            // dataType: "json",
            beforeSend:function(){ 
                SweetAlert('info', 'กำลังออกจากระบบ', null, 'กรุณารอสักครู่. . .', null);
            },
            success: function (result) {
                Cookies.remove('CK_Chack_Status');
                Cookies.remove('SchCK_dept_code');
                Cookies.remove('SchCK_group_code');
                Cookies.remove('SchCK_term');

                window.location="http://localhost/SR/?view=Login";
            }
        });
    }

});