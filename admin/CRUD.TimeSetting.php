<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    if($action==="Update"){
        $time_start = filter_input(INPUT_POST,"time_start");
        $time_end = filter_input(INPUT_POST,"time_end");
        $time_class = filter_input(INPUT_POST,"time_class");
        $time_id = filter_input(INPUT_POST,"time_id");
        
        $sub_time = explode(":",$time_class);
        $hours = $sub_time[0]*60;
        $time_class = $hours+$sub_time[1];

        $sql = "UPDATE tb_time_setting SET time_start = :time_start, time_end = :time_end, time_class = :time_class WHERE time_id = :time_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':time_start',$time_start,PDO::PARAM_STR);
        $stmt->bindValue(':time_end',$time_end,PDO::PARAM_STR);
        $stmt->bindValue(':time_class',$time_class,PDO::PARAM_STR);
        $stmt->bindValue(':time_id',$time_id,PDO::PARAM_INT);
        $stmt->execute();
        $stmt = null;

        $alert = array("type"=>"success", "title"=>"เปลี่ยนตารางเวลาสำเร็จ"); 
        echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
        exit;
    }


?>