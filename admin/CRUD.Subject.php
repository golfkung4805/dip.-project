<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }
    
    // $code_subject = "a1a";
    // $name_subject = "a1";
    // $theory_subject = "a1";
    // $practice_subject = "a1";
    // $credit_subject = "a1";

    // $sql = "INSERT INTO tb_subject (subject_code, subject_name, theory, practice, credit) VALUE(:code_subject, :name_subject, :theory_subject, :practice_subject, :credit_subject)";
    // $stmt = $pdo->prepare($sql);
    // $stmt->bindValue('code_subject',$code_subject,PDO::PARAM_STR);
    // $stmt->bindValue('name_subject',$name_subject,PDO::PARAM_STR);
    // $stmt->bindValue('theory_subject',$theory_subject,PDO::PARAM_STR);
    // $stmt->bindValue('practice_subject',$practice_subject,PDO::PARAM_STR);
    // $stmt->bindValue('credit_subject',$credit_subject,PDO::PARAM_STR);
    // $stmt->execute();
    

    function loadTableSubject(){
        // $con=mysqli_connect('localhost','root','','db_testing');
        global $pdo;

        $request=$_POST;
        $col =array(
            0   =>  'subject_id',
            1   =>  'subject_code',
            2   =>  'subject_name',
            3   =>  'theory-practice-credit',
            4   =>  'subject_id'
        );  //create column LIKE table in database

        //Search
        $sql = "SELECT * FROM tb_subject 
                WHERE 1=1";
        if(!empty($request['search']['value'])){
            $sql.=" AND (subject_id LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR subject_code LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR subject_name LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR CONCAT(theory, '-' ,practice, '-' ,credit) LIKE '%".$request['search']['value']."%' )";
        }
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $totalData = $stmt->rowCount();
        $totalFilter=$totalData;
        $stmt = null;
        //Order
        $sql.=" ORDER BY ".$col[$request['order'][0]['column']]."   ".$request['order'][0]['dir']."  LIMIT ".
            $request['start']."  ,".$request['length']."  ";

        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $data=array();
        $count = $request['start']+1;
        foreach($stmt as $row){
            
            $subdata=array();
            // $subdata[]=$row['id']; //id
            $subdata[]="<div class=\"text-center\">".$count."</div>";
            $subdata[]="<div class=\"text-center\">".$row['subject_code']."</div>"; //subject_code
            $subdata[]="<div class=\"text-left\">".$row['subject_name']."</div>"; //subject_name
            $subdata[]="<div class=\"text-center\">".$row['theory']."-".$row['practice']."-".$row['credit']."</div>"; //theory- practice-credit               $row[0] is id in table on database
            $subdata[]="<div class=\"text-center\"><button class=\"btn btn-outline-warning btn-update mr-3\" data-id=\"$row[subject_id]\">แก้ไข</button> <button class=\"btn btn-outline-danger btn-delete\" data-id=\"$row[subject_id]\">ลบ</button></div>";
            $data[]=$subdata;
            $count++;
        }

        $json_data=array(
            "draw"              =>  intval($request['draw']),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalFilter),
            "data"              =>  $data
        );
        return $json_data;
    }

    if($action==="LoadTableSubject"){
        $dataTable = loadTableSubject();
        echo json_encode($dataTable,JSON_UNESCAPED_UNICODE ); 
        exit;
    }

    if($action==="getDetailSubject"){
        $id_subject = filter_input(INPUT_POST,'id_subject');
        $sql = "SELECT * FROM tb_subject 
                WHERE subject_id = :id_subject";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':id_subject',$id_subject,PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt = null;

        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="Insert"){
        $code_subject = filter_input(INPUT_POST,'code_subject');
        $name_subject = filter_input(INPUT_POST,'name_subject');
        $theory_subject = filter_input(INPUT_POST,'theory_subject');
        $practice_subject = filter_input(INPUT_POST,'practice_subject');
        $credit_subject = filter_input(INPUT_POST,'credit_subject');

        $sql = "SELECT subject_code 
                FROM tb_subject 
                WHERE subject_code = :subject_code";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':subject_code',$code_subject,PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $alert = array("type"=>"warning", "title"=>"รหัสวิชา $code_subject มีในระบบแล้ว"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $sql = "INSERT INTO tb_subject (subject_code, subject_name, theory, practice, credit) 
                    VALUE(:code_subject, :name_subject, :theory_subject, :practice_subject, :credit_subject)";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('code_subject',$code_subject,PDO::PARAM_STR);
            $stmt->bindValue('name_subject',$name_subject,PDO::PARAM_STR);
            $stmt->bindValue('theory_subject',$theory_subject,PDO::PARAM_STR);
            $stmt->bindValue('practice_subject',$practice_subject,PDO::PARAM_STR);
            $stmt->bindValue('credit_subject',$credit_subject,PDO::PARAM_STR);
            $stmt->execute();
            $stmt = null;

            $alert = array("type"=>"success", "title"=>"เพิ่มวิชาเรียนสำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }
    
    if($action==="Update"){
        // $code_subject = filter_input(INPUT_POST,'code_subject');
        $name_subject = filter_input(INPUT_POST,'name_subject');
        $theory_subject = filter_input(INPUT_POST,'theory_subject');
        $practice_subject = filter_input(INPUT_POST,'practice_subject');
        $credit_subject = filter_input(INPUT_POST,'credit_subject');
        $id_subject = filter_input(INPUT_POST,'id_subject');

        if($id_subject=="Error"){
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            // $sql = "SELECT subject_code,
            //         (SELECT count(subject_code) FROM tb_subject WHERE subject_code = :subject_code) as CheckCode
            //         FROM tb_subject 
            //         WHERE subject_id = :subject_id";
            // $stmt = $pdo->prepare($sql);
            // $stmt->bindValue(':subject_code',$code_subject,PDO::PARAM_STR);
            // $stmt->bindValue(':subject_id',$id_subject,PDO::PARAM_INT);
            // $stmt->execute();

            // if($stmt->rowCount() > 0){
            //     $row = $stmt->fetch();
            //     if($row['subject_code'] === $code_subject || $row['CheckCode'] <= 0){
                    $sql = "UPDATE tb_subject SET  subject_name = :name_subject, 
                                                theory = :theory_subject, 
                                                practice = :practice_subject, 
                                                credit = :credit_subject  
                            WHERE subject_id = :id_subject";
                    $stmt = $pdo->prepare($sql);
                    // $stmt->bindValue('code_subject',$code_subject,PDO::PARAM_STR);
                    $stmt->bindValue('name_subject',$name_subject,PDO::PARAM_STR);
                    $stmt->bindValue('theory_subject',$theory_subject,PDO::PARAM_STR);
                    $stmt->bindValue('practice_subject',$practice_subject,PDO::PARAM_STR);
                    $stmt->bindValue('credit_subject',$credit_subject,PDO::PARAM_STR);
                    $stmt->bindValue('id_subject',$id_subject,PDO::PARAM_INT);
                    $stmt->execute();
                    $stmt = null;

                    $alert = array("type"=>"success", "title"=>"แก้ไขวิชาเรียนสำเร็จ"); 
                    echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                    exit;
                // }else{
                //     $alert = array("type"=>"warning", "title"=>"รหัสวิชา $code_subject มีในระบบแล้ว"); 
                //     echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                //     exit;
                // }
            // }else{
            //     $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            //     echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            //     exit;
            // }
        }
    }

    if($action==="Delete"){
        $id_subject = filter_input(INPUT_POST,'id_subject');

        $sql = "SELECT * FROM tb_subject 
                WHERE subject_id = :id_subject";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('id_subject',$id_subject,PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount()===0){
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $sql = "DELETE FROM tb_subject 
                    WHERE subject_id = :id_subject";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('id_subject',$id_subject,PDO::PARAM_INT);
            $stmt->execute();
            $stmt = null;
            
            $alert = array("type"=>"success", "title"=>"ลบวิชาเรียนสำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
        
        
    }
?>