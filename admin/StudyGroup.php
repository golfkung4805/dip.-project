
        <?php include('CRUD.StudyGroup.php'); ?>
        
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> ข้อมูลกลุ่มเรียน </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?php echo $_SERVER_NAME; ?>">หน้าหลัก</a></li>
                                <li class="breadcrumb-item active"> ข้อมูลกลุ่มเรียน </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Modal -->
            <div class="modal fade" id="modalStudyGroup">
                <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content">
                        <form id="modalFormStudyGroup">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modal-title">เพิ่มกลุ่มเรียน</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>รหัสกลุ่มเรียน</label>
                                            <input type="text" class="form-control" id="group_code" name="group_code" placeholder="กรอกรหัสกลุ่ม ...">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>ชื่อย่อกลุ่มเรียน</label>
                                            <input type="text" class="form-control" id="group_subname" name="group_subname" placeholder="กรอกรหัสแผนก ...">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>ชื่อกลุ่มเรียน</label>
                                            <input type="text" class="form-control" id="group_name" name="group_name" placeholder="กรอกชื่อกลุ่มเรียน ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>แผนกวิชา</label>
                                            <select class="form-control select-department" id="dept_code" name="dept_code" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>อาจารย์ที่ปรึกษา</label>
                                            <select class="form-control select-advisors" id="teacher_code" name="teacher_code" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <input type="hidden" id="group_id" name="group_id">
                                <input type="hidden" name="get_dept_code" id="get_dept_code">
                                <input type="hidden" id="action" name="action" value="Insert">
                                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                                <button type="submit" class="btn btn-primary">บันทึกข้อมูลกลุ่มเรียน</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /. Modal -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <?php //if(isset($_GET['dept']) && ChackGet("dept",$_GET['dept'])===TRUE){ ?>

                        <div class="col-lg-12">
                            <div class="col-lg-12 pr-0">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row form-group m-0">
                                            <label class="col-lg-1 col-form-label text-right">แผนก</label>
                                            <div class="col-lg-4">
                                                <select class="form-control select_dept" id="select_dept" name="select_dept" style="width: 100%;">
                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="TableStudyGroup" class="TableStudyGroup table table-bordered table-striped">
                                            <thead>
                                                <tr class="text-center">
                                                    <th class="text-center">ลำดับ</th>
                                                    <th class="text-center">รหัสกลุ่ม</th>
                                                    <th class="text-center">รหัสย่อกลุ่ม</th>
                                                    <th class="text-left">ชื่อกลุ่ม</th>
                                                    <!-- <th>จำนวนนักศึกษา</th> -->
                                                    <th class="text-left">ชื่อครูที่ปรึกษา</th>
                                                    <th class="text-center">ตัวเลือก</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- <tr>
                                                    <td colspan="6" height="70"></td>
                                                </tr> -->
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-center">ลำดับ</th>
                                                    <th class="text-center">รหัสกลุ่ม</th>
                                                    <th class="text-center">รหัสย่อกลุ่ม</th>
                                                    <th class="text-left">ชื่อกลุ่ม</th>
                                                    <!-- <th>จำนวนนักศึกษา</th> -->
                                                    <th class="text-left">ชื่อครูที่ปรึกษา</th>
                                                    <th class="text-center">ตัวเลือก</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div><!-- /.col-lg-12 -->
                        </div>
                        <!-- /.col-lg-9 -->
                        <?php //} ?>
                        
                    </div>
                    <!-- /.row -->
                    
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>

