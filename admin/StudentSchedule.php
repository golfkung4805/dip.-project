
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> ตารางเรียนนักศึกษา </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?php echo $_SERVER_NAME; ?>">หน้าแรก</a></li>
                                <li class="breadcrumb-item active"> ตารางเรียนนักศึกษา </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Modal -->
            <div class="modal fade" id="modalSchedule">
                <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content">
                        <form id="modalFormSchedule">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modal-title">เพิ่มวิชาเรียน</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>วัน</label>
                                            <select class="form-control select-advisors" id="sch_date" name="sch_date" style="width: 100%;">
                                                <option></option>
                                                <option value="1">จันทร์</option>
                                                <option value="2">อังคาร</option>
                                                <option value="3">พุธ</option>
                                                <option value="4">พฤหัสบดี</option>
                                                <option value="5">ศุกร์</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>เริ่มคาบที่</label>
                                            <select class="form-control select-advisors" id="sch_start_time" name="sch_start_time" style="width: 100%;">
                                            
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>จบคาบที่</label>
                                            <select class="form-control select-advisors" id="sch_end_time" name="sch_end_time" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>วิชาเรียน</label>
                                            <select class="form-control select-department" id="subject_code" name="subject_code" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>อาจารย์สอน</label>
                                            <select class="form-control select-advisors" id="teacher_code" name="teacher_code" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>ห้องเรียน</label>
                                            <select class="form-control select-advisors" id="room_code" name="room_code" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <input type="hidden" id="sch_id" name="sch_id">
                                <input type="hidden" id="group_code" name="group_code">
                                <input type="hidden" id="term" name="term">
                                <input type="hidden" id="action" name="action" value="Insert">
                                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                                <button type="submit" class="btn btn-primary">บันทึกข้อมูลกลุ่มเรียน</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /. Modal -->


            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-outline card-primary">
                                <div class="card-header">
                                    <div class="row form-group m-0">
                                        <label class="col-lg-1 col-form-label text-right">แผนก</label>
                                        <div class="col-lg-3">
                                            <select class="form-control select_dept" id="select_dept" name="select_dept" style="width: 100%;" autofocus>
                                                
                                            </select>
                                        </div>
                                        
                                        <label class="col-lg-1 col-form-label text-right">กลุ่มเรียน</label>
                                        <div class="col-lg-3">
                                            <select class="form-control select_group" id="select_group" name="select_group" style="width: 100%;">
                                                
                                            </select>
                                        </div>

                                        <label class="col-lg-1 col-form-label text-right">ปีการศึกษา</label>
                                        <div class="col-lg-3">
                                            <select class="form-control select_year" id="select_year" name="select_year" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card schedule">
                                <?php //include('StudentScheduleSampleTable.php'); ?> 
                                
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card education-program-scroll card-outline card-primary">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-lg-6">จำนวนคาบที่จัดแล้ว : <div style="display: inline-block;" class="perPeriod"> </div></div>
                                        <div class="col-lg-6">จำนวนคาบแผนที่ตั้งไว้ : <div style="display: inline-block;" class="sumPeriod"> </div></div>
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="TableEducationProgram" class="TableEducationProgram table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ลำดับ</th>
                                                <th class="text-center">รหัสวิชา</th>
                                                <th>ชื่อวิชา</th>
                                                <th class="text-center">คาบ</th>
                                                <th class="text-center">หน่วยกิต</th>
                                                <th>ครูผู้สอน</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="text-center">ลำดับ</th>
                                                <th class="text-center">รหัสวิชา</th>
                                                <th>ชื่อวิชา</th>
                                                <th class="text-center">คาบ</th>
                                                <th class="text-center">หน่วยกิต</th>
                                                <th>ครูผู้สอน</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <!-- <div class="card-header">
                                    <h5 class="m-0">รายชื่อนักศึกษา</h5>
                                </div> -->
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="TableSchedule" class="TableSchedule table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-center">วัน</th>
                                                <th class="text-center">เริ่มคาบ</th>
                                                <th class="text-center">จบคาบ</th>
                                                <th>ชื่อผู้สอน</th>
                                                <th class="text-center">รหัสวิชา</th>
                                                <th>ชื่อวิชา</th>
                                                <th class="text-center">ห้องเรียน</th>
                                                <th class="text-center" >ตัวเลือก</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="text-center">วัน</th>
                                                <th class="text-center">เริ่มคาบ</th>
                                                <th class="text-center">จบคาบ</th>
                                                <th>ชื่อผู้สอน</th>
                                                <th class="text-center">รหัสวิชา</th>
                                                <th>ชื่อวิชา</th>
                                                <th class="text-center">ห้องเรียน</th>
                                                <th class="text-center" >ตัวเลือก</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card --> 
                        </div>
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>

