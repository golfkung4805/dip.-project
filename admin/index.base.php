<?php
    if (!isset($_SESSION)) session_start();

    if(empty($_SESSION['Role'])){
        echo "<script>window.location=\"http://localhost/SR/?view=Error401\"</script>";
    }
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> <?php echo $Title; ?> </title>

    <!-- Favicon -->
    <link rel="icon" href="dist/img/AdminLTELogo.png" type="image/x-icon">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- https://kapeli.com/cheat_sheets/Font_Awesome.docset/Contents/Resources/Documents/index -->
    <!-- pace-progress -->
    <link rel="stylesheet" href="plugins/pace-progress/themes/black/pace-theme-flat-top.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,400i,700&display=swap" rel="stylesheet">
    <!-- SweetAlert2 -->
    <!-- <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- My Style CSS -->
    <link rel="stylesheet" href="dist/css/SR.CSS.css">
    <link rel="stylesheet" href="dist/css/ScrollBar.css">
    <?php include("dist/css/SR.CSS.php"); ?>

    <style>
        .swal2-popup.swal2-toast .swal2-title{
            font-size:1.5em
        }
    </style>
</head>
<!-- Modal -->
<div class="modal fade" id="modalChangePassword">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <form id="modalFormChangePassword">
                <div class="modal-header">
                    <h4 class="modal-title">เปลี่ยนรหัสผ่าน</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>รหัสผ่านเดิม</label>
                                <input type="password" class="form-control" id="old_password" name="old_password" placeholder="รหัสผ่านเดิม ...">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>รหัสผ่านใหม่</label>
                                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="รหัสผ่านใหม่ ...">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>ยืนยันรหัสผ่าน</label>
                                <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="ยืนยันรหัสผ่าน ...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <input type="hidden" name="id_teacher" id="id_teacher" value="<?php echo $_SESSION['teacher_id']; ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                    <button type="submit" class="btn btn-primary">เปลี่ยนรหัสผ่าน</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /. Modal -->
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed pace-primary" style="font-family: 'Kanit', sans-serif;">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" id="pushmenu" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Profile Dropdown Menu -->
                <li class="nav-item dropdown user-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <!-- <img src="dist/img/user2-160x160.jpg" class="user-image img-circle elevation-2" alt="User Image"> -->
                        <?php if($_SESSION['Role'] === "ADMIN"){ ?>
                        <div class="text-orange" style="display: inline">[ผู้ดูแลระบบ] </div>
                        <?php }else if($_SESSION['Role'] === "ChiefDEP"){ ?>
                        <div class="text-teal" style="display: inline">[หัวหน้าแผนก] </div>
                        <?php }else{ ?>
                        <div class="text-pink" style="display: inline">[อาจารย์] </div>
                        <?php } ?>
                        <div style="display: inline"><?php echo $_SESSION['gender'].$_SESSION['first_name'].$_SESSION['last_name']; ?></div>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <a href="#" class="btn btn-default btn-change-password btn-flat" data-id="1">เปลี่ยนรหัสผ่าน</a>
                            <a href="#" class="btn btn-default btn-logout btn-flat float-right">ออกจากระบบ<i class="fa fa-sign-out-alt text-right"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-blue elevation-4">
            <!-- Brand Logo -->
            <a href="./" class="brand-link">
                <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">MEC@SRMS</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar sidebar-scroll">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                        <li class="nav-header"> ตัวเลือก </li>
                        <li class="nav-item">
                            <a href="<?php echo $_SERVER_NAME; ?>" class="nav-link <?php echo ( $view == "Home" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-home"></i>
                                <p> หน้าหลัก </p>
                            </a>
                        </li>
                        <!-- <li class="nav-header"> ข้อมูลสถิติ </li>
                        <li class="nav-item">
                            <a href="?view=ChartStudent" class="nav-link <?php //echo ( $view == "ChartStudent" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-chart-line"></i>
                                <p> สถิตินักศึกษา </p>
                            </a>
                        </li> -->
                        <?php if($_SESSION['Role'] === "ADMIN" || $_SESSION['Role'] === "ChiefDEP"){ ?>
                        <li class="nav-header"> ข้อมูลทะเบียน </li>
                        <?php if($_SESSION['Role']==="ADMIN"){ ?>

                        <li class="nav-item">
                            <a href="?view=Department" class="nav-link <?php echo ( $view == "Department" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-sitemap"></i>
                                <p> ข้อมูลแผนก </p>
                            </a>
                        </li>
                        <?php } ?>

                        <li class="nav-item">
                            <a href="?view=Teacher" class="nav-link <?php echo ( $view == "Teacher" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-user-graduate"></i>
                                <p> ข้อมูลอาจารย์ </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=StudyGroup" class="nav-link <?php echo ( $view == "StudyGroup" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-book-reader"></i>
                                <p> ข้อมูลกลุ่มเรียน </p>
                            </a>
                        </li>
                        <?php } ?>

                        <li class="nav-header"> ข้อมูลระบบ </li>
                        <li class="nav-item">
                            <a href="?view=Subject" class="nav-link <?php echo ( $view == "Subject" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-book-open"></i>
                                <p> ข้อมูลวิชาเรียน </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=Room" class="nav-link <?php echo ( $view == "Room" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-building"></i>
                                <p> ข้อมูลห้องเรียน </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=EducationProgram" class="nav-link <?php echo ( $view == "EducationProgram" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-pencil-alt"></i>
                                <p> ข้อมูลแผนการเรียน </p>
                            </a>
                        </li>
                        <li class="nav-header"> จัดตารางเรียน </li>
                        <li class="nav-item">
                            <a href="?view=StudentSchedule" class="nav-link <?php echo ( $view == "StudentSchedule" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-calendar-alt"></i>
                                <p> จัดตารางเรียนนักศึกษา </p>
                            </a>
                        </li>
                        <?php if($_SESSION['Role']==="ADMIN"){ ?>
                            
                        <li class="nav-header"> ตั้งค่าระบบ </li>
                        <li class="nav-item">
                            <a href="?view=PositionSetting" class="nav-link <?php echo ( $view == "PositionSetting" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-sitemap"></i>
                                <p> ตั้งค่าตำแหน่งงาน </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=TimeSetting" class="nav-link <?php echo ( $view == "TimeSetting" ) ? "active" : "" ; ?>">
                                <i class="nav-icon fas fa-clock"></i>
                                <p> ตั้งค่าตารางเวลาเรียน </p>
                            </a>
                        </li>
                        <?php } ?>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <?php include( $MainContent ); ?>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                [MEC@SRMS] SIRI Management System | <!--<a href="https://github.com/ColorlibHQ/AdminLTE">Version 3.0.1</a>-->
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io/themes/dev/AdminLTE/">AdminLTE.io</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <script src="dist/js/js.cookie.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- pace-progress -->
    <script src="plugins/pace-progress/pace.min.js"></script>
    <!-- SweetAlert2 -->
    <script src="plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="plugins/toastr/toastr.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.js"></script>
    <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/js/select2.full.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- OPTIONAL SCRIPTS -->
    <!-- <script src="plugins/chart.js/Chart.min.js"></script> -->
    <!-- <script src="dist/js/SR.ChartStudent.js"></script> -->
    <!-- My Style JS -->
    <script src="dist/js/SR.JS.js"></script>
    <?php include("dist/js/SR.JS.php"); ?>
    
</body>
</html>