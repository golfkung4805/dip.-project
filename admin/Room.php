
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> ข้อมูลห้องเรียน </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?php echo $_SERVER_NAME; ?>">หน้าหลัก</a></li>
                                <li class="breadcrumb-item active"> ข้อมูลห้องเรียน </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Modal -->
            <div class="modal fade" id="modalRoom">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <form id="modalFormRoom">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modal-title">เพิ่มห้องเรียน</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>ชื่อย่อห้องเรียน</label>
                                            <input type="text" class="form-control" id="code_room" name="code_room" placeholder="กรอกชื่อย่อห้องเรียน ...">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>ชื่อห้องเรียน</label>
                                            <input type="text" class="form-control" id="name_room" name="name_room" placeholder="กรอกชื่อห้องเรียน ...">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <input type="hidden" id="id_room" name="id_room">
                                <input type="hidden" id="action" name="action" value="Insert">
                                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                                <button type="submit" class="btn btn-primary btn-submit">บันทึกข้อมูลห้องเรียน</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /. Modal -->
            
            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <table id="TableRoom" class="TableRoom table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ลำดับ</th>
                                                <th class="text-center">ชื่อย่อห้องเรียน</th>
                                                <th>ชื่อห้องเรียน</th>
                                                <th class="text-center">ตัวเลือก</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" height="70"></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="text-center">ลำดับ</th>
                                                <th class="text-center">ชื่อย่อห้องเรียน</th>
                                                <th>ชื่อห้องเรียน</th>
                                                <th class="text-center">ตัวเลือก</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col-md-12-->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
