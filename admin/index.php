<?php
    // TODO เช็คค่า View ว่ามีการส่งมาไหม
    session_start();
    $view = ( isset($_GET['view']) && $_GET['view'] != "" )? $_GET['view'] : "" ;
    $_SESSION['$view'] = $view;
    $view = $_SESSION['$view'];
    switch ($view) {
        // case 'ChartStudent':
        //     $Title = "สถิตินักศึกษา : MEC@SRMS";
        //     $MainContent = "ChartStudent.php";
        //     break;
        case 'Department':
            $Title = "ข้อมูลแผนก : MEC@SRMS";
            $MainContent = "Department.php";
            break;
        case 'Teacher':
            $Title = "ข้อมูลอาจารย์ : MEC@SRMS";
            $MainContent = "Teacher.php";
            break;
        case 'StudyGroup':
            $Title = "ข้อมูลกลุ่มเรียน : MEC@SRMS";
            $MainContent = "StudyGroup.php";
            break;
        case 'Subject':
            $Title = "ข้อมูลวิชาเรียน : MEC@SRMS";
            $MainContent = "Subject.php";
            break;
        case 'Room':
            $Title = "ข้อมูลห้องเรียน : MEC@SRMS";
            $MainContent = "Room.php";
            break;
        case 'EducationProgram':
            $Title = "ข้อมูลแผนการเรียน : MEC@SRMS";
            $MainContent = "EducationProgram.php";
            break;
        case 'StudentSchedule':
            $Title = "จัดตารางเรียนนักศึกษา : MEC@SRMS";
            $MainContent = "StudentSchedule.php";
            break;
        case 'PositionSetting':
            $Title = "ตั้งค่าตำแหน่งงาน : MEC@SRMS";
            $MainContent = "PositionSetting.php";
            break;
        case 'TimeSetting':
            $Title = "ตั้งค่าตารางเวลาเรียน : MEC@SRMS";
            $MainContent = "TimeSetting.php";
            break;
        default:
            $Title = "หน้าหลัก : MEC@SRMS";
            $MainContent = "Home.php";
            $view = "Home";
    }

    if($_SERVER['SERVER_NAME'] === "www2.mec.ac.th"){
        $_SERVER_NAME = "http://www2.mec.ac.th/it008/admin/";
    }else{
        $_SERVER_NAME = "http://localhost/SR/admin/";
    }

    include ('index.base.php');
?>