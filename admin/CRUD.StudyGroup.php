<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    function LoadTableStudyGroup($group){
        global $pdo;
        // $sql = "SELECT * FROM tb_study_group
        //         LEFT JOIN tb_teacher ON tb_study_group.teacher_code = tb_teacher.teacher_code 
        //         WHERE dept_code = (SELECT dept_code FROM tb_dept WHERE dept_name = :group OR dept_code = :code)";
        // $stmt = $pdo->prepare($sql);
        // $stmt->bindValue('group',$group,PDO::PARAM_STR);
        // $stmt->bindValue('code',$group,PDO::PARAM_STR);
        // $stmt->execute();
        // return $stmt;
        $request=$_POST;
        $col =array(
            0   =>  'group_id',
            1   =>  'group_code',
            2   =>  'group_subname',
            3   =>  'group_name',
            4   =>  'gender-first_name-last_name',
            5   =>  'group_id'
        );  //create column LIKE table in database

        //Search
        $sql = "SELECT * FROM tb_study_group
                LEFT JOIN tb_teacher ON tb_study_group.teacher_code = tb_teacher.teacher_code 
                WHERE 1=1 AND tb_study_group.dept_code = (SELECT dept_code FROM tb_dept WHERE dept_name = :group OR dept_code = :code)";
        if(!empty($request['search']['value'])){
            $sql.=" AND (group_id LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR group_code LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR group_subname LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR group_name LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR CONCAT(gender, ' ' ,first_name, ' ' ,last_name) LIKE '%".$request['search']['value']."%' )";
            
        }
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group',$group,PDO::PARAM_STR);
        $stmt->bindValue('code',$group,PDO::PARAM_STR);
        $stmt->execute();
        $totalData = $stmt->rowCount();
        $totalFilter=$totalData;
        $stmt = null;
        //Order
        $sql.=" ORDER BY ".$col[$request['order'][0]['column']]."   ".$request['order'][0]['dir']."  LIMIT ".
            $request['start']."  ,".$request['length']."  ";

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group',$group,PDO::PARAM_STR);
        $stmt->bindValue('code',$group,PDO::PARAM_STR);
        $stmt->execute();

        $data=array();
        $count = $request['start']+1;
        foreach($stmt as $row){ 
            $subdata=array();
            // $subdata[]=$row['id']; //id
            $subdata[]="<div class=\"text-center\">".$count."</div>";
            $subdata[]="<div class=\"text-center\">".$row['group_code']."</div>"; //dept_code
            $subdata[]="<div class=\"text-center\">".$row['group_subname']."</div>"; //dept_name
            $subdata[]="<div class=\"text-left\">".$row['group_name']."</div>"; //dept_name               $row[0] is id in table on database
            $subdata[]="<div class=\"text-left\">".$row['gender']." ".$row['first_name']." ".$row['last_name']."</div>"; //teacher_code
            $subdata[]="<div class=\"text-center\"><button class=\"btn btn-outline-warning btn-update mr-3\" data-id=\"$row[group_id]\">แก้ไข</button> <button class=\"btn btn-outline-danger btn-delete\" data-id=\"$row[group_id]\">ลบ</button></div>";
            $data[]=$subdata;
            $count++;
        }

        $json_data=array(
            "draw"              =>  intval($request['draw']),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalFilter),
            "data"              =>  $data
        );
        return $json_data;
    }

    function queryStudent($group){
        global $pdo;
        $sql = "SELECT * FROM tb_student
                WHERE GRO = '$group'";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group',$group,PDO::PARAM_STR);
        $stmt->execute();
        return $stmt;
    }

    function ChackGet($chack,$GetValue){
        global $pdo;
        if($chack==="dept"){
            $sql = "SELECT * FROM tb_dept 
                    WHERE dept_name = :chack";
        }else if($chack==="group"){
            $sql = "SELECT * FROM tb_study_group 
                    WHERE group_code = :chack";
        }
        
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('chack',$GetValue,PDO::PARAM_STR);
        $stmt->execute();
        if($stmt->rowCount()>=1){
            return TRUE;
        }else{
            echo "<script>alert('มีการแก้ไข URL');</script>";
            echo "<script>window.location.href='?view=StudyGroup';</script>";
            return FALSE;
        }
        $stmt = null;
        exit;
    }

    if($action==="LoadTableStudyGroup"){
        $dataTable = LoadTableStudyGroup($_POST['group']);
        echo json_encode($dataTable,JSON_UNESCAPED_UNICODE ); 
        exit;
    }

    if($action==="GetSelectDepartment"){
        $sql = "SELECT * FROM tb_dept 
                ORDER BY dept_name";
        $stmt = $pdo->query($sql);
        $GetDepartment[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name']); 
        }
        $stmt = null;
        
        echo json_encode($GetDepartment,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetDepartment"){
        $dept_code = "";
        if(isset($_POST['dept_code'])){
            $dept_code = $_POST['dept_code'];
        }
        $sql = "SELECT * FROM tb_dept 
                ORDER BY dept_name";
        $stmt = $pdo->query($sql);
        $GetDepartment[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['dept_code']===$dept_code){
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name'] , "selected"=>true); 
            }else{
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name']); 
            }
        }
        
        $stmt = null;
        
        echo json_encode($GetDepartment,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetTeacher"){
        $sql = "SELECT * FROM tb_teacher";
        $stmt = $pdo->query($sql);
        $GetTeacher[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['teacher_code']===$_POST['teacher_code']){
                $GetTeacher[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name'], "selected"=>true); 
            }else{
                $GetTeacher[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name']); 
            }
        }
        $stmt = null;
        
        echo json_encode($GetTeacher,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="getDetailStudyGroup"){
        $group_id = filter_input(INPUT_POST,"group_id");

        $sql = "SELECT * FROM tb_study_group
                wHERE group_id = :group_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group_id',$group_id,PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt = null;

        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="Insert"){
        $group_code = filter_input(INPUT_POST,"group_code");
        $group_subname = filter_input(INPUT_POST,"group_subname");
        $group_name = filter_input(INPUT_POST,"group_name");
        $dept_code = filter_input(INPUT_POST,"get_dept_code");
        $teacher_code = filter_input(INPUT_POST,"teacher_code");

        $sql = "SELECT group_code
                FROM tb_study_group 
                WHERE group_code = :group_code";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $alert = array("type"=>"warning", "title"=>"รหัสกลุ่ม $group_code มีในระบบแล้ว"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $sql = "INSERT INTO tb_study_group (group_code, group_subname, group_name, dept_code, teacher_code)
            VALUE(:group_code, :group_subname, :group_name, :dept_code, :teacher_code)";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
            $stmt->bindValue(':group_subname',$group_subname,PDO::PARAM_STR);
            $stmt->bindValue(':group_name',$group_name,PDO::PARAM_STR);
            $stmt->bindValue(':dept_code',$dept_code,PDO::PARAM_STR);
            $stmt->bindValue(':teacher_code',$teacher_code,PDO::PARAM_STR);
            $stmt->execute();
            $stmt = null;

            $alert = array("type"=>"success", "title"=>"เพิ่มกลุ่มสำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }

    if($action==="Update"){
        // $group_code = filter_input(INPUT_POST,"group_code");
        $group_subname = filter_input(INPUT_POST,"group_subname");
        $group_name = filter_input(INPUT_POST,"group_name");
        $teacher_code = filter_input(INPUT_POST,"teacher_code");
        $group_id = filter_input(INPUT_POST,"group_id");
        
        // $sql = "SELECT group_code,
        //         (SELECT count(group_code) FROM tb_study_group WHERE group_code = :group_code) as CheckCode
        //         FROM tb_study_group 
        //         WHERE group_id = :group_id";
        // $stmt = $pdo->prepare($sql);
        // $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
        // $stmt->bindValue(':group_id',$group_id,PDO::PARAM_INT);
        // $stmt->execute();

        // if($stmt->rowCount() > 0){
        //     $row = $stmt->fetch();
            // if($row['group_code'] === $group_code || $row['CheckCode'] <= 0){
                $sql = "UPDATE tb_study_group SET   group_subname = :group_subname, 
                                                    group_name = :group_name, 
                                                    teacher_code = :teacher_code 
                        WHERE group_id = :group_id";
                $stmt = $pdo->prepare($sql);
                // $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
                $stmt->bindValue(':group_subname',$group_subname,PDO::PARAM_STR);
                $stmt->bindValue(':group_name',$group_name,PDO::PARAM_STR);
                // $stmt->bindValue(':dept_code',$dept_code,PDO::PARAM_STR);
                $stmt->bindValue(':teacher_code',$teacher_code,PDO::PARAM_STR);
                $stmt->bindValue(':group_id',$group_id,PDO::PARAM_STR);
                $stmt->execute();
                $stmt = null;
    
                $alert = array("type"=>"success", "title"=>"แก้ไขกลุ่มสำเร็จ"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            // }else{
            //     $alert = array("type"=>"warning", "title"=>"รหัสกลุ่ม $group_code มีในระบบแล้ว"); 
            //     echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            //     exit;
            // } 
        // }else{
        //     $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
        //     echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
        //     exit;
        // }
    }

    if($action==="Delete"){
        $group_id = filter_input(INPUT_POST,"group_id");

        $sql = "SELECT * FROM tb_study_group 
                WHERE group_id = :group_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group_id',$group_id,PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount()===0){
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $sql = "DELETE FROM tb_study_group
                    WHERE group_id = :group_id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('group_id',$group_id,PDO::PARAM_INT);
            $stmt->execute();
            $stmt = null;

            $alert = array("type"=>"success", "title"=>"ลบกลุ่มสำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }


?>