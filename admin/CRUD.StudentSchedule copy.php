<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    function LoadTableEducationProgram($group_code,$term){
        global $pdo;
        // $sql = "SELECT * FROM tb_study_group
        //         LEFT JOIN tb_teacher ON tb_study_group.teacher_code = tb_teacher.teacher_code 
        //         WHERE dept_code = (SELECT dept_code FROM tb_dept WHERE dept_name = :group OR dept_code = :code)";
        // $stmt = $pdo->prepare($sql);
        // $stmt->bindValue('group',$group,PDO::PARAM_STR);
        // $stmt->bindValue('code',$group,PDO::PARAM_STR);
        // $stmt->execute();
        // return $stmt;
        $request=$_POST;
        $col =array(
            0   =>  'plan_id',
            1   =>  'tb_plan.subject_code',
            2   =>  'subject_name',
            3   =>  'period',
            4   =>  'theory-practice-credit',
            5   =>  'gender-first_name-last_name'
        );  //create column LIKE table in database

        //Search
        // $sql = "SELECT * FROM tb_study_group
        //         LEFT JOIN tb_teacher ON tb_study_group.teacher_code = tb_teacher.teacher_code 
        //         WHERE 1=1 AND dept_code = (SELECT dept_code FROM tb_dept WHERE dept_name = :group OR dept_code = :code)";
        $sql = "SELECT *,tb_subject.subject_code as subject_codee FROM tb_plan
                LEFT JOIN tb_subject ON tb_plan.subject_code = tb_subject.subject_code
                LEFT JOIN tb_teacher ON tb_plan.teacher_code = tb_teacher.teacher_code
                WHERE 1=1 AND group_code = :group_code AND term = :term";
        if(!empty($request['search']['value'])){
            $sql.=" AND (plan_id LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR tb_plan.subject_code LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR subject_name LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR period LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR CONCAT(theory, ' ' ,practice, ' ' ,credit) LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR CONCAT(gender, ' ' ,first_name, ' ' ,last_name) LIKE '%".$request['search']['value']."%' )";
            
        }
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue('term',$term,PDO::PARAM_STR);
        $stmt->execute();
        $totalData = $stmt->rowCount();
        $totalFilter=$totalData;
        $stmt = null;
        //Order
        $sql.=" ORDER BY ".$col[$request['order'][0]['column']]."   ".$request['order'][0]['dir']."  LIMIT ".
            $request['start']."  ,".$request['length']."  ";

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue('term',$term,PDO::PARAM_STR);
        $stmt->execute();

        $data=array();
        $count = $request['start']+1;
        foreach($stmt as $row){ 
            $subdata=array();
            // $subdata[]=$row['id']; //id
            $subdata[]="<div class=\"text-center\">".$count."</div>";
            $subdata[]="<div class=\"text-center\">".$row['subject_codee']."</div>"; //dept_code
            $subdata[]="<div class=\"text-left\">".$row['subject_name']."</div>"; //dept_name
            $subdata[]="<div class=\"text-center\">".$row['period']."/".($row['theory']+$row['practice'])."</div>"; //dept_name
            $subdata[]="<div class=\"text-center\">".$row['theory']."-".$row['practice']."-".$row['credit']."</div>"; //dept_name               $row[0] is id in table on database
            $subdata[]="<div class=\"text-left\">".$row['gender']." ".$row['first_name']." ".$row['last_name']."</div>"; //teacher_code
            $data[]=$subdata;
            $count++;
        }

        $json_data=array(
            "draw"              =>  intval($request['draw']),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalFilter),
            "data"              =>  $data
        );
        return $json_data;
    }

    if($action==="LoadTableEducationProgram"){
        $dept = $_POST['dept_code'];
        $group_code = $_POST['group_code'];
        $term = $_POST['select_year'];

        $dataTable = LoadTableEducationProgram($group_code,$term);
        echo json_encode($dataTable,JSON_UNESCAPED_UNICODE ); 
        exit;
    }
    
    function LoadTableSchedule($group_code,$term){
        global $pdo;
        // $sql = "SELECT * FROM tb_study_group
        //         LEFT JOIN tb_teacher ON tb_study_group.teacher_code = tb_teacher.teacher_code 
        //         WHERE dept_code = (SELECT dept_code FROM tb_dept WHERE dept_name = :group OR dept_code = :code)";
        // $stmt = $pdo->prepare($sql);
        // $stmt->bindValue('group',$group,PDO::PARAM_STR);
        // $stmt->bindValue('code',$group,PDO::PARAM_STR);
        // $stmt->execute();
        // return $stmt;
        $request=$_POST;
        $col =array(
            0   =>  'sch_date',
            1   =>  'sch_start_time',
            2   =>  'sch_end_time',
            3   =>  'gender-first_name-last_name',
            4   =>  'tb_schedule.subject_code',
            5   =>  'subject_name',
            6   =>  'room_code',
            7   =>  'sch_date'
        );  //create column LIKE table in database

        //Search
        // $sql = "SELECT * FROM tb_study_group
        //         LEFT JOIN tb_teacher ON tb_study_group.teacher_code = tb_teacher.teacher_code 
        //         WHERE 1=1 AND dept_code = (SELECT dept_code FROM tb_dept WHERE dept_name = :group OR dept_code = :code)";
        $sql = "SELECT *,tb_subject.subject_code as subject_codee FROM tb_schedule 
                LEFT JOIN tb_subject ON tb_schedule.subject_code = tb_subject.subject_code
                LEFT JOIN tb_teacher ON tb_schedule.teacher_code = tb_teacher.teacher_code
                WHERE 1=1 AND group_code = :group_code AND term = :term ";

        if(!empty($request['search']['value'])){
            $sql.=" AND (sch_date LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR sch_start_time LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR sch_end_time LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR CONCAT(gender, ' ' ,first_name, ' ' ,last_name) LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR tb_schedule.subject_code LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR subject_name LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR room_code LIKE '%".$request['search']['value']."%' )";
            
        }
        
        
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue('term',$term,PDO::PARAM_STR);
        $stmt->execute();
        $totalData = $stmt->rowCount();
        $totalFilter=$totalData;
        $stmt = null;
        //Order
        $sql.=" ORDER BY sch_date, sch_start_time, ".$col[$request['order'][0]['column']]."   ".$request['order'][0]['dir']."  LIMIT ".
            $request['start']."  ,".$request['length']."  ";

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue('term',$term,PDO::PARAM_STR);
        $stmt->execute();

        $data=array();
        $count = $request['start']+1;
        $thai_day_arr=array("จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์","อาทิตย์");
        foreach($stmt as $row){ 
            $subdata=array();
            // $subdata[]=$row['id']; //id
            $subdata[]="<div class=\"text-center\">".$thai_day_arr[$row['sch_date']-1]."</div>";
            $subdata[]="<div class=\"text-center\">".$row['sch_start_time']."</div>"; //sch_start_time
            $subdata[]="<div class=\"text-center\">".$row['sch_end_time']."</div>"; //sch_end_time
            $subdata[]="<div class=\"text-left\">".$row['gender']." ".$row['first_name']." ".$row['last_name']."</div>"; //teacher_code
            $subdata[]="<div class=\"text-center\">".$row['subject_codee']."</div>"; //subject_codee
            $subdata[]="<div class=\"text-left\">".$row['subject_name']."</div>"; //subject_name
            $subdata[]="<div class=\"text-center\">".$row['room_code']."</div>"; //room_code
            $subdata[]="<div class=\"text-center\"><button class=\"btn btn-outline-warning btn-update mr-3\" data-id=\"$row[sch_id]\">แก้ไข</button> <button class=\"btn btn-outline-danger btn-delete\" data-id=\"$row[sch_id]\">ลบ</button></div>";
            $data[]=$subdata;
            $count++;
        }

        $json_data=array(
            "draw"              =>  intval($request['draw']),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalFilter),
            "data"              =>  $data
        );
        return $json_data;
    }

    if($action==="LoadTableSchedule"){
        $dept = $_POST['dept_code'];
        $group_code = $_POST['group_code'];
        $term = $_POST['select_year'];

        $dataTable = LoadTableSchedule($group_code,$term);
        echo json_encode($dataTable,JSON_UNESCAPED_UNICODE ); 
        exit;
    }
    
    if($action==="GetDepartment"){
        $sql = "SELECT * FROM tb_dept 
                ORDER BY dept_name";
        $stmt = $pdo->query($sql);
        $GetDepartment[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['dept_code']===$_POST['dept_code']){
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name'] , "selected"=>true); 
            }else{
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name']); 
            }
            
        }
        $stmt = null;
        
        echo json_encode($GetDepartment,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetGroup"){
        $dept_code = $_POST['dept_code'];
        $sql = "SELECT * FROM tb_study_group 
                WHERE dept_code = :dept_code
                ORDER BY group_code";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('dept_code',$dept_code,PDO::PARAM_STR);
        $stmt->execute();

        $GetGroup[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['group_code']===$_POST['group_code']){
                $GetGroup[] = array("id"=>$row['group_code'], "text"=>$row['group_code']." | ".$row['group_subname'] , "selected"=>true); 
            }else{
                $GetGroup[] = array("id"=>$row['group_code'], "text"=>$row['group_code']." | ".$row['group_subname']); 
            }
            
        }
        
        $stmt = null;
        
        echo json_encode($GetGroup,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetYear"){
        $Year = date("Y")+543;

        $NowYear = substr($Year,0,2);

        $group_code = $_POST['group_code'];
        $select_year = $_POST['select_year'];
        $subYear = intval(substr($group_code,0,2));

        $subClass = substr($group_code,2,1);

        if($subClass==="2"){
            $loop = 3; // ปวช เรียน 3 ปี
        }else if($subClass==="3"){
            $loop = 2; // ปวช เรียน 2 ปี
        }
        $Output[] = array("id"=>"", "text"=>""); 
        for($i=1;$i<=$loop;$i++){
            if($subYear<10){
                $returns = "1/".$NowYear."0".$subYear;
                if($select_year===$returns){
                    $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                }else{
                    $Output[] = array("id"=>$returns, "text"=>$returns); 
                }
                $returns = "2/".$NowYear."0".$subYear;
                if($select_year===$returns){
                    $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                }else{
                    $Output[] = array("id"=>$returns, "text"=>$returns); 
                }

                if($i === 1 && $loop === 2){
                    $returns = "S/".$NowYear."0".$subYear;
                    if($select_year===$returns){
                        $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                    }else{
                        $Output[] = array("id"=>$returns, "text"=>$returns); 
                    } 
                }
            }else{
                $returns = "1/".$NowYear.$subYear;
                if($select_year===$returns){
                    $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                }else{
                    $Output[] = array("id"=>$returns, "text"=>$returns); 
                } 

                $returns = "2/".$NowYear.$subYear;
                if($select_year===$returns){
                    $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                }else{
                    $Output[] = array("id"=>$returns, "text"=>$returns); 
                } 

                if($i === 1 && $loop === 2){
                    $returns = "S/".$NowYear.$subYear;
                    if($select_year===$returns){
                        $Output[] = array("id"=>$returns, "text"=>$returns, "selected"=>true); 
                    }else{
                        $Output[] = array("id"=>$returns, "text"=>$returns); 
                    } 
                }
                
            }

            $subYear++;
            if($subYear>99){
                $subYear = intval(substr($subYear,-2));
                $NowYear++;
            }
        }

        echo json_encode($Output,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetPeriod"){
        $group_code = $_POST['group_code'];
        $term = $_POST['term'];
        $sql = "SELECT subject_code as subject_codee, period ,(SELECT theory FROM tb_subject WHERE subject_code = subject_codee)+(SELECT practice  FROM tb_subject WHERE subject_code = subject_codee)as sumPeriod 
                FROM tb_plan WHERE group_code = :group_code AND term = :term";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue('term',$term,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt = null;
        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        
        exit;
    }

    if($action==="GetStartTime"){
        $sql = "SELECT * FROM tb_time_setting";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $dataTime = $stmt->fetch();

        $sc_startTime=date("Y-m-d $dataTime[time_start]");  // กำหนดเวลาเริ่มต้ม เปลี่ยนเฉพาะเลขเวลา
        $sc_endtTime=date("Y-m-d $dataTime[time_end]");  // กำหนดเวลาสื้นสุด เปลี่ยนเฉพาะเลขเวลา
        $sc_t_startTime=strtotime($sc_startTime);
        $sc_t_endTime=strtotime($sc_endtTime);
        $sc_numStep = $dataTime['time_class']; // ช่วงช่องว่างเวลา หน่ายนาที 60 นาที = 1 ชั่วโมง
        $sc_timeStep=array();
        $sc_numCol=0;
        ////////////////////// ส่วนของการจัดการตารางเวลา /////////////////////
        while($sc_t_startTime<=$sc_t_endTime){
            $sc_timeStep[$sc_numCol]=date("H:i",$sc_t_startTime);    
            $sc_t_startTime=$sc_t_startTime+($sc_numStep*60); 
            $sc_numCol++;    // ได้จำนวนคอลัมน์ที่จะแสดง
        }


        $GetStartTime[] = array("id"=>"", "text"=>""); 

        for($i = 1;$i<$sc_numCol;$i++){
            if($i==$_POST['sch_start_time']){
                $GetStartTime[] = array("id"=>$i, "text"=>"คาบที่ ".$i." เวลา ".$sc_timeStep[$i-1], "selected"=>true); 
            }else{
                $GetStartTime[] = array("id"=>$i, "text"=>"คาบที่ ".$i." เวลา ".$sc_timeStep[$i-1]); 
            }
        }

        $stmt = null;
        
        echo json_encode($GetStartTime,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetEndTime"){
        $sql = "SELECT * FROM tb_time_setting";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $dataTime = $stmt->fetch();

        $sc_startTime=date("Y-m-d $dataTime[time_start]");  // กำหนดเวลาเริ่มต้ม เปลี่ยนเฉพาะเลขเวลา
        $sc_endtTime=date("Y-m-d $dataTime[time_end]");  // กำหนดเวลาสื้นสุด เปลี่ยนเฉพาะเลขเวลา
        $sc_t_startTime=strtotime($sc_startTime);
        $sc_t_endTime=strtotime($sc_endtTime);
        $sc_numStep = $dataTime['time_class']; // ช่วงช่องว่างเวลา หน่ายนาที 60 นาที = 1 ชั่วโมง
        $sc_timeStep=array();
        $sc_numCol=0;
        ////////////////////// ส่วนของการจัดการตารางเวลา /////////////////////
        while($sc_t_startTime<=$sc_t_endTime){
            $sc_timeStep[$sc_numCol]=date("H:i",$sc_t_startTime);    
            $sc_t_startTime=$sc_t_startTime+($sc_numStep*60); 
            $sc_numCol++;    // ได้จำนวนคอลัมน์ที่จะแสดง
        }

        $GetEndTime[] = array("id"=>"", "text"=>""); 

        for($i = $_POST['startTime'];$i<$sc_numCol;$i++){
            if($i==$_POST['sch_end_time']){
                $GetEndTime[] = array("id"=>$i, "text"=>" คาบที่ ".$i." เวลา ".$sc_timeStep[$i], "selected"=>true); 
            }else{
                $GetEndTime[] = array("id"=>$i, "text"=>" คาบที่ ".$i." เวลา ".$sc_timeStep[$i]); 
            }
        }

        $stmt = null;
        
        echo json_encode($GetEndTime,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetSubjectInPlan"){
        $group_code = $_POST['group_code'];
        $term = $_POST['term'];

        $sql = "SELECT *,tb_subject.subject_code as subject_codee FROM tb_plan
                LEFT JOIN tb_subject ON tb_plan.subject_code = tb_subject.subject_code
                WHERE group_code = :group_code AND term = :term";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue('term',$term,PDO::PARAM_STR);
        $stmt->execute();

        $GetSupjectInPlan[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['subject_code']===$_POST['subject_code']){
                $GetSupjectInPlan[] = array("id"=>$row['subject_code'], "text"=>$row['subject_code']." | ".$row['subject_name'], "selected"=>true); 
            }
            if(intval($row['period'])!==(intval($row['theory'])+intval($row['practice']))){
                // if($row['subject_code']===$_POST['subject_code']){
                //     $GetSupjectInPlan[] = array("id"=>$row['subject_code'], "text"=>$row['subject_code']." | ".$row['subject_name'], "selected"=>true); 
                // }else{
                    $GetSupjectInPlan[] = array("id"=>$row['subject_code'], "text"=>$row['subject_code']." | ".$row['subject_name']); 
                // }
            }
        }
        
        $stmt = null;
        
        echo json_encode($GetSupjectInPlan,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action === "GetCodeTeacher"){
        $subject_code = $_POST['subject_code'];
        $group_code = $_POST['group_code'];
        $term = $_POST['term'];

        $sql = "SELECT teacher_code FROM tb_schedule WHERE subject_code = :subject_code AND group_code = :group_code AND term = :term";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('subject_code',$subject_code,PDO::PARAM_STR);
        $stmt->bindValue('group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue('term',$term,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $stmt = null;
        
        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetTeacher"){
        $sql = "SELECT * FROM tb_teacher";
        $stmt = $pdo->query($sql);
        $GetTeacher[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['teacher_code']===$_POST['teacher_code']){
                $GetTeacher[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name'], "selected"=>true); 
            }else{
                $GetTeacher[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name']); 
            }
        }
        $stmt = null;
        
        echo json_encode($GetTeacher,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetRoom"){
        $sql = "SELECT * FROM tb_room";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        
        $GetRoom[] = array("id"=>"", "text"=>""); 

        foreach($stmt as $row){
            if($row['room_code']===$_POST['room_code']){
                $GetRoom[] = array("id"=>$row['room_code'], "text"=>$row['room_code']." | ".$row['room_name'], "selected"=>true); 
            }else{
                $GetRoom[] = array("id"=>$row['room_code'], "text"=>$row['room_code']." | ".$row['room_name']); 
            }
        }

        $stmt = null;
        
        echo json_encode($GetRoom,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action === "GetDetailSchedule"){
        $sch_id = $_POST['sch_id'];

        $sql = "SELECT * FROM tb_schedule WHERE sch_id = :sch_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('sch_id',$sch_id,PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $stmt = null;
        
        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="Insert"){
        $sch_date = filter_input(INPUT_POST,"sch_date");
        $sch_start_time = filter_input(INPUT_POST,"sch_start_time");
        $sch_end_time = filter_input(INPUT_POST,"sch_end_time");
        $subject_code = filter_input(INPUT_POST,"subject_code");
        $teacher_code = filter_input(INPUT_POST,"teacher_code");
        $room_code = filter_input(INPUT_POST,"room_code");
        $group_code = filter_input(INPUT_POST,"group_code");
        $term = filter_input(INPUT_POST,"term");

        $sql1 = "SELECT * FROM tb_schedule 
                WHERE sch_date = '$sch_date' AND group_code = '$group_code' AND term = '$term' HAVING sch_start_time BETWEEN '$sch_start_time' AND '$sch_end_time' OR sch_end_time BETWEEN '$sch_start_time' AND '$sch_end_time'";
        $stmt1 = $pdo->prepare($sql1);
        $stmt1->execute();
        if($stmt1->rowCount()>0){
            $alert = array("type"=>"info", "title"=>"กรุณาเลือกคาบเรียนเรียนใหม่", "text"=>"อาจารย์สอนระหว่าง $sch_start_time - $sch_end_time แล้ว"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }

        $sql1 = "SELECT * FROM tb_schedule 
                WHERE teacher_code = '$teacher_code' AND sch_date = '$sch_date' AND term = '$term' HAVING sch_start_time BETWEEN '$sch_start_time' AND '$sch_end_time' OR sch_end_time BETWEEN '$sch_start_time' AND '$sch_end_time'";
        $stmt1 = $pdo->prepare($sql1);
        $stmt1->execute();
        $row = $stmt1->fetch();

        
        if($stmt1->rowCount()>0){
            if($row['subject_code'] == $subject_code){
                if($row['sch_start_time'] != $sch_start_time || $row['sch_end_time'] != $sch_end_time){
                    $alert = array("type"=>"info", "title"=>"กรุณาเลือกเวลาใหม่", "text"=>"กรุณาเลือกเวลาเป็น $row[sch_start_time] - $row[sch_end_time]"); 
                    echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                    exit;
                }
            }else{
                $alert = array("type"=>"info", "title"=>"กรุณาเลือกอาจารย์ใหม่", "text"=>"เนื่องจากอาจารย์ท่านนี้มีสอนวิชาอื่นในคาบนี้แล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }
        
        $sql2 = "SELECT * FROM tb_schedule WHERE room_code = '$room_code' AND sch_date = '$sch_date' AND term = '$term' HAVING sch_start_time BETWEEN '$sch_start_time' AND '$sch_end_time' OR sch_end_time BETWEEN '$sch_start_time' AND '$sch_end_time'";
        $stmt2 = $pdo->prepare($sql2);
        $stmt2->execute();
        $row2 = $stmt2->fetch();
        if($stmt2->rowCount()>0){
            if($row2['room_code'] === $room_code){ 
                if($row2['teacher_code'] !== $teacher_code ){
                    $alert = array("type"=>"info", "title"=>"กรุณาเลือกห้องเรียนใหม่", "text"=>"เนื่องจากคาบนี้มีอาจารย์ใช้ห้องแล้ว"); 
                    echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                    exit;
                }
            }else{
                $alert = array("type"=>"info", "title"=>"กรุณาเลือกห้องเรียนใหม่", "text"=>"เนื่องจากคาบนี้มีอาจารย์ใช้ห้องแล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }else{
            if($stmt1->rowCount()>0){
                $alert = array("type"=>"info", "title"=>"กรุณาเลือกอาจารย์ใหม่", "text"=>"กรุณาเลือกห้องเป็น $row[room_code]"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE );
                exit;
            }
        }
        
        $period = ($sch_end_time-$sch_start_time)+1;

        $sql = "SELECT * FROM tb_plan 
        LEFT JOIN tb_subject ON tb_plan.subject_code = tb_subject.subject_code
        LEFT JOIN tb_teacher ON tb_plan.teacher_code = tb_teacher.teacher_code
        WHERE tb_plan.subject_code = :subject_code AND group_code = :group_code AND term = :term";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':subject_code',$subject_code,PDO::PARAM_STR);
        $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue(':term',$term,PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch();
        $stmt = null;

        if($row['teacher_code']!==$teacher_code AND $row['teacher_code'] !== NULL){
            $alert = array("type"=>"error", "title"=>"กรุณาแก้ไขรหัสอาจารย์ให้ตรงกัน", "text"=>"วิชา $row[subject_name] อาจารย์ $row[first_name] $row[last_name] เป็นคนสอน"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }

        $SumPeriod = intval($row['theory'])+intval($row['practice']);
        if($period>$SumPeriod){
            $alert = array("type"=>"error", "title"=>"คุณเลือกคาบเรียนเกินกำหนด", "text"=>"วิชา $row[subject_name] เลือกได้สูงสุด $SumPeriod คาบ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            if($period + $row['period'] > $SumPeriod){
                $alert = array("type"=>"error", "title"=>"คุณได้เลือกคาบเรียนเกินกำหนด", "text"=>"วิชา $row[subject_name] เลือกได้เพียง ".($SumPeriod-$row['period'])." คาบ"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }
        
        if($row['teacher_code']===NULL){
            $sql = "UPDATE tb_plan SET period = period+:period, teacher_code = :teacher_code WHERE subject_code = :subject_code AND group_code = :group_code AND term = :term";
        }else{
            $sql = "UPDATE tb_plan SET period = period+:period WHERE subject_code = :subject_code AND group_code = :group_code AND term = :term";
        }

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':period',$period,PDO::PARAM_INT);
        if($row['teacher_code']===NULL){
            $stmt->bindValue(':teacher_code',$teacher_code,PDO::PARAM_STR);
        }
        $stmt->bindValue(':subject_code',$subject_code,PDO::PARAM_STR);
        $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue(':term',$term,PDO::PARAM_STR);
        $stmt->execute();
        $stmt = null;

        $sql = "INSERT INTO tb_schedule (subject_code, room_code, teacher_code, sch_date, sch_start_time, sch_end_time, group_code, term)
                VALUE(:subject_code, :room_code, :teacher_code, :sch_date, :sch_start_time, :sch_end_time, :group_code, :term)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':subject_code',$subject_code,PDO::PARAM_STR);
        $stmt->bindValue(':room_code',$room_code,PDO::PARAM_STR);
        $stmt->bindValue(':teacher_code',$teacher_code,PDO::PARAM_STR);
        $stmt->bindValue(':sch_date',$sch_date,PDO::PARAM_INT);
        $stmt->bindValue(':sch_start_time',$sch_start_time,PDO::PARAM_INT);
        $stmt->bindValue(':sch_end_time',$sch_end_time,PDO::PARAM_INT);
        $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue(':term',$term,PDO::PARAM_STR);
        $stmt->execute();
        $stmt = null;

        $alert = array("type"=>"success", "title"=>"เพิ่มวิชาเรียนสำเร็จ"); 
        echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
        exit;
    }

    if($action==="Update"){
        $sch_date = filter_input(INPUT_POST,"sch_date");
        $sch_start_time = filter_input(INPUT_POST,"sch_start_time");
        $sch_end_time = filter_input(INPUT_POST,"sch_end_time");
        $subject_code = filter_input(INPUT_POST,"subject_code");
        $teacher_code = filter_input(INPUT_POST,"teacher_code");
        $room_code = filter_input(INPUT_POST,"room_code");
        $group_code = filter_input(INPUT_POST,"group_code");
        $term = filter_input(INPUT_POST,"term");
        $sch_id = filter_input(INPUT_POST,"sch_id");

        $sql1 = "SELECT * FROM tb_schedule WHERE sch_id NOT IN ('$sch_id') AND sch_date = '$sch_date' AND group_code = '$group_code' AND term = '$term' HAVING sch_start_time BETWEEN '$sch_start_time' AND '$sch_end_time' OR sch_end_time BETWEEN '$sch_start_time' AND '$sch_end_time'";
        $stmt1 = $pdo->prepare($sql1);
        $stmt1->execute();
        $row1 = $stmt1->fetch();
        if($stmt1->rowCount()>0){
            $alert = array("type"=>"info", "title"=>"กรุณาเลือกคาบเรียนเรียนใหม่", "text"=>"คาบ $row1[sch_start_time] - $row1[sch_end_time] มีอาจารย์สอนแล้ว"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
        
        $sql1 = "SELECT * FROM tb_schedule WHERE sch_id NOT IN ('$sch_id') AND  teacher_code = '$teacher_code' AND sch_date = '$sch_date' AND term = '$term' HAVING sch_start_time BETWEEN '$sch_start_time' AND '$sch_end_time' OR sch_end_time BETWEEN '$sch_start_time' AND '$sch_end_time'";
        $stmt1 = $pdo->prepare($sql1);
        $stmt1->execute();
        $row = $stmt1->fetch();
        if($stmt1->rowCount()>0){
            if($row['subject_code'] == $subject_code){
                if($row['sch_start_time'] != $sch_start_time && $row['sch_end_time'] != $sch_end_time){
                    $alert = array("type"=>"info", "title"=>"กรุณาเลือกเวลาใหม่", "text"=>"เป็น $row[sch_start_time] - $row[sch_end_time] ."); 
                    echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                    exit;
                }
            }else{
                $alert = array("type"=>"info", "title"=>"กรุณาเลือกอาจารย์ใหม่", "text"=>"เนื่องจากอาจารย์ท่านนี้มีสอนวิชาอื่นในคาบนี้แล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }
        
        $sql2 = "SELECT * FROM tb_schedule WHERE sch_id NOT IN ('$sch_id') AND room_code = '$room_code' AND sch_date = '$sch_date' AND term = '$term' HAVING sch_start_time BETWEEN '$sch_start_time' AND '$sch_end_time' OR sch_end_time BETWEEN '$sch_start_time' AND '$sch_end_time'";
        // $sql2 = "SELECT * FROM tb_schedule WHERE room_code = '$room_code' AND sch_date = '$sch_date' AND term = '$term' HAVING sch_start_time BETWEEN '$sch_start_time' AND '$sch_end_time' OR sch_end_time BETWEEN '$sch_start_time' AND '$sch_end_time'";
        $stmt2 = $pdo->prepare($sql2);
        $stmt2->execute();
        $row2 = $stmt2->fetch();
        if($stmt2->rowCount()>0){
            if($row2['room_code'] === $room_code){ 
                if($row2['teacher_code'] !== $teacher_code ){
                    $alert = array("type"=>"info", "title"=>"กรุณาเลือกห้องเรียนใหม่", "text"=>"เนื่องจากคาบนี้มีอาจารย์ใช้ห้องแล้ว"); 
                    echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                    exit;
                }
            }else{
                $alert = array("type"=>"info", "title"=>"กรุณาเลือกห้องเรียนใหม่", "text"=>"เนื่องจากคาบนี้มีอาจารย์ใช้ห้องแล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }else{
            if($stmt1->rowCount()>0){
                $alert = array("type"=>"info", "title"=>"กรุณาเลือกอาจารย์ใหม่", "text"=>"กรุณาเลือกห้องเป็น $row[room_code]"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE );
                exit;
            }
        }
        
        $period = ($sch_end_time-$sch_start_time)+1;

        $sql = "SELECT *, (SELECT sch_start_time FROM tb_schedule WHERE sch_id = :id_start)as start,
                            (SELECT sch_end_time FROM tb_schedule WHERE sch_id = :id_end)as end FROM tb_plan 
                            LEFT JOIN tb_subject ON tb_plan.subject_code = tb_subject.subject_code
                            LEFT JOIN tb_teacher ON tb_plan.teacher_code = tb_teacher.teacher_code
                            WHERE tb_plan.subject_code = :subject_code AND group_code = :group_code AND term = :term";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':id_start',$sch_id,PDO::PARAM_INT);
        $stmt->bindValue(':id_end',$sch_id,PDO::PARAM_INT);
        $stmt->bindValue(':subject_code',$subject_code,PDO::PARAM_STR);
        $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue(':term',$term,PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch();
        $stmt = null;

        if($row['teacher_code']!==$teacher_code){
            $alert = array("type"=>"error", "title"=>"กรุณาแก้ไขรหัสอาจารย์ให้ตรงกัน", "text"=>"วิชา $row[subject_name] อาจารย์ $row[first_name] $row[last_name] เป็นคนสอน"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }

        $SumPeriod = intval($row['theory'])+intval($row['practice']);
        $totalPeriod = (intval($row['end']) - intval($row['start'])) + 1 ; 

        if($period + ($row['period'] - $totalPeriod) > $SumPeriod){ 
            $alert = array("type"=>"error", "title"=>"คุณเลือกคาบเรียนเกินกำหนด", "text"=>"วิชา $row[subject_name] เลือกได้สูงสุด $SumPeriod คาบ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
        

        $period  = $period - intval($row['period']);

        if($row['teacher_code']===NULL){
            $sql = "UPDATE tb_plan SET period = period+:period, teacher_code = :teacher_code WHERE subject_code = :subject_code AND group_code = :group_code AND term = :term";
        }else{
            $sql = "UPDATE tb_plan SET period = period+:period WHERE subject_code = :subject_code AND group_code = :group_code AND term = :term";
        }

        
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':period',$period,PDO::PARAM_INT);
        if($row['teacher_code']===NULL){
            $stmt->bindValue(':teacher_code',$teacher_code,PDO::PARAM_STR);
        }
        $stmt->bindValue(':subject_code',$subject_code,PDO::PARAM_STR);
        $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue(':term',$term,PDO::PARAM_STR);
        $stmt->execute();
        $stmt = null;

        $sql = "UPDATE tb_schedule SET subject_code = :subject_code, 
                                        room_code = :room_code, 
                                        teacher_code = :teacher_code, 
                                        sch_date = :sch_date, 
                                        sch_start_time = :sch_start_time, 
                                        sch_end_time = :sch_end_time
                WHERE   sch_id = :sch_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':subject_code',$subject_code,PDO::PARAM_STR);
        $stmt->bindValue(':room_code',$room_code,PDO::PARAM_STR);
        $stmt->bindValue(':teacher_code',$teacher_code,PDO::PARAM_STR);
        $stmt->bindValue(':sch_date',$sch_date,PDO::PARAM_INT);
        $stmt->bindValue(':sch_start_time',$sch_start_time,PDO::PARAM_INT);
        $stmt->bindValue(':sch_end_time',$sch_end_time,PDO::PARAM_INT);
        $stmt->bindValue(':sch_id',$sch_id,PDO::PARAM_INT);
        $stmt->execute();
        $stmt = null;

        $alert = array("type"=>"success", "title"=>"แก้ไขวิชาเรียนสำเร็จ"); 
        echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
        exit;
    }

    if($action==="Delete"){
        $sch_id = filter_input(INPUT_POST,"sch_id");


        $sql ="SELECT * FROM tb_schedule WHERE sch_id = :sch_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':sch_id',$sch_id,PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount() === 0){
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $data = $stmt->fetch();

            $period = ($data['sch_end_time']-$data['sch_start_time'])+1;
            $sql = "SELECT * FROM tb_plan WHERE subject_code = :subject_code AND group_code = :group_code AND term = :term";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':subject_code',$data['subject_code'],PDO::PARAM_STR);
            $stmt->bindValue(':group_code',$data['group_code'],PDO::PARAM_STR);
            $stmt->bindValue(':term',$data['term'],PDO::PARAM_STR);
            $stmt->execute();
            $data2 = $stmt->fetch();
            $stmt = null;

            if($period - $data2['period'] === 0){
                $sql = "UPDATE tb_plan SET period = period-:period, teacher_code = NULL WHERE subject_code = :subject_code AND group_code = :group_code AND term = :term";
            }else{
                $sql = "UPDATE tb_plan SET period = period-:period WHERE subject_code = :subject_code AND group_code = :group_code AND term = :term";
            }

            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':period',$period,PDO::PARAM_INT);
            $stmt->bindValue(':subject_code',$data['subject_code'],PDO::PARAM_STR);
            $stmt->bindValue(':group_code',$data['group_code'],PDO::PARAM_STR);
            $stmt->bindValue(':term',$data['term'],PDO::PARAM_STR);
            $stmt->execute();
            $stmt = null;
    
            $sql = "DELETE FROM tb_schedule
                    WHERE sch_id = :sch_id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('sch_id',$sch_id,PDO::PARAM_INT);
            $stmt->execute();
            $stmt = null;
    
            $alert = array("type"=>"success", "title"=>"ลบวิชาในตารางเรียนสำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
        
    }


?>