<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    // $code_teach = "a1a";
    // $prefix_teach = "a1";
    // $first_name_teach = "a1";
    // $last_name_teach = "a1";
    // $Room_teach = "a1";

    // $sql = "INSERT INTO tb_teacher (teacher_code, gender, first_name, last_name, Room) VALUE(:code_teach, :prefix_teach, :first_name_teach, :last_name_teach, :Room_teach)";
    // $stmt = $pdo->prepare($sql);
    // $stmt->bindValue('code_teach',$code_teach,PDO::PARAM_STR);
    // $stmt->bindValue('prefix_teach',$prefix_teach,PDO::PARAM_STR);
    // $stmt->bindValue('first_name_teach',$first_name_teach,PDO::PARAM_STR);
    // $stmt->bindValue('last_name_teach',$last_name_teach,PDO::PARAM_STR);
    // $stmt->bindValue('Room_teach',$Room_teach,PDO::PARAM_STR);
    // $stmt->execute();
    

    function LoadTableRoom(){
        global $pdo;

        $request=$_POST;
        $col =array(
            0   =>  'room_id',
            1   =>  'room_code',
            2   =>  'room_name',
            3   =>  'room_id'
        );  //create column LIKE table in database

        //Search
        $sql = "SELECT * FROM `tb_room` 
                WHERE 1=1";
        if(!empty($request['search']['value'])){
            $sql.=" AND (room_id LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR room_code LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR room_name LIKE '%".$request['search']['value']."%' )";
        }
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $totalData = $stmt->rowCount();
        $totalFilter=$totalData;
        $stmt = null;
        //Order
        $sql.=" ORDER BY ".$col[$request['order'][0]['column']]."   ".$request['order'][0]['dir']."  LIMIT ".
            $request['start']."  ,".$request['length']."  ";

        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $data=array();
        $count = $request['start']+1;
        foreach($stmt as $row){ 
            $subdata=array();
            // $subdata[]=$row['id']; //id
            $subdata[]="<div class=\"text-center\">".$count."</div>";
            $subdata[]="<div class=\"text-center\">".$row['room_code']."</div>"; //room_code
            $subdata[]="<div class=\"text-left\">".$row['room_name']."</div>"; //room_name               $row[0] is id in table on database
            $subdata[]="<div class=\"text-center\"><button class=\"btn btn-outline-warning btn-update mr-3\" data-id=\"$row[room_id]\">แก้ไข</button> <button class=\"btn btn-outline-danger btn-delete\" data-id=\"$row[room_id]\">ลบ</button></div>";
            $data[]=$subdata;
            $count++;
        }

        $json_data=array(
            "draw"              =>  intval($request['draw']),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalFilter),
            "data"              =>  $data
        );
        return $json_data;
    }

    if($action==="LoadTableRoom"){
        $dataTable = LoadTableRoom();
        echo json_encode($dataTable,JSON_UNESCAPED_UNICODE ); 
        exit;
    }

    if($action==="getDetailRoom"){
        $room_id = filter_input(INPUT_POST,'room_id');

        $sql = "SELECT * FROM tb_room 
                WHERE room_id = :room_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('room_id',$room_id,PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt = null;

        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="Insert"){
        $code_room = filter_input(INPUT_POST,'code_room');
        $name_room = filter_input(INPUT_POST,'name_room');

        $sql = "SELECT (SELECT room_code FROM tb_room WHERE room_code = :room_code)as roomCode,
                        (SELECT room_code FROM tb_room WHERE room_name = :name_room)as roomName";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':room_code',$code_room,PDO::PARAM_STR);
        $stmt->bindValue(':name_room',$name_room,PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $row = $stmt->fetch();
            if($row['roomCode']!=""){
                $alert = array("type"=>"warning", "title"=>"ชื่อย่อห้องเรียน $code_room มีในระบบแล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else if($row['roomName']!=""){
                $alert = array("type"=>"warning", "title"=>"ชื่อห้องเรียน $name_room มีในระบบแล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else{
                $sql = "INSERT INTO tb_room (room_code, room_name) 
                        VALUE(:code_room, :name_room)";
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue('code_room',$code_room,PDO::PARAM_STR);
                $stmt->bindValue('name_room',$name_room,PDO::PARAM_STR);
                $stmt->execute();
                $stmt = null;

                $alert = array("type"=>"success", "title"=>"เพิ่มห้องเรียนสำเร็จ"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }else{
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }

    if($action==="Update"){
        $code_room = filter_input(INPUT_POST,'code_room');
        $name_room = filter_input(INPUT_POST,'name_room');
        $id_room = filter_input(INPUT_POST,'id_room');

        
        $sql = "SELECT room_code,
                    (SELECT count(room_code) FROM tb_room WHERE room_code = :room_code) as CheckCode
                    FROM tb_room 
                    WHERE room_id = :room_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':room_code',$code_room,PDO::PARAM_STR);
        $stmt->bindValue(':room_id',$id_room,PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $row = $stmt->fetch();
            if($row['room_code'] === $code_room || $row['CheckCode'] <= 0){
                $sql = "UPDATE tb_room SET  room_code = :code_room, 
                                            room_name = :name_room
                        WHERE room_id = :id_room";
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue('code_room',$code_room,PDO::PARAM_STR);
                $stmt->bindValue('name_room',$name_room,PDO::PARAM_STR);
                $stmt->bindValue('id_room',$id_room,PDO::PARAM_INT);
                $stmt->execute();
                $stmt = null;

                $alert = array("type"=>"success", "title"=>"แก้ไขห้องเรียนสำเร็จ"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else{
                $alert = array("type"=>"warning", "title"=>"ชื่อย่อห้องเรียน $code_room มีในระบบแล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }else{
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }

    if($action==="Delete"){
        $room_id = filter_input(INPUT_POST,'room_id');

        $sql = "SELECT * FROM tb_room 
                WHERE room_id = :room_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('room_id',$room_id,PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount()===0){
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $sql = "DELETE FROM tb_room 
                    WHERE room_id = :room_id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('room_id',$room_id,PDO::PARAM_INT);
            $stmt->execute();
            $stmt = null;

            $alert = array("type"=>"success", "title"=>"ลบห้องเรียนสำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }
?>