                                <?php
                                    include('./include/DBConfig.SR.php');
                                    $sql = "SELECT * FROM tb_time_setting";
                                    $stmt = $pdo->prepare($sql);
                                    $stmt->execute();
                                    $numRow = $stmt->rowCount();
                                    if($numRow>1){
                                        $sql2 = "";
                                        $stmt2 = $pdo->prepare($sql2);
                                        $stmt2->execute();
                                    }
                                    if($numRow<1 || $numRow>1){
                                        $sql2 = "TRUNCATE sr_system.tb_time_setting";
                                        $stmt2 = $pdo->prepare($sql2);
                                        $stmt2->execute();
                                        $sql2 = "INSERT INTO tb_time_setting (time_start, time_end, time_class) VALUES ('08:00:00', '20:00:00', '60')";
                                        $stmt2 = $pdo->prepare($sql2);
                                        $stmt2->execute();
                                    }
                                    $stmt->execute();
                                    $dataTime = $stmt->fetch();

                                    $stmt = null;
                                    $stmt2 = null;

                                    $thai_day_arr=array("จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์","อาทิตย์");
                                    $sc_startTime=date("Y-m-d $dataTime[time_start]");  // กำหนดเวลาเริ่มต้ม เปลี่ยนเฉพาะเลขเวลา
                                    $sc_endtTime=date("Y-m-d $dataTime[time_end]");  // กำหนดเวลาสื้นสุด เปลี่ยนเฉพาะเลขเวลา
                                    $sc_t_startTime=strtotime($sc_startTime);
                                    $sc_t_endTime=strtotime($sc_endtTime);
                                    $sc_numStep = $dataTime['time_class']; // ช่วงช่องว่างเวลา หน่ายนาที 60 นาที = 1 ชั่วโมง
                                    $num_dayShow=5;  // จำนวนวันที่โชว์ 1 - 7
                                    $sc_timeStep=array();
                                    $sc_numCol=0;
                                    while($sc_t_startTime<=$sc_t_endTime){
                                        $sc_timeStep[$sc_numCol]=date("H:i",$sc_t_startTime);    
                                        $sc_t_startTime=$sc_t_startTime+($sc_numStep*60); 
                                        $sc_numCol++;    // ได้จำนวนคอลัมน์ที่จะแสดง
                                    }
                                ?>
                                <table class="table table-bordered table-studentschedule text-center border-0 m-0">
                                    <thead>
                                        <tr>
                                            <th class="pl-0 pr-0 align-middle">วัน/เวลา</th>
                                            <?php for($i_time=0;$i_time<$sc_numCol-1;$i_time++){ ?>

                                            <th class="pl-0 pr-0"><?=$sc_timeStep[$i_time]?> - <?=$sc_timeStep[$i_time+1]?> </th>
                                            <?php } ?>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php for($i_day=0;$i_day<$num_dayShow;$i_day++){?>

                                        <tr>
                                            <td> <?=$thai_day_arr[$i_day]?> </td>
                                            <?php for($i_time=0;$i_time<$sc_numCol-1;$i_time++){ ?>
                                                
                                            <td> </td>
                                            <?php }?>

                                        </tr>
                                    <?php } ?>
                                    
                                    </tbody>
                                </table>