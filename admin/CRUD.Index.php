<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    if($action === "Logout"){
        session_destroy();
        exit;
    }

    if($action === "ChangePassword"){
        $id_teacher = filter_input(INPUT_POST,'id_teacher');
        $old_passwd = filter_input(INPUT_POST,'old_passwd');
        $new_passwd = filter_input(INPUT_POST,'new_passwd');
        $confirm_passwd = filter_input(INPUT_POST,'confirm_passwd');


        if($_SESSION['Role']==="ADMIN"){
            $sql = "SELECT * FROM tb_admin
            WHERE admin_id = :admin_id AND password = :old_passwd";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('admin_id',$id_teacher,PDO::PARAM_INT);
            $stmt->bindValue('old_passwd',md5($old_passwd),PDO::PARAM_STR);
            $stmt->execute();
            if($stmt->rowCount() > 0){
                $sql = "UPDATE tb_admin SET  password = :new_passwd
                        WHERE admin_id = :admin_id";
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue('admin_id',$id_teacher,PDO::PARAM_INT);
                $stmt->bindValue('new_passwd',md5($new_passwd),PDO::PARAM_STR);
                $stmt->execute();
                $stmt = null;
    
                $alert = array("type"=>"success", "title"=>"แก้ไขรหัสผ่านสำเร็จ"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else{
                $alert = array("type"=>"error", "title"=>"รหัสผ่านเก่าผิดกรุณากรอกใหม่อีครั้ง"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }else{
            $sql = "SELECT * FROM tb_teacher
            WHERE teacher_id = :id_teacher AND teacher_password = :old_passwd";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('id_teacher',$id_teacher,PDO::PARAM_INT);
            $stmt->bindValue('old_passwd',$old_passwd,PDO::PARAM_STR);
            $stmt->execute();
            if($stmt->rowCount() > 0){
                $sql = "UPDATE tb_teacher SET  teacher_password = :new_passwd
                        WHERE teacher_id = :id_teacher";
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue('id_teacher',$id_teacher,PDO::PARAM_INT);
                $stmt->bindValue('new_passwd',$new_passwd,PDO::PARAM_STR);
                $stmt->execute();
                $stmt = null;

                $alert = array("type"=>"success", "title"=>"แก้ไขรหัสผ่านสำเร็จ"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else{
                $alert = array("type"=>"error", "title"=>"รหัสผ่านเก่าผิดกรุณากรอกใหม่อีครั้ง"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }
    }

?>