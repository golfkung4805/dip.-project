
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> ตั้งค่าตำแหน่งงาน </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?php echo $_SERVER_NAME; ?>">หน้าแรก</a></li>
                                <li class="breadcrumb-item active"> ตั้งค่าตำแหน่งงาน </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-outline card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">ตั้งค่าตำแหน่ง</h3>
                                </div>
                                    <form id="modalFormPositionSetting">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg"></div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>ผู้อํานวยการ:</label>
                                                        <select class="form-control select-director" id="director" name="director" style="width: 100%;">
                                                    
                                                        </select>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>รองผู้อำนวยการฝ่ายวิชาการ:</label>
                                                        <select class="form-control select-dacademic" id="dacademic" name="dacademic" style="width: 100%;">
                                                    
                                                        </select>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>หัวหน้างานหลักสูตรฯ:</label>
                                                        <select class="form-control select-program" id="program" name="program" style="width: 100%;">
                                                    
                                                        </select>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                                <div class="col-lg"></div>
                                            </div>
                                        
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer text-right">
                                        <label class="mb-8">&nbsp;</label>
                                        <input type="hidden" id="action" name="action" value="Update">
                                        <input type="hidden" id="director_id" name="director_id" value="<?php //echo $dataTime['time_id']; ?>">
                                        <input type="hidden" id="dacademic_id" name="dacademic_id" value="<?php //echo $dataTime['time_id']; ?>">
                                        <input type="hidden" id="program_id" name="program_id" value="<?php //echo $dataTime['time_id']; ?>">
                                        <button type="submit" class="btn btn-primary">บันทึกข้อมูลตำแหน่งงาน</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>

