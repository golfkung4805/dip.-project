<?php
    $server = "localhost";
    $username = "root";
    $password = "";
    // $db = "std_testing";
    $db = "sr_system";
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4",
        PDO::ATTR_EMULATE_PREPARES => FALSE
    ];
    try{
        $pdo = new PDO("mysql:host=$server;dbname=$db", $username, $password, $options);
    }catch(PDOException $e){
        echo $e->getMessage();
    }
    // ! SELECT
    // try {
    //     $sch_id = "1349016401";
    //     $sql = "SELECT * FROM `depart` WHERE sch_id = :sch_id";
    //     $stmt = $pdo->prepare($sql);
    //     $stmt->execute([':sch_id'=>$sch_id]);
    //     // $result = $stmt->fetchAll();
    //     foreach($stmt as $row){
    //         echo $row['depname']."<br>";
    //     }
    // } catch (PDOException $e) {
    //     echo $e->getMessage();
    // }
    // ! INSERT
    // $name="ทดสอบๆ";
    // $age = 18;
    // try {
    // $sql = "INSERT INTO testing(name,age) VALUE( :name , :age)";
    // $stmt = $pdo->prepare($sql);
    // $stmt->bindParam(':name',$name,PDO::PARAM_STR);
    // $stmt->bindParam(':age',$age,PDO::PARAM_INT);
    // $stmt->execute();

    //! UPDATE
    // $name = "ทดสอบอัพเดจ";
    // $age= "999";
    // $id="5";
    // $sql = "UPDATE testing SET name=:name, age=:age WHERE id=:id";
    // $stmt = $pdo->prepare($sql);
    // $stmt->bindValue(':name',$name,PDO::PARAM_STR);
    // $stmt->bindValue(':age',$age,PDO::PARAM_INT);
    // $stmt->bindValue(':id',$id,PDO::PARAM_INT);
    // $stmt->execute();

    // ! DELETE
    // $id = 2;
    // $sql = "DELETE FROM testing WHERE id = :id";
    // $stmt = $pdo->prepare($sql);
    // $stmt->bindValue(':id',$id,PDO::PARAM_INT);
    // $stmt->execute();
    
?>