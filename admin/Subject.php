
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> ข้อมูลวิชาเรียน </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?php echo $_SERVER_NAME; ?>">หน้าหลัก</a></li>
                                <li class="breadcrumb-item active"> ข้อมูลวิชาเรียน </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Modal -->
            <div class="modal fade" id="modalSubject">
                <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content">
                        <form id="modalFormSubject">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modal-title">เพิ่มวิชาเรียน</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>รหัสวิชา</label>
                                            <input type="text" class="form-control" id="code_subject" name="code_subject" placeholder="กรอกรหัสวิชา ...">
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label>ชื่อวิชา</label>
                                            <input type="text" class="form-control" id="name_subject" name="name_subject" placeholder="กรอกชื่อวิชา ...">
                                        </div>
                                    </div>
                                    <!-- <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>ชื่อวิชา [Eng]</label>
                                            <input type="text" class="form-control" id="name_eng_subject" name="name_eng_subject" placeholder="กรอกชื่อวิชา[Eng] ...">
                                        </div>
                                    </div> -->
                                <!-- </div>
                                <div class="row"> -->
                                    <!-- <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>ประเภทวิชา</label>
                                            <input type="text" class="form-control" id="type_subject" name="type_subject" placeholder="กรอกประเภทวิชา ...">
                                        </div>
                                    </div> -->
                                    <!-- <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>คาบ</label>
                                            <input type="text" class="form-control" id="period_subject" name="period_subject" placeholder="กรอกคาบ ...">
                                        </div>
                                    </div> -->
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <label>ทฤษฏี</label>
                                            <input type="text" class="form-control" id="theory_subject" name="theory_subject" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <label>ปฏิบัติ</label>
                                            <input type="text" class="form-control" id="practice_subject" name="practice_subject" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <label>หน่วยกิต</label>
                                            <input type="text" class="form-control" id="credit_subject" name="credit_subject" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <input type="hidden" id="id_subject" name="id_subject">
                                <input type="hidden" id="action" name="action" value="Insert">
                                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                                <button type="submit" class="btn btn-primary">บันทึกข้อมูลวิชาเรียน</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /. Modal -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <table id="TableSubject" class="TableSubject table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ลำดับ</th>
                                                <th class="text-center">รหัสวิชา</th>
                                                <th>ชื่อวิชา</th>
                                                <th class="text-center">หน่วยกิต (ท-ป-น)</th>
                                                <th class="text-center">ตัวเลือก</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="5" height="70"></td>
                                            </tr>
                                        <tbody>    
                                        <tfoot>
                                            <tr>
                                                <th class="text-center">ลำดับ</th>
                                                <th class="text-center">รหัสวิชา</th>
                                                <th>ชื่อวิชา</th>
                                                <th class="text-center">หน่วยกิต (ท-ป-น)</th>
                                                <th class="text-center">ตัวเลือก</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col-md-12-->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>

