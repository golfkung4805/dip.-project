<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    if($action==="getPosition"){
        $sql = "SELECT * FROM tb_position_setting";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt = null;

        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="Update"){
        $director = filter_input(INPUT_POST,"director");
        $dacademic = filter_input(INPUT_POST,"dacademic");
        $program = filter_input(INPUT_POST,"program");

        $director_id = filter_input(INPUT_POST,"director_id");
        $dacademic_id = filter_input(INPUT_POST,"dacademic_id");
        $program_id = filter_input(INPUT_POST,"program_id");

        $sql = "UPDATE tb_position_setting SET teacher_code = :teacher_code WHERE pos_position = :pos_position";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':teacher_code',$director,PDO::PARAM_STR);
        $stmt->bindValue(':pos_position',$director_id,PDO::PARAM_STR);
        $stmt->execute();

        $sql = "UPDATE tb_position_setting SET teacher_code = :teacher_code WHERE pos_position = :pos_position";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':teacher_code',$dacademic,PDO::PARAM_STR);
        $stmt->bindValue(':pos_position',$dacademic_id,PDO::PARAM_STR);
        $stmt->execute();

        $sql = "UPDATE tb_position_setting SET teacher_code = :teacher_code WHERE pos_position = :pos_position";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':teacher_code',$program,PDO::PARAM_STR);
        $stmt->bindValue(':pos_position',$program_id,PDO::PARAM_STR);
        $stmt->execute();

        $stmt = null;

        // $sql = "SELECT * FROM tb_position_setting";
        // $stmt = $pdo->prepare($sql);
        // $stmt->execute();
        // $result = $stmt->fetchAll();
        // $stmt = null;

        // echo json_encode($result,JSON_UNESCAPED_UNICODE);

        // $alert = array("type"=>"success", "title"=>"เปลี่ยนตารางเวลาสำเร็จ"); 
        // echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
        exit;
    }

    if($action==="GetTeacher"){
        $sql = "SELECT * FROM tb_teacher";
        $stmt = $pdo->query($sql);
        $GetTeacher[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            $GetTeacher[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name']); 
        }
        $stmt = null;
        
        echo json_encode($GetTeacher,JSON_UNESCAPED_UNICODE);
        exit;
    }

?>