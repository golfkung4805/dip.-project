
        <?php include('CRUD.EducationProgram.php'); ?>

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> ข้อมูลแผนการเรียน </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?php echo $_SERVER_NAME; ?>">หน้าหลัก</a></li>
                                <li class="breadcrumb-item active"> ข้อมูลแผนการเรียน </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Modal -->
            <div class="modal fade" id="modalSubject">
                <div class="modal-dialog modal-dialog-centered modal-xl">
                    <div class="modal-content">
                        <form id="modalFormSubject">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modal-title">เพิ่มวิชาเรียน</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>แผนกวิชา</label>
                                            <select class="form-control select-department" id="department" name="department" style="width: 100%;" disabled>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>กลุ่มเรียน</label>
                                            <select class="form-control select-study-group" id="studygroup" name="studygroup" style="width: 100%;" disabled>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>ภาคเรียน</label>
                                            <input type="text" class="form-control" id="term" name="term" placeholder="" disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>ปีการศึกษา</label>
                                            <input type="text" class="form-control" id="year" name="year" placeholder="" disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>วิชาเรียน</label>
                                            <select class="form-control select-subject-code" id="subject_code" name="subject_code" style="width: 100%;">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>วิชา</label>
                                            <input type="text" class="form-control" id="subject_name" name="subject_name" placeholder="" disabled>
                                        </div>
                                    </div> -->

                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <label>คาบ</label>
                                            <input type="text" class="form-control" id="period" name="period" placeholder="" disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <label>ทฤษฏี</label>
                                            <input type="text" class="form-control" id="theory" name="theory" placeholder="" disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <label>ปฏิบัติ</label>
                                            <input type="text" class="form-control" id="practice" name="practice" placeholder="" disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <label>หน่วยกิต</label>
                                            <input type="text" class="form-control" id="credit" name="credit" placeholder="" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <input type="hidden" id="plan_id" name="plan_id">
                                <input type="hidden" id="action" name="action" value="Insert">
                                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                                <button type="submit" class="btn btn-primary">บันทึกข้อวิชาเรียน</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /. Modal -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card card-primary card-outline">
                                    <div class="card-header">
                                        <div class="row form-group m-0">
                                            <label class="col-lg-1 col-form-label text-right">แผนก</label>
                                            <div class="col-lg-3">
                                                <select class="form-control select_dept" id="select_dept" name="select_dept" style="width: 100%;">
                                                    
                                                </select>
                                            </div>
                                            
                                            <label class="col-lg-1 col-form-label text-right">กลุ่มเรียน</label>
                                            <div class="col-lg-3">
                                                <select class="form-control select_group" id="select_group" name="select_group" style="width: 100%;">
                                                    
                                                </select>
                                            </div>

                                            <label class="col-lg-1 col-form-label text-right">ปีการศึกษา</label>
                                            <div class="col-lg-3">
                                                <select class="form-control select_year" id="select_year" name="select_year" style="width: 100%;">
                                                    
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="TableEducationProgram" class="TableEducationProgram table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">ลำดับ</th>
                                                    <th class="text-center">รหัสวิชา</th>
                                                    <th>ชื่อวิชา</th>
                                                    <th class="text-center">คาบ</th>
                                                    <th class="text-center">หน่วยกิต(ท-ป-น)</th>
                                                    <th>ครูผู้สอน</th>
                                                    <th class="text-center">ตัวเลือก</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-center">ลำดับ</th>
                                                    <th class="text-center">รหัสวิชา</th>
                                                    <th>ชื่อวิชา</th>
                                                    <th class="text-center">คาบ</th>
                                                    <th class="text-center">หน่วยกิต(ท-ป-น)</th>
                                                    <th>ครูผู้สอน</th>
                                                    <th class="text-center">ตัวเลือก</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card --> 
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>

