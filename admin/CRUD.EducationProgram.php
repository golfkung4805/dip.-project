<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    function LoadTableEducationProgram($group_code,$term){
        global $pdo;
        // $sql = "SELECT * FROM tb_study_group
        //         LEFT JOIN tb_teacher ON tb_study_group.teacher_code = tb_teacher.teacher_code 
        //         WHERE dept_code = (SELECT dept_code FROM tb_dept WHERE dept_name = :group OR dept_code = :code)";
        // $stmt = $pdo->prepare($sql);
        // $stmt->bindValue('group',$group,PDO::PARAM_STR);
        // $stmt->bindValue('code',$group,PDO::PARAM_STR);
        // $stmt->execute();
        // return $stmt;
        $request=$_POST;
        $col =array(
            0   =>  'plan_id',
            1   =>  'tb_plan.subject_code',
            2   =>  'subject_name',
            3   =>  'period',
            4   =>  'theory-practice-credit',
            5   =>  'gender-first_name-last_name',
            6   =>  'plan_id'
        );  //create column LIKE table in database

        //Search
        // $sql = "SELECT * FROM tb_study_group
        //         LEFT JOIN tb_teacher ON tb_study_group.teacher_code = tb_teacher.teacher_code 
        //         WHERE 1=1 AND dept_code = (SELECT dept_code FROM tb_dept WHERE dept_name = :group OR dept_code = :code)";
        $sql = "SELECT *,tb_subject.subject_code as subject_codee FROM tb_plan
                LEFT JOIN tb_subject ON tb_plan.subject_code = tb_subject.subject_code
                LEFT JOIN tb_teacher ON tb_plan.teacher_code = tb_teacher.teacher_code
                WHERE 1=1 AND group_code = :group_code AND term = :term";
        if(!empty($request['search']['value'])){
            $sql.=" AND (plan_id LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR tb_plan.subject_code LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR subject_name LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR period LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR CONCAT(theory, ' ' ,practice, ' ' ,credit) LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR CONCAT(gender, ' ' ,first_name, ' ' ,last_name) LIKE '%".$request['search']['value']."%' )";
            
        }
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue('term',$term,PDO::PARAM_STR);
        $stmt->execute();
        $totalData = $stmt->rowCount();
        $totalFilter=$totalData;
        $stmt = null;
        //Order
        $sql.=" ORDER BY ".$col[$request['order'][0]['column']]."   ".$request['order'][0]['dir']."  LIMIT ".
            $request['start']."  ,".$request['length']."  ";

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue('term',$term,PDO::PARAM_STR);
        $stmt->execute();

        $data=array();
        $count = $request['start']+1;
        foreach($stmt as $row){ 
            $subdata=array();
            // $subdata[]=$row['id']; //id
            $subdata[]="<div class=\"text-center\">".$count."</div>";
            $subdata[]="<div class=\"text-center\">".$row['subject_codee']."</div>"; //dept_code
            $subdata[]="<div class=\"text-left\">".$row['subject_name']."</div>"; //dept_name
            $subdata[]="<div class=\"text-center\">".$row['period']."/".($row['theory']+$row['practice'])."</div>"; //dept_name
            $subdata[]="<div class=\"text-center\">".$row['theory']."-".$row['practice']."-".$row['credit']."</div>"; //dept_name               $row[0] is id in table on database
            $subdata[]="<div class=\"text-left\">".$row['gender']." ".$row['first_name']." ".$row['last_name']."</div>"; //teacher_code
            $subdata[]="<div class=\"text-center\"><button class=\"btn btn-outline-warning btn-update mr-3\" data-id=\"$row[plan_id]\">แก้ไข</button> <button class=\"btn btn-outline-danger btn-delete\" data-id=\"$row[plan_id]\">ลบ</button></div>";
            $data[]=$subdata;
            $count++;
        }

        $json_data=array(
            "draw"              =>  intval($request['draw']),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalFilter),
            "data"              =>  $data
        );
        return $json_data;
    }

    if($action==="LoadTableEducationProgram"){
        $dept = $_POST['dept_code'];
        $group_code = $_POST['group_code'];
        $term = $_POST['select_year'];

        $dataTable = LoadTableEducationProgram($group_code,$term);
        echo json_encode($dataTable,JSON_UNESCAPED_UNICODE ); 
        exit;
    }

    if($action==="GetDepartment"){
        $sql = "SELECT * FROM tb_dept 
                ORDER BY dept_name";
        $stmt = $pdo->query($sql);
        $GetDepartment[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['dept_code']===$_POST['dept_code']){
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name'] , "selected"=>true); 
            }else{
                $GetDepartment[] = array("id"=>$row['dept_code'], "text"=>$row['dept_name']); 
            }
            
        }
        $stmt = null;
        
        echo json_encode($GetDepartment,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetGroup"){
        $dept_code = $_POST['dept_code'];
        $sql = "SELECT * FROM tb_study_group 
                WHERE dept_code = :dept_code
                ORDER BY group_code";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('dept_code',$dept_code,PDO::PARAM_STR);
        $stmt->execute();

        $GetGroup[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['group_code']===$_POST['group_code']){
                $GetGroup[] = array("id"=>$row['group_code'], "text"=>$row['group_code']." | ".$row['group_subname'] , "selected"=>true); 
            }else{
                $GetGroup[] = array("id"=>$row['group_code'], "text"=>$row['group_code']." | ".$row['group_subname']); 
            }
            
        }
        
        $stmt = null;
        
        echo json_encode($GetGroup,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetSubject"){
        $check = $_POST['check'];
        $subject_code = $_POST['subject_code'];
        
        $sql = "SELECT * FROM tb_subject";
        if($check!=="ALL"){
            $sql .= " WHERE subject_code = :subject_code";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('subject_code',$subject_code,PDO::PARAM_STR);
            $stmt->execute();
            $result = $stmt->fetchAll();

            $GetSubject = $result;
        }else{
            $stmt = $pdo->query($sql);

            $GetSubject[] = array("id"=>"", "text"=>""); 
            foreach($stmt as $row){
                if($subject_code===$row['subject_code']){
                    $GetSubject[] = array("id"=>$row['subject_code'], "text"=>$row['subject_code']." | ".$row['subject_name'], "selected"=>true); 
                }else{
                    $GetSubject[] = array("id"=>$row['subject_code'], "text"=>$row['subject_code']." | ".$row['subject_name']); 
                }
                
            }
        }
        $stmt = null;
        
        echo json_encode($GetSubject,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetYear"){
        $Year = date("Y")+543;

        $NowYear = substr($Year,0,2);

        $group_code = $_POST['group_code'];
        $subYear = intval(substr($group_code,0,2));

        $subClass = substr($group_code,2,1);

        if($subClass==="2"){
            $loop = 3; // ปวช เรียน 3 ปี
        }else if($subClass==="3"){
            $loop = 2; // ปวช เรียน 2 ปี
        }
        $Output[] = array("id"=>"", "text"=>""); 
        for($i=1;$i<=$loop;$i++){
            if($subYear<10){
                $returns = "1/".$NowYear."0".$subYear;
                $Output[] = array("id"=>$returns, "text"=>$returns); 

                $returns = "2/".$NowYear."0".$subYear;
                $Output[] = array("id"=>$returns, "text"=>$returns); 

                if($i === 1 && $loop === 2){
                    $returns = "S/".$NowYear."0".$subYear;
                    $Output[] = array("id"=>$returns, "text"=>$returns); 
                }
            }else{
                $returns = "1/".$NowYear.$subYear;
                $Output[] = array("id"=>$returns, "text"=>$returns); 

                $returns = "2/".$NowYear.$subYear;
                $Output[] = array("id"=>$returns, "text"=>$returns); 

                if($i === 1 && $loop === 2){
                    $returns = "S/".$NowYear.$subYear;
                    $Output[] = array("id"=>$returns, "text"=>$returns); 
                }
                
            }

            $subYear++;
            if($subYear>99){
                $subYear = intval(substr($subYear,-2));
                $NowYear++;
            }
        }

        echo json_encode($Output,JSON_UNESCAPED_UNICODE);
        exit;
    }

    // if($action==="GetTeacher"){
    //     $sql = "SELECT * FROM tb_teacher";
    //     $stmt = $pdo->query($sql);
    //     $GetTeacher[] = array("id"=>"", "text"=>""); 
    //     foreach($stmt as $row){
    //         if($row['teacher_code']===$_POST['teacher_code']){
    //             $GetTeacher[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name'], "selected"=>true); 
    //         }else{
    //             $GetTeacher[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name']); 
    //         }
    //     }
    //     $stmt = null;
        
    //     echo json_encode($GetTeacher,JSON_UNESCAPED_UNICODE);
    //     exit;
    // }

    if($action==="getPlan"){
        $plan_id = filter_input(INPUT_POST,"plan_id");

        $sql = "SELECT * FROM tb_plan
                wHERE plan_id = :plan_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('plan_id',$plan_id,PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt = null;

        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="Insert"){
        $subject_code = filter_input(INPUT_POST,"subject_code");
        $dept_code = filter_input(INPUT_POST,"dept_code");
        $group_code = filter_input(INPUT_POST,"group_code");
        $term = filter_input(INPUT_POST,"term");
 
        $sql = "SELECT subject_code 
                FROM tb_plan 
                WHERE subject_code = :subject_code AND group_code = :group_code AND term = :term";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':subject_code',$subject_code,PDO::PARAM_STR);
        $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
        $stmt->bindValue(':term',$term,PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $alert = array("type"=>"warning", "title"=>"รหัสวิชา $subject_code มีในแผนแล้ว"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $sql = "INSERT INTO tb_plan (subject_code, group_code, term)
                    VALUE(:subject_code, :group_code, :term)";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue(':subject_code',$subject_code,PDO::PARAM_STR);
            $stmt->bindValue(':group_code',$group_code,PDO::PARAM_STR);
            $stmt->bindValue(':term',$term,PDO::PARAM_STR);
            $stmt->execute();
            $stmt = null;

            $alert = array("type"=>"success", "title"=>"เพิ่มวิชาสำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }

    if($action==="Update"){
        $subject_code = filter_input(INPUT_POST,"subject_code");
        $plan_id = filter_input(INPUT_POST,"plan_id");
        
        $sql = "SELECT subject_code,
                    (SELECT count(subject_code) FROM tb_plan WHERE subject_code = :subject_code) as CheckCode
                    FROM tb_plan 
                    WHERE plan_id = :plan_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':subject_code',$subject_code,PDO::PARAM_STR);
        $stmt->bindValue(':plan_id',$plan_id,PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $row = $stmt->fetch();
            if($row['subject_code'] === $subject_code || $row['CheckCode'] <= 0){
                $sql = "UPDATE tb_plan SET subject_code = :subject_code 
                        WHERE plan_id = :plan_id";
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue(':subject_code',$subject_code,PDO::PARAM_STR);
                $stmt->bindValue(':plan_id',$plan_id,PDO::PARAM_INT);
                $stmt->execute();
                $stmt = null;

                $alert = array("type"=>"success", "title"=>"แก้ไขวิชาในแผนสำเร็จ"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else{
                $alert = array("type"=>"warning", "title"=>"รหัสวิชา $subject_code มีในแผนแล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }else{
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }

    if($action==="Delete"){
        $plan_id = filter_input(INPUT_POST,"plan_id");

        $sql = "SELECT * FROM tb_plan 
                WHERE plan_id = :plan_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('plan_id',$plan_id,PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount()===0){
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $sql = "DELETE FROM tb_plan
                    WHERE plan_id = :plan_id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('plan_id',$plan_id,PDO::PARAM_INT);
            $stmt->execute();
            $stmt = null;

            $alert = array("type"=>"success", "title"=>"ลบวิชาสำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }


?>