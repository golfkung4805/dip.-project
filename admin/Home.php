
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> หน้าหลัก </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item active">หน้าหลัก</li>
                                <li class="breadcrumb-item"> </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">

                    <!-- <div class="row">
                        <div class="col-md-12">
                            <div class="card card-outline card-purple">
                                <div class="card-header">
                                    <h3 class="card-title">ข้อมูลสถิติ</h3>
                                </div>
                                
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="card card-outline card-primary">
                                                <div class="card-header card-menu-home">
                                                    <a href="?view=ChartStudent">
                                                        <i class="fas fa-chart-line fa-3x"></i>
                                                        <h3 class="card-title"> สถิตินักศึกษา </h3>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- /.row -->
                    <?php if($_SESSION['Role'] === "ADMIN" || $_SESSION['Role'] === "ChiefDEP"){ ?>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-outline card-purple">
                                <div class="card-header">
                                    <h3 class="card-title">ข้อมูลทะเบียน</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <?php if($_SESSION['Role']==="ADMIN"){ ?>

                                        <div class="col-md-3 mx-auto">
                                            <div class="card card-outline card-primary">
                                                <div class="card-header card-menu-home">
                                                    <a href="?view=Department">
                                                        <i class="fas fa-sitemap fa-3x"></i>
                                                        <h3 class="card-title text-center">  ข้อมูลแผนก  </h3>
                                                    </a>
                                                </div>
                                                <!-- /.card-header -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                        <!-- /.col-md-3 -->
                                        <?php } ?>

                                        <div class="col-md-3 mx-auto">
                                            <div class="card card-outline card-primary">
                                                <div class="card-header card-menu-home">
                                                    <a href="?view=Teacher">
                                                        <i class="fas fa-user-graduate fa-3x"></i>
                                                        <h3 class="card-title">   ข้อมูลอาจารย์   </h3>
                                                    </a>
                                                </div>
                                                <!-- /.card-header -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                        <!-- /.col-md-3 -->
                                        <div class="col-md-3 mx-auto">
                                            <div class="card card-outline card-primary">
                                                <div class="card-header card-menu-home">
                                                    <a href="?view=StudyGroup">
                                                        <i class="fas fa-book-reader fa-3x"></i>
                                                        <h3 class="card-title">  ข้อมูลกลุ่มเรียน  </h3>
                                                    </a>
                                                </div>
                                                <!-- /.card-header -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                        <!-- /.col-md-3 -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col-md-12 -->
                        <?php } ?>

                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-outline card-purple">
                                <div class="card-header">
                                    <h3 class="card-title">ข้อมูลระบบ</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-3 mx-auto">
                                            <div class="card card-outline card-primary">
                                                <div class="card-header card-menu-home">
                                                    <a href="?view=Subject">
                                                        <i class="fas fa-book-open fa-3x"></i>
                                                        <h3 class="card-title">  ข้อมูลวิชาเรียน  </h3>
                                                    </a>
                                                </div>
                                                <!-- /.card-header -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                         <!-- /.col-md-3 -->
                                         <div class="col-md-3 mx-auto">
                                            <div class="card card-outline card-primary">
                                                <div class="card-header card-menu-home">
                                                    <a href="?view=Room">
                                                        <i class="fas fa-building fa-3x"></i>
                                                        <h3 class="card-title">  ข้อมูลห้องเรียน  </h3>
                                                    </a>
                                                </div>
                                                <!-- /.card-header -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                        <!-- /.col-md-3 -->
                                        <div class="col-md-3 mx-auto">
                                            <div class="card card-outline card-primary">
                                                <div class="card-header card-menu-home">
                                                    <a href="?view=EducationProgram">
                                                        <i class="fas fa-pencil-alt fa-3x"></i>
                                                        <h3 class="card-title">  ข้อมูลแผนการเรียน  </h3>
                                                    </a>
                                                </div>
                                                <!-- /.card-header -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                        <!-- /.col-md-3 -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col-md-12 -->

                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-6 mx-auto">
                            <div class="card card-outline card-purple">
                                <div class="card-header">
                                    <h3 class="card-title">จัดตารางเรียน</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="card card-outline card-primary">
                                                <div class="card-header card-menu-home">
                                                    <a href="?view=StudentSchedule">
                                                        <i class="fas fa-calendar-alt fa-3x"></i>
                                                        <h3 class="card-title">  จัดตารางเรียนนักศึกษา  </h3>
                                                    </a>
                                                </div>
                                                <!-- /.card-header -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                        <!-- /.col-md-3 -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col-md-12 -->
                        <?php if($_SESSION['Role']==="ADMIN"){ ?>

                        <div class="col-md-6">
                            <div class="card card-outline card-purple">
                                <div class="card-header">
                                    <h3 class="card-title">ตั้งค่าระบบ</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 mx-auto">
                                            <div class="card card-outline card-primary">
                                                <div class="card-header card-menu-home">
                                                    <a href="?view=PositionSetting">
                                                        <i class="fas fa-sitemap fa-3x"></i>
                                                        <h3 class="card-title">  ตั้งค่าตำแหน่งงาน  </h3>
                                                    </a>
                                                </div>
                                                <!-- /.card-header -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                        <!-- /.col-md-3 -->

                                        <div class="col-md-6 mx-auto">
                                            <div class="card card-outline card-primary">
                                                <div class="card-header card-menu-home">
                                                    <a href="?view=TimeSetting">
                                                        <i class="fas fa-clock fa-3x"></i>
                                                        <h3 class="card-title">  ตั้งค่าตารางเวลาเรียน  </h3>
                                                    </a>
                                                </div>
                                                <!-- /.card-header -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                        <!-- /.col-md-3 -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col-md-12 -->
                        <?php } ?>

                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
