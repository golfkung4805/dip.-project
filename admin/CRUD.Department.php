<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    // $code_teach = "a1a";
    // $prefix_teach = "a1";
    // $first_name_teach = "a1";
    // $last_name_teach = "a1";
    // $department_teach = "a1";

    // $sql = "INSERT INTO tb_teacher (teacher_code, gender, first_name, last_name, department) VALUE(:code_teach, :prefix_teach, :first_name_teach, :last_name_teach, :department_teach)";
    // $stmt = $pdo->prepare($sql);
    // $stmt->bindValue('code_teach',$code_teach,PDO::PARAM_STR);
    // $stmt->bindValue('prefix_teach',$prefix_teach,PDO::PARAM_STR);
    // $stmt->bindValue('first_name_teach',$first_name_teach,PDO::PARAM_STR);
    // $stmt->bindValue('last_name_teach',$last_name_teach,PDO::PARAM_STR);
    // $stmt->bindValue('department_teach',$department_teach,PDO::PARAM_STR);
    // $stmt->execute();
    

    function LoadTableDepartment(){
        global $pdo;

        $request=$_POST;
        $col =array(
            0   =>  'dept_id',
            1   =>  'tb_dept.dept_code',
            2   =>  'dept_name',
            3   =>  'gender-first_name-last_name',
            4   =>  'dept_id'
        );  //create column LIKE table in database

        //Search
        $sql = "SELECT *,tb_dept.dept_code as dept_codee FROM `tb_dept` 
                LEFT JOIN tb_teacher ON tb_dept.teacher_code = tb_teacher.teacher_code  
                WHERE 1=1";
        if(!empty($request['search']['value'])){
            $sql.=" AND (dept_id LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR tb_dept.dept_code LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR dept_name LIKE '%".$request['search']['value']."%' ";
            $sql.=" OR CONCAT(gender, ' ' ,first_name, ' ' ,last_name) LIKE '%".$request['search']['value']."%' )";
            
        }
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $totalData = $stmt->rowCount();
        $totalFilter=$totalData;
        $stmt = null;
        //Order
        $sql.=" ORDER BY ".$col[$request['order'][0]['column']]."   ".$request['order'][0]['dir']."  LIMIT ".
            $request['start']."  ,".$request['length']."  ";

        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $data=array();
        $count = $request['start']+1;
        foreach($stmt as $row){ 
            $subdata=array();
            // $subdata[]=$row['id']; //id
            $subdata[]="<div class=\"text-center\">".$count."</div>";
            $subdata[]="<div class=\"text-center\">".$row['dept_codee']."</div>"; //dept_code
            $subdata[]="<div class=\"text-left\">".$row['dept_name']."</div>"; //dept_name               $row[0] is id in table on database
            $subdata[]="<div class=\"text-left\">".$row['gender']." ".$row['first_name']." ".$row['last_name']."</div>"; //teacher_code
            $subdata[]="<div class=\"text-center\"><button class=\"btn btn-outline-warning btn-update mr-3\" data-id=\"$row[dept_id]\">แก้ไข</button> <button class=\"btn btn-outline-danger btn-delete\" data-id=\"$row[dept_id]\">ลบ</button></div>";
            $data[]=$subdata;
            $count++;
        }

        $json_data=array(
            "draw"              =>  intval($request['draw']),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalFilter),
            "data"              =>  $data
        );
        return $json_data;
    }

    if($action==="LoadTableDepartment"){
        $dataTable = LoadTableDepartment();
        echo json_encode($dataTable,JSON_UNESCAPED_UNICODE ); 
        exit;
    }

    if($action==="GetTeacher"){
        $sql = "SELECT * FROM tb_teacher";
        $stmt = $pdo->query($sql);
        if($stmt->rowCount() <= 0){
            $GetTeacher[] = array("id"=>"0", "text"=>"ยังไม่มีหัวหน้าแผนก", "selected"=>true); 
        }else{
            // $GetTeacher[] = array("id"=>"", "text"=>""); 
            $GetTeacher[] = array("id"=>"0", "text"=>"ยังไม่มีหัวหน้าแผนก", "selected"=>true); 
            foreach($stmt as $row){
                if($row['teacher_code']===$_POST['teacher_code']){
                    $GetTeacher[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name'], "selected"=>true); 
                }else{
                    $GetTeacher[] = array("id"=>$row['teacher_code'], "text"=>$row['gender'].$row['first_name']." ".$row['last_name']); 
                }
            }
            $stmt = null;
        }
        
        
        echo json_encode($GetTeacher,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="getDetailDepartment"){
        $dept_id = filter_input(INPUT_POST,'dept_id');

        $sql = "SELECT * FROM tb_dept 
                WHERE dept_id = :dept_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('dept_id',$dept_id,PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $stmt = null;

        echo json_encode($result,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="Insert"){
        $code_dep = filter_input(INPUT_POST,'code_dep');
        $name_dep = filter_input(INPUT_POST,'name_dep');
        $chief_dep = filter_input(INPUT_POST,'chief_dep');
        if($chief_dep=="0"){
            $chief_dep = NULL;
        }
        $sql = "SELECT (SELECT dept_code FROM tb_dept WHERE dept_code = :dept_code)as dept_codee,
                        (SELECT dept_name  FROM tb_dept WHERE dept_name  = :dept_name )as dept_namee,
                        (SELECT teacher_code  FROM tb_dept WHERE teacher_code  = :teacher_code )as teacher_codee";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':dept_code',$code_dep,PDO::PARAM_STR);
        $stmt->bindValue(':dept_name',$name_dep,PDO::PARAM_STR);
        $stmt->bindValue(':teacher_code',$chief_dep,PDO::PARAM_STR);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $row = $stmt->fetch();
            if($row['dept_codee']!=""){
                $alert = array("type"=>"warning", "title"=>"รหัสแผนก $code_dep มีในระบบแล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else if($row['dept_namee']!=""){
                $alert = array("type"=>"warning", "title"=>"ชื่อแผนก $name_dep มีในระบบแล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else if($row['teacher_codee']!=""){
                $alert = array("type"=>"warning", "title"=>"รหัสอาจารย์ท่านนี้ $chief_dep เป็นหัวหน้าแผนกแล้ว"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else if($row['dept_codee']=="" AND $row['dept_namee']=="" AND $row['teacher_codee']==""){
                $sql = "INSERT INTO tb_dept (dept_code, dept_name, teacher_code) 
                        VALUE(:code_dep, :name_dep, :chief_dep)";
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue('code_dep',$code_dep,PDO::PARAM_STR);
                $stmt->bindValue('name_dep',$name_dep,PDO::PARAM_STR);
                $stmt->bindValue('chief_dep',$chief_dep,PDO::PARAM_STR);
                $stmt->execute();
                $stmt = null;
                $alert = array("type"=>"success", "title"=>"เพิ่มแผนกสำเร็จ"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else{
                $alert = array("type"=>"error", "title"=>"กรุณาตรวจสอบความถูกต้องของข้อมูล"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }else{
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
        }
    }

    if($action==="Update"){
        $name_dep = filter_input(INPUT_POST,'name_dep');
        $chief_dep = filter_input(INPUT_POST,'chief_dep');
        $id_dep = filter_input(INPUT_POST,'id_dep');
        if($chief_dep=="0"){
            $chief_dep = NULL;
        }
        // ! ใช้ NOT IN เช็คง่ายกว่า
        $sql = "SELECT dept_name,(SELECT count(dept_name) FROM tb_dept WHERE dept_name = :dept_name) as CheckName,
                        teacher_code,(SELECT count(teacher_code) FROM tb_dept WHERE teacher_code = :teacher_code) as CheckTeach
                    FROM tb_dept 
                    WHERE dept_id = :dept_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':dept_name',$name_dep,PDO::PARAM_STR);
        $stmt->bindValue(':teacher_code',$chief_dep,PDO::PARAM_STR);
        $stmt->bindValue(':dept_id',$id_dep,PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount() > 0){
            $row = $stmt->fetch();
            if($row['dept_name'] === $name_dep || $row['CheckName'] <= 0 AND $row['teacher_code'] === $chief_dep || $row['CheckTeach'] <= 0){
                $sql = "UPDATE tb_dept SET  dept_name = :name_dep, 
                                            teacher_code = :chief_dep 
                        WHERE dept_id = :id_dep";
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue('name_dep',$name_dep,PDO::PARAM_STR);
                $stmt->bindValue('chief_dep',$chief_dep,PDO::PARAM_STR);
                $stmt->bindValue('id_dep',$id_dep,PDO::PARAM_INT);
                $stmt->execute();
                $stmt = null;

                $alert = array("type"=>"success", "title"=>"แก้ไขแผนกสำเร็จ"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }else{
                if($row['dept_name']!=$name_dep){
                    $alert = array("type"=>"warning", "title"=>"ชื่อแผนก $name_dep มีในระบบแล้ว"); 
                    echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                    exit;
                }else if($row['teacher_code']!=$chief_dep){
                    $alert = array("type"=>"warning", "title"=>"รหัสอาจารย์ท่านนี้ $chief_dep เป็นหัวหน้าแผนกอื่นแล้ว"); 
                    echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                    exit;
                }
            }
        }else{
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }

    if($action==="Delete"){
        $dept_id = filter_input(INPUT_POST,'dept_id');

        $sql = "SELECT * FROM tb_dept 
                WHERE dept_id = :dept_id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('dept_id',$dept_id,PDO::PARAM_INT);
        $stmt->execute();

        if($stmt->rowCount()===0){
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $sql = "DELETE FROM tb_dept 
                    WHERE dept_id = :dept_id";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('dept_id',$dept_id,PDO::PARAM_INT);
            $stmt->execute();
            $stmt = null;

            $alert = array("type"=>"success", "title"=>"ลบแผนกสำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }
?>