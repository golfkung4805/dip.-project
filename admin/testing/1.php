<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>devbanban.com</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function() { //
            $("#brand").change(function() { //

                $.ajax({
                    url: "2.php", //ทำงานกับไฟล์นี้
                    data: "bid=" + $("#brand").val(), //ส่งตัวแปร
                    type: "POST",
                    async: false,
                    success: function(data, status) {
						console.log(data);
                        $("#model").html(data);

                    }

                });
                //return flag;
            });
        });
    </script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>
                    ตัวอย่างระบบ Ajax Select/Option : devbanban <br />
                    ตย.ข้อมูล เลือกยี่ห้อ แสดง รุ่นกล้อง
                </h2>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <form class="form-horizontal">
                    <div class="form-group">
                        <?php
							$con= mysqli_connect("localhost","root","","db_testing") or die("Error: " . mysqli_error($con));
							mysqli_query($con, "SET NAMES 'utf8' "); 
							$sql= "SELECT * FROM  tb_brand" or die("Error:" . mysqli_error());
							$result = mysqli_query($con, $sql);
								echo"BRAND : <select id='brand' name='bid' class='form-control'>";
										echo"<option value=''>-Select-</option>";
										while($row = mysqli_fetch_array($result)){
											echo"<option value='$row[0]'>".$row["bname"]."</option>";
										}
								echo"</select>";
							echo '</div>';
							echo '<div class="form-group">';
	
								mysqli_close($con);
									
							?>
							
							MODEL : <select id='model' name='mid' class='form-control'>
							<option value=''>-Select-</option>
							</select>
                </form>
                <hr />
                dev by <a href="http://devbanban.com/">
                    devbanban.com
                </a>
            </div>
        </div>
    </div>
</body>

</html>