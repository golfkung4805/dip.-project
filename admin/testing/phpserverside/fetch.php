<?php
$con=mysqli_connect('localhost','root','','db_testing');
if(isset($_POST['action'])){
    $action = $_POST['action'];
}else{
    $action = null;
}
if($action=="LoadData"){   
    $request=$_POST;
    $col =array(
        0   =>  'id',
        1   =>  'name',
        2   =>  'salary',
        3   =>  'age'
    );  //create column like table in database

    //Search
    $sql ="SELECT * FROM tbperson WHERE 1";
    $request_data = mysqli_real_escape_string($con,$request['search']['value']);
    if(!empty($request_data)){
        $sql.=" AND (id Like '".$request_data."%' ";
        $sql.=" OR name Like '".$request_data."%' ";
        $sql.=" OR salary Like '".$request_data."%' ";
        $sql.=" OR age Like '".$request_data."%' )";
    }
    $query=mysqli_query($con,$sql);
    $totalData=mysqli_num_rows($query);
    $totalFilter=$totalData;
    //Order
    $sql.=" ORDER BY ".$col[$request['order'][0]['column']]."   ".$request['order'][0]['dir']."  LIMIT ".
        $request['start']."  ,".$request['length']."  ";

    $query=mysqli_query($con,$sql);

    $data=array();

    while($row=mysqli_fetch_array($query)){
        $subdata=array();
        $subdata[]=$row[0]; //id
        $subdata[]=$row[1]; //name
        $subdata[]=$row[2]; //salary
        $subdata[]=$row[3]; //age                $row[0] is id in table on database
        $subdata[]='<button type="button" id="getEdit" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal" data-id="'.$row[0].'"><i class="glyphicon glyphicon-pencil">&nbsp;</i>Edit</button>
                    <a href="index.php?delete='.$row[0].'" onclick="return confirm(\'Are You Sure ?\')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash">&nbsp;</i>Delete</a>';
        $data[]=$subdata;
    }

    $json_data=array(
        "draw"              =>  intval($request['draw']),
        "recordsTotal"      =>  intval($totalData),
        "recordsFiltered"   =>  intval($totalFilter),
        "data"              =>  $data
    );

    echo json_encode($json_data);

}
?>
