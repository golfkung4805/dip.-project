
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"> ตั้งค่าตารางเวลาเรียน </h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?php echo $_SERVER_NAME; ?>">หน้าแรก</a></li>
                                <li class="breadcrumb-item active"> ตั้งค่าตารางเวลาเรียน </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card schedule">
                                <?php include('TimeSettingSampleTable.php'); ?> 
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-outline card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">ตั้งค่าเวลาเริ่ม-สิ้นสุดเวลาเรียน</h3>
                                </div>
                                <form id="modalFormTimeSetting">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>เริ่มเวลาเรียน:</label>
                                                    <div class="input-group date" id="time_start" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input time_start" id="time_start" name="time_start"  data-target="#time_start" value="<?php echo $dataTime['time_start']; ?>">
                                                        <div class="input-group-append" id="time_start_target" data-target="#time_start" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>สิ้นสุดเวลาเรียน:</label>
                                                    <div class="input-group date" id="time_end" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input time_end" id="time_end" name="time_end" data-target="#time_end" value="<?php echo $dataTime['time_end']; ?>">
                                                        <div class="input-group-append" id="time_end_target" data-target="#time_end" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label>เวลาคาบเรียน(ชั่วโมง):</label>
                                                    <div class="input-group date" id="time_class" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input time_class" id="time_class" name="time_class" data-target="#time_class" value="<?php echo floor($dataTime['time_class']/60).":".($dataTime['time_class']%60); ?>">
                                                        <div class="input-group-append" id="time_class_target" data-target="#time_class" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="fa fa-stopwatch"></i></div>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer text-right">
                                        <label class="mb-8">&nbsp;</label>
                                        <input type="hidden" id="action" name="action" value="Update">
                                        <input type="hidden" id="time_id" name="time_id" value="<?php echo $dataTime['time_id']; ?>">
                                        <button type="submit" class="btn btn-primary">เปลี่ยนตารางเวลา</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>

