                                <?php
                                    include('./include/DBConfig.SR.php');

                                    $sql = "SELECT * FROM tb_time_setting";
                                    $stmt = $pdo->prepare($sql);
                                    $stmt->execute();
                                    $numRow = $stmt->rowCount();
                                    if($numRow>1){
                                        $sql2 = "";
                                        $stmt2 = $pdo->prepare($sql2);
                                        $stmt2->execute();
                                    }
                                    if($numRow<1 || $numRow>1){
                                        $sql2 = "TRUNCATE sr_system.tb_time_setting";
                                        $stmt2 = $pdo->prepare($sql2);
                                        $stmt2->execute();
                                        $sql2 = "INSERT INTO tb_time_setting (time_start, time_end, time_class) VALUES ('08:00:00', '20:00:00', '60')";
                                        $stmt2 = $pdo->prepare($sql2);
                                        $stmt2->execute();
                                    }
                                    $stmt->execute();
                                    $dataTime = $stmt->fetch();

                                    $stmt = null;
                                    $stmt2 = null;
                                    // ส่วนของตัวแปรสำหรับกำหนด
                                    $thai_day_arr=array("จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์","อาทิตย์");
                                    
                                    ////////////////////// ส่วนของการจัดการตารางเวลา /////////////////////
                                    $sc_startTime=date("Y-m-d $dataTime[time_start]");  // กำหนดเวลาเริ่มต้ม เปลี่ยนเฉพาะเลขเวลา
                                    $sc_endtTime=date("Y-m-d $dataTime[time_end]");  // กำหนดเวลาสื้นสุด เปลี่ยนเฉพาะเลขเวลา
                                    $sc_t_startTime=strtotime($sc_startTime);
                                    $sc_t_endTime=strtotime($sc_endtTime);
                                    $sc_numStep = $dataTime['time_class']; // ช่วงช่องว่างเวลา หน่ายนาที 60 นาที = 1 ชั่วโมง
                                    $num_dayShow=5;  // จำนวนวันที่โชว์ 1 - 7
                                    $sc_timeStep=array();
                                    $sc_numCol=0;
                                    ////////////////////// ส่วนของการจัดการตารางเวลา /////////////////////
                                    while($sc_t_startTime<=$sc_t_endTime){
                                        $sc_timeStep[$sc_numCol]=date("H:i",$sc_t_startTime);    
                                        $sc_t_startTime=$sc_t_startTime+($sc_numStep*60); 
                                        $sc_numCol++;    // ได้จำนวนคอลัมน์ที่จะแสดง
                                    }
                                    // print_r($sc_timeStep);
                                    ///////////////// ส่วนของข้อมูล ที่ดึงจากฐานข้อมูบ ////////////////////////
                                    $data_schedule=array();
                                    if(isset($_POST['room_code'])){
                                        $room_code = $_POST['room_code'];
                                    }else{
                                        $room_code = NULL;
                                    }
                                    if(isset($_POST['term'])){
                                        $term = $_POST['term'];
                                    }else{
                                        $term = NULL;
                                    }
                                    // $teacher_code = "ม.1/1";
                                    for($i=1;$i<=$num_dayShow;$i++){
                                        $schedule=0;
                                        $sql="
                                            SELECT sch_date,sch_start_time,sch_end_time  FROM tb_schedule 
                                            WHERE sch_date = '".$i."' AND room_code = '$room_code' AND term = '$term'
                                            GROUP BY sch_start_time";
                                        // $result = $mysqli->query($sql);  
                                        $result = $pdo->prepare($sql);
                                        $result->execute();
                                        // $schedule = $result->num_rows;
                                        foreach($result as $row){
                                            
                                        $data_schedule[$i][$schedule] = array();
                                        $sql2="
                                            SELECT * FROM tb_schedule 
                                            LEFT JOIN tb_subject ON tb_schedule.subject_code = tb_subject.subject_code 
                                            LEFT JOIN tb_teacher ON tb_schedule.teacher_code = tb_teacher.teacher_code 
                                            LEFT JOIN tb_room ON tb_schedule.room_code = tb_room.room_code 
                                            LEFT JOIN tb_study_group ON tb_schedule.group_code = tb_study_group.group_code 
                                            WHERE sch_date = '$i' AND tb_schedule.room_code = '$room_code'
                                            AND sch_start_time = '".$row['sch_start_time']."' AND sch_end_time = '".$row['sch_end_time']."'";
                                            // $result2 = $mysqli->query($sql2);
                                            $result2 = $pdo->prepare($sql2);
                                            $result2->execute();
                                            $num = $result2->rowCount();
                                            // echo "<pre>";
                                            // print_r($row);
                                            // echo $num;
                                            // echo "</pre>";
                                            if($num>1){
                                                $name = "";
                                                $detail = "";
                                                $room = "";
                                                $numBR = 1;
                                                foreach($result2 as $row2){
                                                    $sch_start_time = $sc_timeStep[$row2['sch_start_time']-1];
                                                    $sch_end_time = $sc_timeStep[$row2['sch_end_time']];
                                                    // print_r($sch_start_time);
                                                    $chkOverlap = "danger";
                                                    $start_time = $sch_start_time.":00";
                                                    $end_time = $sch_end_time.":00";
                                                    $detail .= $row2['subject_code'];
                                                    $name .= "<br>".$row2['subject_name'];
                                                    // $teacher_name = $row2['gender'].$row2['first_name']." ".$row2['last_name'];
                                                    $teacher_name = $row2['first_name'];
                                                    $room = $row2['group_subname'];
                                                    if($num!==$numBR){
                                                        $room .= "<br>";
                                                    }
                                                    $numBR++;
                                                }
                                            }else{
                                                foreach($result2 as $row2){
                                                    $sch_start_time = $sc_timeStep[$row2['sch_start_time']-1];
                                                    $sch_end_time = $sc_timeStep[$row2['sch_end_time']];
                                                    // print_r($sch_start_time);
                                                    $chkOverlap = "activity";
                                                    $start_time = $sch_start_time.":00";
                                                    $end_time = $sch_end_time.":00";
                                                    $detail = $row2['subject_code'];
                                                    $name = "<br>".$row2['subject_name'];
                                                    // $teacher_name = $row2['gender'].$row2['first_name']." ".$row2['last_name'];
                                                    $teacher_name = $row2['first_name'];
                                                    $room = $row2['group_subname'];
                                                }
                                            }
                                                array_push($data_schedule[$i][$schedule],$chkOverlap);
                                                array_push($data_schedule[$i][$schedule],$start_time);
                                                array_push($data_schedule[$i][$schedule],$end_time);
                                                array_push($data_schedule[$i][$schedule],$detail);
                                                array_push($data_schedule[$i][$schedule],$name);
                                                array_push($data_schedule[$i][$schedule],$teacher_name);
                                                array_push($data_schedule[$i][$schedule],$room);
                                                $schedule++;
                                        }
                                    }
                                    $result = null;
                                    $result2 = null;
                                    ///////////////// ส่วนของข้อมูล ที่ดึงจากฐานข้อมูบ ////////////////////////
                                    // echo "<pre>";
                                    // var_export($data_schedule);
                                    // echo "</pre>";
                                ?>
                                <!-- <div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div> -->
                                <table class="table table-bordered table-studentschedule text-center border-0 m-0">
                                    <thead>
                                        <tr>
                                            <!-- <th class="pl-0 pr-0">วัน/คาบ</th> -->
                                            <?php //for($i_time=0;$i_time<$sc_numCol-1;$i_time++){ ?>
                                            <!-- <th class="pl-0 pr-0"><?php //echo $i_time+1; ?> </th> -->
                                            <?php //} ?>

                                            <th class="pl-0 pr-0 align-middle">วัน/เวลา</th>
                                            <?php for($i_time=0;$i_time<$sc_numCol-1;$i_time++){ ?>

                                            <th class="pl-0 pr-0"><?=$sc_timeStep[$i_time]?> - <?=$sc_timeStep[$i_time+1]?> </th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php // วนลูปแสดงจำนวนวันตามที่กำหนด
                                        for($i_day=0;$i_day<$num_dayShow;$i_day++){
                                        ?>
                                        <tr id="day_<?php echo $i_day+1; ?>">
                                            <td><?=$thai_day_arr[$i_day]?> </td>
                                            <?php
                                            // ตรวจสอบและกำหนดช่วงเวลาให้สอดคล้องกับช้อมูล        
                                            if(isset($data_schedule[$i_day+1])){
                                                $num_data=count($data_schedule[$i_day+1]);
                                                $NumData = $num_data;
                                            }else{
                                                $num_data=0;
                                            }
                                            $arr_chkOverlap=array();
                                            $arr_checkSpan=array();
                                            $arr_detailShow=array();
                                            $arr_nameShow=array();
                                            $arr_teacherShow=array();
                                            $arr_roomShow=array();
                                            $arr_delShow=array();
                                            for($i_time=0;$i_time<$sc_numCol-1;$i_time++){
                                                if($num_data>0){
                                                    $chkOverlap="";
                                                    $haveIN=0;
                                                    $dataShow="";
                                                    $name="";
                                                    $teacher="";
                                                    $room="";
                                                    $del="";
                                                    foreach($data_schedule[$i_day+1] as $k=>$v){
                                                        /*
                                                            $v['0'] = chkOverlap
                                                            $v['1'] = start_time
                                                            $v['2'] = end_time
                                                            $v['3'] = title
                                                            $v['4'] = name
                                                            $v['5'] = teacher
                                                            $v['6'] = room
                                                        */
                                                        if(($sc_timeStep[$i_time].":00" == $v['1'])
                                                        ){
                                                            $chkOverlap = $v['0'];
                                                            $haveIN++; 
                                                            $dataShow=$v['3'];
                                                            // $name = $v['4'];
                                                            $teacher="<br>".$v['5'];
                                                            $room = "<br>".$v['6'];
                                                            // $del="<br><a href=\"processSchedule.php?act=del&class=$teacher_code&start=$v[1]&end=$v[2]\">ลบ</a>";
                                                            $add=1;
                                                            while($sc_timeStep[$i_time+$add].":00" < $v['2']){
                                                                $chkOverlap = $v['0'];
                                                                $haveIN++; 
                                                                $dataShow=$v['3'];
                                                                // $name = $v['4'];
                                                                $teacher="<br>".$v['5'];
                                                                $room = "<br>".$v['6'];
                                                                $add++;
                                                            }
                                                        }
                                                    }

                                                    $arr_chkOverlap[$i_time]=$chkOverlap;
                                                    $arr_checkSpan[$i_time]=$haveIN;
                                                    $arr_detailShow[$i_time]=$dataShow;
                                                    // $arr_nameShow[$i_time]=$name;
                                                    $arr_teacherShow[$i_time]=$teacher;
                                                    $arr_roomShow[$i_time]=$room;
                                                    // $arr_delShow[$i_time]=$del;
                                                }
                                            }
                                            // echo "<pre>";
                                            // var_export($arr_chkOverlap);
                                            // var_export($arr_nameShow);
                                            // echo "</pre>";
                                            // echo "<hr>";
                                            for($i_time=0;$i_time<$sc_numCol-1;$i_time++){
                                            // for($i_time=0;$i_time<1;$i_time++){
                                                $colspan="";
                                                $css_use="";
                                                $dataShowIN="";
                                                if(isset($arr_checkSpan[$i_time])){
                                                    if($arr_checkSpan[$i_time]>0){
                                                        if($arr_chkOverlap[$i_time]=="activity"){
                                                            $css_use="class=\"activity\"";
                                                        }else{
                                                            $css_use="class=\"danger\"";
                                                        }
                                                        $dataShowIN=$arr_detailShow[$i_time]; 
                                                        // $dataShowIN.=$arr_nameShow[$i_time];
                                                        $dataShowIN.=$arr_roomShow[$i_time];
                                                        $dataShowIN.=$arr_teacherShow[$i_time];
                                                        // $dataShowIN.=$arr_delShow[$i_time];
                                                    }
                                                    if($arr_checkSpan[$i_time]>1){
                                                        $colspan="colspan=\"".$arr_checkSpan[$i_time]."\"";
                                                        $step_add=$arr_checkSpan[$i_time]-1;
                                                        $i_time+=$step_add;
                                                    }       
                                                }
                                                // //echo "<pre>";
                                                // var_export($dataShowIN);
                                                // //echo "</pre>";
                                            ?>
                                            <td <?=$css_use?> <?=$colspan?>>
                                            <?php echo $dataShowIN; ?>
                                            </td>
                                            <?php }?>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>