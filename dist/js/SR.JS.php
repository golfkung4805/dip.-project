<?php
    switch ($view) {
        case 'Login':
            echo "<script src=\"dist/js/SR.Login.js\"></script>";
            break;
        case 'ForgotPassword':
            echo "<script src=\"dist/js/SR.ForgotPassword.js\"></script>";
            break;
        case 'RecoverPassword':
            echo "<script src=\"dist/js/SR.RecoverPassword.js\"></script>";
            break;
        case 'StudentSchedule':
            echo "<script src=\"dist/js/SR.StudentSchedule.js\"></script>";
            break;
        case 'TeacherSchedule':
            echo "<script src=\"dist/js/SR.TeacherSchedule.js\"></script>";
            break;
        case 'RoomSchedule':
            echo "<script src=\"dist/js/SR.RoomSchedule.js\"></script>";
            break;
        default:
            // echo "<script src=\"dist/js/SR.HOME.js\"></script>";
    }
?>
