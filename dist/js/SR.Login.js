function ToastAlert(type, title) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3500
    });

    Toast.fire({
        type: type,
        title: title,
    })
}

function SweetAlert(type, title, text, button, footer) {
    Swal.fire({
        type: type,
        title: title,
        text: text,
        confirmButtonText: button,
        footer: footer
    });
}

if(typeof Cookies.get('UserName')!=="undefined"){
    $('#username').val(Cookies.get('UserName'));
}

$(document).ready(function() {
    $('#FormLogin').on("submit", function(e) {
        e.preventDefault();
        var username = $('#username').val();
        var password = $('#password').val();

        if ($('#username').val().trim() === "") {
            ToastAlert('warning', 'กรุณากรอกรหัสอาจารย์');
            $('#username').focus();
        } else if ($('#password').val().trim() === "") {
            ToastAlert('warning', 'กรุณากรอกรหัสผ่านอาจารย์');
            $('#password').focus();
        } else {
            $.ajax({
                type: "POST",
                url: "CRUD.Index.php",
                data: {
                    action: "Login",
                    username: username,
                    password: password
                },
                dataType: "json",
                // processData: false,
                // contentType: false,
                cache: false,
                success: function(data) {
                    Cookies.set('UserName',username);
                    if(typeof data.alert !== "undefined"){
                        if (data.alert.type === "warning") {
                            if(data.alert.status === "username"){
                                $('#username').focus();
                            }else if(data.alert.status === "password"){
                                $('#password').focus();
                            }
                            ToastAlert(data.alert.type, data.alert.title);
                        }
                    }else{
                        if(data.Role === null){
                            Cookies.set('CK_Chack_Status',data.dept_code);
                        }else{
                            Cookies.set('CK_Chack_Status','ADMIN');
                        }
                        $('#FormLogin')[0].reset();
                        window.location="http://localhost/SR/admin/";
                    }
                },
                error: function(error) {
                    console.error(error.responseText);
                    ToastAlert('error', 'เกิดข้อผิดผลาด');
                }
            });
        }
    });


});