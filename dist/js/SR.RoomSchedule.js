$(function () {
    function LoadDataRoomSchedule(room_code, select_year){
        if(room_code !== null && select_year !== null){
            var ToDay = new Date();
            $.ajax({
                type: "POST",
                url: "RoomScheduleTable.php",
                data:  { room_code : room_code,
                        term: select_year  },
                beforeSend:function(){ 
                    $('.loading').remove();
                    $('.schedule').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (RoomScheduleTable) {
                    $(".schedule").html(RoomScheduleTable);
                    $('.loading').remove();
                    $('#day_'+ToDay.getDay()).css("background-color","#dee2e6");
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });
            $('.print-schedule').removeClass("invisible").addClass("visible");
        }else{
            $('.print-schedule').removeClass("visible").addClass("invisible");
            $(".schedule").load("ScheduleSampleTable.php");
        }
    }


    dataNull = [{ id: "", text: '' }];

    function select_year(){
        $('.select_year').empty();
        $('#select_year').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกห้องเรียนก่อน",
            disabled: true,
            data: dataNull
        });
    }

    function select2room_reset(room_code){
        var GetDepartment = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.RoomSchedule.php",
                data: { action:"GetRoom",
                        room_code:room_code
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
            
        }();

        $('.select_room').empty();
        $('.select_room').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกห้องเรียน",
            allowClear: true,
            data: GetDepartment
        });
        
        if(typeof Cookies.get('RoomCK_room_code')==="undefined"){
            select_year();
        }else{
            select2year_reset();
        }
    }

    function select2year_reset(){
        if(typeof Cookies.get('RoomCK_term')==="undefined"){
            RoomCK_term = "NULL";
        }else{
            RoomCK_term = Cookies.get('RoomCK_term');
        }
        var GetYear = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.RoomSchedule.php",
                data: { action:"GetYear",
                        select_year:RoomCK_term    
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();
        
        $('.select_year').empty();
        $('.select_year').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกปีการศึกษา",
            allowClear: true,
            disabled: false,
            data: GetYear
        });
    }
    
    //  ! ตัวเก่า
    if(typeof Cookies.get('RoomCK_room_code')!=="undefined"){
        select2room_reset(Cookies.get('RoomCK_room_code'));
    }else{
        Cookies.remove('RoomCK_room_code');
        Cookies.remove('RoomCK_term');

        select2room_reset("NULL");
        LoadDataRoomSchedule(null,null);
    }

    if(typeof Cookies.get('RoomCK_term')!=="undefined"){
        select2year_reset(Cookies.get('RoomCK_term')); 
        LoadDataRoomSchedule(Cookies.get('RoomCK_room_code'),Cookies.get('RoomCK_term'));
    }else{
        Cookies.remove('RoomCK_term');

        LoadDataRoomSchedule(null,null);
    }
    
    $('#select_room').change(function () { 
        var room_code = $(this).val();
        if(room_code !== ""){
            Cookies.set('RoomCK_room_code', room_code);
            Cookies.remove('RoomCK_term');
            select2year_reset(); 
            LoadDataRoomSchedule(room_code,null);
        }else{
            // $(".schedule").load("ScheduleSampleTable.php");
            Cookies.remove('RoomCK_room_code');
            Cookies.remove('RoomCK_term');
            select_year();
            LoadDataRoomSchedule(null,null);
        }
    });

    $('#select_year').change(function () { 
        var select_year = $(this).val();
        if(select_year !== ""){
            Cookies.set('RoomCK_term', select_year);
            var room_code = $('#select_room').val();
            LoadDataRoomSchedule(room_code,select_year);
        }else{
            // $(".schedule").load("ScheduleSampleTable.php");
            Cookies.remove('RoomCK_term');
            LoadDataRoomSchedule(room_code,null);
        }
    });

    $('.print-schedule').click(function (e) { 
        Cookies.set('staReport', 'RoomSchedule');
        window.open("Report.php", "_blank", "width=1000, height=700");
        // $(".schedule2 table").removeClass();
        // $(".schedule2 table").addClass("table table-bordered table-studentschedule text-center");
        
    });
});