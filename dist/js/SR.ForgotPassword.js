function ToastAlert(type, title) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3500
    });

    Toast.fire({
        type: type,
        title: title,
    })
}

function SweetAlert(type, title, text, button, footer) {
    Swal.fire({
        type: type,
        title: title,
        text: text,
        confirmButtonText: button,
        footer: footer
    });
}

$(document).ready(function() {
    $('#FormForgotPassword').on("submit", function(e) {
        e.preventDefault();

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        var username = $('#username').val();
        var tomail = $('#tomail').val();

        if ($('#username').val().trim() === "") {
            ToastAlert('warning', 'กรุณากรอกรหัสอาจารย์');
            $('#username').focus();
        } else if ($('#tomail').val().trim() === "") {
            ToastAlert('warning', 'กรุณากรอกอีเมล');
            $('#tomail').focus();
        }else if(!validateEmail(tomail)){
            ToastAlert('warning', 'กรุณากรอกอีเมลให้ถูกต้อง');
            console.log(1)
            $('#tomail').focus();
        } else {
            $.ajax({
                type: "POST",
                url: "CRUD.Index.php",
                data: {
                    action: "ForgotPassword",
                    username: username,
                    email: tomail
                },
                beforeSend:function(){ 
                    ToastAlert('info', 'กำลังส่งอีเมล');
                    $('.btn-primary').attr('disabled','disabled');
                    $('.btn-primary').append("<i class=\"fas fa-spinner fa-spin btn-loading\"></i>");
                },
                dataType: "json",
                // processData: false,
                // contentType: false,
                cache: false,
                success: function(data) {
                    ToastAlert(data.alert.type, data.alert.title);
                    $('.btn-primary').removeAttr("disabled");
                    $('.btn-loading').remove();
                    if(data.alert.type==="success"){
                        $('#FormForgotPassword')[0].reset();
                    }
                },
                error: function(error) {
                    console.error(error.responseText);
                    $('.btn-primary').removeAttr("disabled");
                    $('.btn-loading').remove();
                    ToastAlert('error', 'เกิดข้อผิดผลาด');
                }
            });
        }
    });


});