function ToastAlert(type, title) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3500
    });

    Toast.fire({
        type: type,
        title: title,
    })
}

function SweetAlert(type, title, text, button, footer) {
    Swal.fire({
        type: type,
        title: title,
        text: text,
        confirmButtonText: button,
        footer: footer
    });
}

$(document).ready(function() {
    param = window.location.search.split("token=");
    $('#token').val(param[1]);
    $.ajax({
        type: "POST",
        url: "CRUD.Index.php",
        data: {
            action: "GetRecoverPassword",
            token:param[1]
        },
        dataType: "json",
        // processData: false,
        // contentType: false,
        cache: false,
        success: function(data) {
            if(data.alert.type==="success"){
                console.log(data['data']);
                $('#username').val(data['data'].teacher_code);
                $('#name').val(data['data'].gender+data['data'].first_name+" "+data['data'].last_name);
            }else{
                window.location="http://localhost/SR/?view=Error401";
            }
        },
        error: function(error) {
            console.error(error.responseText);
            ToastAlert('error', 'เกิดข้อผิดผลาด');
            window.location="http://localhost/SR/?view=Error401";
        }
    });
    


    $('#FormRecoverPassword').on("submit", function(e) {
        e.preventDefault();
        var token = $('#token').val();
        var new_password = $('#new_password').val();
        var confirm_password = $('#confirm_password').val();

        if($('#new_password').val().trim()===""){
            ToastAlert('warning','กรุณากรอกรหัสผ่านใหม่');
            $('#new_password').focus();
        }else if($('#new_password').val().length < 8){
            ToastAlert('warning','รหัสผ่านควรมี 8 ตัวขึ้นไป');
            $('#new_password').focus();
        }else if($('#confirm_password').val().trim()===""){
            ToastAlert('warning','กรุณากรอกยืนยันรหัสผ่าน');
            $('#confirm_password').focus();
        }else if($('#new_password').val() !== $('#confirm_password').val()){
            ToastAlert('warning','รหัสผ่านใหม่หรือยืนยันรหัสผ่านไม่ตรงกัน');
            $('#confirm_password').focus();
        }else{
            $.ajax({
                type: "POST",
                url: "CRUD.Index.php",
                data: {
                    action: "RecoverPassword",
                    token:token,
                    confirm_password:confirm_password
                },
                beforeSend:function(){ 
                    $('.btn-primary').attr('disabled','disabled');
                },
                dataType: "json",
                // processData: false,
                // contentType: false,
                cache: false,
                success: function(data) {
                    ToastAlert(data.alert.type, data.alert.title);
                    $('.btn-primary').removeAttr("disabled");
                    if(data.alert.type==="success"){
                        setTimeout(function () {
                            window.location="http://localhost/SR/?view=Login";
                        }, 2000);
                    }
                },
                error: function(error) {
                    console.error(error.responseText);
                    $('.btn-primary').removeAttr("disabled");
                    ToastAlert('error', 'เกิดข้อผิดผลาด');
                }
            });
        }
    });


});