$(function () {
    function LoadDataTeacherSchedule(dept_code,teach_code,select_year){
        if(dept_code !== null && teach_code !== null && select_year !== null){
            var ToDay = new Date();
            $.ajax({
                type: "POST",
                url: "TeacherScheduleTable.php",
                data:  { teach_code : teach_code,
                        term: select_year  },
                beforeSend:function(){ 
                    $('.loading').remove();
                    $('.schedule').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (TeacherScheduleTable) {
                    $(".schedule").html(TeacherScheduleTable);
                    $('.loading').remove();
                    $('#day_'+ToDay.getDay()).css("background-color","#dee2e6");
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });
            $('.print-schedule').removeClass("invisible").addClass("visible");
        }else{
            $('.print-schedule').removeClass("visible").addClass("invisible");
            $(".schedule").load("ScheduleSampleTable.php");
        }
    }

    dataNull = [{ id: "", text: '' }];

    function select_teach(){
        $('.select_teach').empty();
        $('.select_teach').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนกวิชาก่อน",
            disabled: true,
            data: dataNull
        });
    }

    function select_year(status){
        var NotSelect = "กรุณาเลือกอาจารย์เรียนก่อน";
        if(status==="NotSelect"){
            NotSelect = "กรุณาเลือกแผนกและอาจารย์เรียนก่อน"
        }

        $('.select_year').empty();
        $('#select_year').select2({
            theme: 'bootstrap4',
            placeholder: NotSelect,
            disabled: true,
            data: dataNull
        });
    }

    function select2dept_reset(dept_code){
        var GetDepartment = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.TeacherSchedule.php",
                data: { action:"GetDepartment",
                        dept_code:dept_code
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
            
        }();

        $('.select_dept').empty();
        $('.select_dept').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนกวิชา",
            allowClear: true,
            data: GetDepartment
        });
        
        if(typeof Cookies.get('TeachCK_dept_code')==="undefined"){
            select_teach();
            select_year("NotSelect");
        }else{
            select2teach_reset(dept_code);
            select_year();
        }
    }

    function select2teach_reset(dept_code){

        if(typeof Cookies.get('TeachCK_teach_code')==="undefined"){
            TeachCK_teach_code = "NULL";
        }else{
            TeachCK_teach_code = Cookies.get('TeachCK_teach_code');
        }

        var GetTeach = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.TeacherSchedule.php",
                data: { action:"GetTeach",
                        dept_code:dept_code,
                        teach_code:TeachCK_teach_code   
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });
            return data;
        }();
        
        $('.select_teach').empty();
        $('.select_teach').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกอาจารย์",
            allowClear: true,
            disabled: false,
            data: GetTeach
        });

        if(TeachCK_teach_code==="NULL"){
            select_year();
        }else{
            select2year_reset(Cookies.get('TeachCK_teach_code'));
        }
        
    }

    function select2year_reset(){
        if(typeof Cookies.get('TeachCK_term')==="undefined"){
            TeachCK_term = "NULL";
        }else{
            TeachCK_term = Cookies.get('TeachCK_term');
        }
        var GetYear = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.TeacherSchedule.php",
                data: { action:"GetYear",
                        select_year:TeachCK_term    
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();
        
        $('.select_year').empty();
        $('.select_year').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกปีการศึกษา",
            allowClear: true,
            disabled: false,
            data: GetYear
        });
    }
    
    //  ! ตัวเก่า
    if(typeof Cookies.get('TeachCK_dept_code')!=="undefined"){
        select2dept_reset(Cookies.get('TeachCK_dept_code'));
    }else{
        Cookies.remove('TeachCK_dept_code');
        Cookies.remove('TeachCK_teach_code');
        Cookies.remove('TeachCK_term');

        select2dept_reset("NULL");
        LoadDataTeacherSchedule(null,null,null);
    }

    //  ! ตัวเก่า
    if(typeof Cookies.get('TeachCK_teach_code')!=="undefined"){
        select2teach_reset(Cookies.get('TeachCK_dept_code')); 
    }else{
        Cookies.remove('TeachCK_teach_code');
        Cookies.remove('TeachCK_term');

        LoadDataTeacherSchedule(null,null,null);
    }

    if(typeof Cookies.get('TeachCK_term')!=="undefined"){
        select2year_reset(Cookies.get('TeachCK_teach_code')); 
        LoadDataTeacherSchedule(Cookies.get('TeachCK_dept_code'),Cookies.get('TeachCK_teach_code'),Cookies.get('TeachCK_term'));
    }else{
        Cookies.remove('TeachCK_term');

        LoadDataTeacherSchedule(null,null,null);
    }


    
    $('#select_dept').change(function () { 
        var dept_code = $(this).val();
        if(dept_code !== ""){
            Cookies.set('TeachCK_dept_code', dept_code);
            Cookies.remove('TeachCK_teach_code');
            Cookies.remove('TeachCK_term');
            select2teach_reset(dept_code); 
            LoadDataTeacherSchedule(dept_code,null,null);
        }else{
            // $(".schedule").load("ScheduleSampleTable.php");
            Cookies.remove('TeachCK_dept_code');
            Cookies.remove('TeachCK_teach_code');
            Cookies.remove('TeachCK_term');
            select_teach();
            select_year("NotSelect");
            LoadDataTeacherSchedule(null,null,null);
        }
    });

    $('#select_teach').change(function () { 
        var dept_code = $('#select_dept').val();
        var teach_code = $(this).val();
        if(teach_code !== ""){
            Cookies.set('TeachCK_teach_code', teach_code);
            Cookies.remove('TeachCK_term');
            select2year_reset(teach_code); 
            LoadDataTeacherSchedule(dept_code,teach_code,null);
        }else{
            // $(".schedule").load("ScheduleSampleTable.php");
            Cookies.remove('TeachCK_teach_code');
            Cookies.remove('TeachCK_term');
            select_year();
            LoadDataTeacherSchedule(dept_code,null,null);
        }
    });
    
    $('#select_year').change(function () { 
        var select_year = $(this).val();
        if(select_year !== ""){
            Cookies.set('TeachCK_term', select_year);
            var dept_code = $('#select_dept').val();
            var teach_code = $('#select_teach').val();
            LoadDataTeacherSchedule(dept_code,teach_code,select_year);
        }else{
            // $(".schedule").load("ScheduleSampleTable.php");
            Cookies.remove('TeachCK_term');
            LoadDataTeacherSchedule(dept_code,teach_code,null);
        }
    });

    $('.print-schedule').click(function (e) { 
        Cookies.set('staReport', 'TeacherSchedule');
        window.open("Report.php", "_blank", "width=1000, height=700");
        // $(".schedule2 table").removeClass();
        // $(".schedule2 table").addClass("table table-bordered table-studentschedule text-center");
        
    });
});