$(function () {
    function LoadDataStudentSchedule(dept_code,group_code,select_year){
        if(dept_code !== null && group_code !== null && select_year !== null){
            var ToDay = new Date();
            $.ajax({
                type: "POST",
                url: "StudentScheduleTable.php",
                data:  { group_code : group_code,
                        term: select_year  },
                beforeSend:function(){ 
                    $('.loading').remove();
                    $('.schedule').append('<div class="loading overlay d-flex justify-content-center align-items-center"><i class="fas fa-2x fa-sync fa-spin"></i></div>');
                },
                success: function (StudentScheduleTable) {
                    $(".schedule").html(StudentScheduleTable);

                    $('.loading').remove();
                    $('#day_'+ToDay.getDay()).css("background-color","#dee2e6");
                },
                error: function(error) {
                    console.error(error.responseText);
                    SweetAlert('error', 'เกิดข้อผิดผลาด', null, 'ตกลง', null);
                }
            });
            $('.print-schedule').removeClass("invisible").addClass("visible");
        }else{
            $('.print-schedule').removeClass("visible").addClass("invisible");
            $(".schedule").load("ScheduleSampleTable.php");
        }
    }

    dataNull = [{ id: "", text: '' }];

    function select_group(){
        $('.select_group').empty();
        $('.select_group').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนกวิชาก่อน",
            disabled: true,
            data: dataNull
        });
    }

    function select_year(status){
        var NotSelect = "กรุณาเลือกกลุ่มเรียนก่อน";
        if(status==="NotSelect"){
            NotSelect = "กรุณาเลือกแผนกและกลุ่มเรียนก่อน"
        }

        $('.select_year').empty();
        $('#select_year').select2({
            theme: 'bootstrap4',
            placeholder: NotSelect,
            disabled: true,
            data: dataNull
        });
    }

    function select2dept_reset(dept_code){
        var GetDepartment = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetDepartment",
                        dept_code:dept_code
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
            
        }();

        $('.select_dept').empty();
        $('.select_dept').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกแผนกวิชา",
            allowClear: true,
            data: GetDepartment
        });
        
        if(typeof Cookies.get('STDCK_dept_code')==="undefined"){
            select_group();
            select_year("NotSelect");
        }else{
            select2group_reset(dept_code);
            select_year();
        }
    }

    function select2group_reset(dept_code){
        if(typeof Cookies.get('STDCK_group_code')==="undefined"){
            STDCK_group_code = "NULL";
        }else{
            STDCK_group_code = Cookies.get('STDCK_group_code');
        }
        var GetGroup = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetGroup",
                        dept_code:dept_code,
                        group_code:STDCK_group_code   
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();

        $('.select_group').empty();
        $('.select_group').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกกลุ่มเรียน",
            allowClear: true,
            disabled: false,
            data: GetGroup
        });

        if(STDCK_group_code==="NULL"){
            select_year();
        }else{
            select2year_reset(Cookies.get('STDCK_group_code'));
        }
        
    }

    function select2year_reset(group_code){
        if(typeof Cookies.get('STDCK_term')==="undefined"){
            STDCK_term = "NULL";
        }else{
            STDCK_term = Cookies.get('STDCK_term');
        }
        var GetYear = function () {
            var data = null;
            $.ajax({
                async: false,
                type: "POST",
                url:"CRUD.StudentSchedule.php",
                data: { action:"GetYear",
                        group_code:group_code,
                        select_year:STDCK_term    
                },
                dataType: "json",
                success: function (result) {
                    data = result;
                }
            });
            return data;
        }();
        
        $('.select_year').empty();
        $('.select_year').select2({
            theme: 'bootstrap4',
            placeholder: "กรุณาเลือกปีการศึกษา",
            allowClear: true,
            disabled: false,
            data: GetYear
        });
    }
    
    //  ! ตัวเก่า
    if(typeof Cookies.get('STDCK_dept_code')!=="undefined"){
        select2dept_reset(Cookies.get('STDCK_dept_code'));
    }else{
        Cookies.remove('STDCK_dept_code');
        Cookies.remove('STDCK_group_code');
        Cookies.remove('STDCK_term');

        select2dept_reset("NULL");
        LoadDataStudentSchedule(null,null,null);
    }

    //  ! ตัวเก่า
    if(typeof Cookies.get('STDCK_group_code')!=="undefined"){
        select2group_reset(Cookies.get('STDCK_dept_code')); 
    }else{
        Cookies.remove('STDCK_group_code');
        Cookies.remove('STDCK_term');

        LoadDataStudentSchedule(null,null,null);
    }

    if(typeof Cookies.get('STDCK_term')!=="undefined"){
        select2year_reset(Cookies.get('STDCK_group_code')); 
        LoadDataStudentSchedule(Cookies.get('STDCK_dept_code'),Cookies.get('STDCK_group_code'),Cookies.get('STDCK_term'));
    }else{
        Cookies.remove('STDCK_term');

        LoadDataStudentSchedule(null,null,null);
    }


    
    $('#select_dept').change(function () { 
        var dept_code = $(this).val();
        if(dept_code !== ""){
            Cookies.set('STDCK_dept_code', dept_code);
            Cookies.remove('STDCK_group_code');
            Cookies.remove('STDCK_term');
            select2group_reset(dept_code); 
            LoadDataStudentSchedule(dept_code,null,null);
        }else{
            // $(".schedule").load("ScheduleSampleTable.php");
            Cookies.remove('STDCK_dept_code');
            Cookies.remove('STDCK_group_code');
            Cookies.remove('STDCK_term');
            select_group();
            select_year("NotSelect");
            LoadDataStudentSchedule(null,null,null);
        }
    });

    $('#select_group').change(function () { 
        var dept_code = $('#select_dept').val();
        var group_code = $(this).val();
        if(group_code !== ""){
            Cookies.set('STDCK_group_code', group_code);
            Cookies.remove('STDCK_term');
            select2year_reset(group_code); 
            LoadDataStudentSchedule(dept_code,group_code,null);
        }else{
            // $(".schedule").load("ScheduleSampleTable.php");
            Cookies.remove('STDCK_group_code');
            Cookies.remove('STDCK_term');
            select_year();
            LoadDataStudentSchedule(dept_code,null,null);
        }
    });
    
    $('#select_year').change(function () { 
        var select_year = $(this).val();
        if(select_year !== ""){
            Cookies.set('STDCK_term', select_year);
            var dept_code = $('#select_dept').val();
            var group_code = $('#select_group').val();
            LoadDataStudentSchedule(dept_code,group_code,select_year);
        }else{
            // $(".schedule").load("ScheduleSampleTable.php");
            Cookies.remove('STDCK_term');
            LoadDataStudentSchedule(dept_code,group_code,null);
        }
    });
    
    $('.print-schedule').click(function (e) { 
        Cookies.set('staReport', 'StudentSchedule');
        window.open("Report.php", "_blank", "width=1000, height=700");
        // $(".schedule2 table").removeClass();
        // $(".schedule2 table").addClass("table table-bordered table-studentschedule text-center");
        
    });
});