<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite31c8d839e6d04daa040705208be9031
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite31c8d839e6d04daa040705208be9031::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite31c8d839e6d04daa040705208be9031::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
