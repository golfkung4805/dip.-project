<?php
    include('./include/DBConfig.SR.php');

    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    if($action==="GetRoom"){
        $sql = "SELECT * FROM tb_room";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        
        $GetRoom[] = array("id"=>"", "text"=>""); 

        foreach($stmt as $row){
            if($row['room_code']===$_POST['room_code']){
                $GetRoom[] = array("id"=>$row['room_code'], "text"=>$row['room_code']." | ".$row['room_name'], "selected"=>true); 
            }else{
                $GetRoom[] = array("id"=>$row['room_code'], "text"=>$row['room_code']." | ".$row['room_name']); 
            }
        }

        $stmt = null;
        
        echo json_encode($GetRoom,JSON_UNESCAPED_UNICODE);
        exit;
    }

    if($action==="GetYear"){
        $sql = "SELECT term 
                FROM tb_schedule 
                GROUP BY term";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $Output[] = array("id"=>"", "text"=>""); 
        foreach($stmt as $row){
            if($row['term']===$_POST['select_year']){
                $Output[] = array("id"=>$row['term'], "text"=>$row['term'], "selected"=>true); 
            }else{
                $Output[] = array("id"=>$row['term'], "text"=>$row['term']); 
            }
            
        }
        
        $stmt = null;

        echo json_encode($Output,JSON_UNESCAPED_UNICODE);
        exit;
    }
?>