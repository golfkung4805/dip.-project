    <div class="modal fade" id="modalTutorial">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">วิธีการใช้งานหน้าตารางเรียนนักศึกษา</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>วิธีใช้บล๊าๆๆ&hellip;</p>
                <p>วิธีใช้บล๊าๆๆ&hellip;</p>
                <p>วิธีใช้บล๊าๆๆ&hellip;</p>
                <p>วิธีใช้บล๊าๆๆ&hellip;</p>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default bg-maroon btn-never-show-tutorial">ปิดและไม่ต้องแสดงอีก <i class="fa fa-window-close"></i></button> -->
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด <i class="fa fa-times"></i></button>
            </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> ตารางเรียนนักศึกษา </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?php echo $_SERVER_NAME; ?>">Home</a></li>
                            <li class="breadcrumb-item active">ตารางเรียนนักศึกษา</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline card-primary schedule-select">
                            <div class="card-header">
                                <div class="row form-group m-0">
                                    <label class="col-lg-1 col-form-label text-right">แผนก</label>
                                    <div class="col-lg-3">
                                        <select class="form-control select_dept" id="select_dept" name="select_dept" style="width: 100%;" autofocus>
                                            
                                        </select>
                                    </div>
                                    
                                    <label class="col-lg-1 col-form-label text-right">กลุ่มเรียน</label>
                                    <div class="col-lg-3">
                                        <select class="form-control select_group" id="select_group" name="select_group" style="width: 100%;">
                                            
                                        </select>
                                    </div>

                                    <label class="col-lg-1 col-form-label text-right">ปีการศึกษา</label>
                                    <div class="col-lg-3">
                                        <select class="form-control select_year" id="select_year" name="select_year" style="width: 100%;">
                                            
                                        </select>
                                    </div>
                                </div>
                                <!-- /.card-tools -->
                            </div>
                            <!-- /.card-header -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-purple schedule">
                            <?php //include('StudentScheduleTable.php'); ?> 
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col-md-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card report">
                            <?php //include('StudentScheduleTable.php'); ?> 
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col-md-12 -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>