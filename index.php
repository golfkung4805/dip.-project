<?php
    // TODO เช็คค่า View ว่ามีการส่งมาไหม
    session_start();
    $view = ( isset($_GET['view']) && $_GET['view'] != "" )? $_GET['view'] : "" ;
    $_SESSION['$view'] = $view;
    $view = $_SESSION['$view'];
    switch ($view) {
        case 'TeacherSchedule':
            $Title = "ตารางสอน : MEC@SRMS";
            $MainContent = "TeacherSchedule.php";
            break;
        case 'StudentSchedule':
            $Title = "ตารางเรียน : MEC@SRMS";
            $MainContent = "StudentSchedule.php";
            break;
        case 'RoomSchedule':
            $Title = "ตารางการใช้ห้อง : MEC@SRMS";
            $MainContent = "RoomSchedule.php";
            break;
        case 'Login':
            $Title = "เข้าสู่ระบบ : MEC@SRMS";
            $MainContent = "Login.php";
            break;
        case 'ForgotPassword':
            $Title = "ลืมรหัสผ่าน : MEC@SRMS";
            $MainContent = "ForgotPassword.php";
            break;
        case 'RecoverPassword':
            $Title = "กู้คืนระหัสผ่าน : MEC@SRMS";
            $MainContent = "RecoverPassword.php";
            break;
        case 'Error401':
            $Title = "Error 401 : MEC@SRMS";
            $MainContent = "Error401.php";
            break;
        default:
            $view = "Home";
            $Title = "หน้าหลัก : MEC@SRMS";
            $MainContent = "Home.php";
            
    }

    if($_SERVER['SERVER_NAME'] === "www2.mec.ac.th"){
        $_SERVER_NAME = "http://www2.mec.ac.th/it008/";
    }else{
        $_SERVER_NAME = "http://localhost/SR/";
    }

    if($view==="Login" || $view==="ForgotPassword" || $view==="RecoverPassword" || $view==="Error401"){
        include ('LFR.base.php');
    }else{
        include ('index.base.php');
    }
    
?>