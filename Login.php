    <div class="card">
        <div class="card-body login-card-body">
            <img src="dist/img/logo.png"width="80" class="mx-auto d-block">
            <p class="login-box-msg">เข้าสู่ระบบระบบ<br>จัดตารางสอนออนไลน์<br>วิทยาลัยการอาชีพนวมินทราชินีมุกดาหาร

            <form id="FormLogin">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" id="username" name="username" placeholder="รหัสอาจารย์">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" id="password" name="password" placeholder="รหัสผ่านอาจารย์">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-6">
                        <a class="btn btn-light btn-block" href="<?php echo $_SERVER_NAME; ?>">กลับหน้าหลัก</a>
                    </div>
                    <!-- /.col -->
                    <div class="col-1">

                    </div>
                    <!-- /.col -->
                    <div class="col-5">
                        <button type="submit" class="btn btn-primary btn-block">เข้าสู่ระบบ</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <p class="mb-1">
                <a href="<?php echo $_SERVER_NAME; ?>?view=ForgotPassword">ลืมรหัสผ่าน</a>
            </p>
            <!-- <p class="mb-1">
                <a type="submit" class="btn btn-outline-primary" href="<?php echo $_SERVER_NAME; ?>">กลับหน้าหลัก</a>
            </p> -->
        </div>
        <!-- /.login-card-body -->
    </div>