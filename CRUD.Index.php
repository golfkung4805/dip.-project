<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailerPHPMailer\Exception;

    include('./include/DBConfig.SR.php');
    
    function sendMail($email,$username,$name,$token){
        // Import PHPMailer classes into the global namespace
        // These must be at the top of your script, not inside a function
        // use PHPMailer\PHPMailer\PHPMailer;
        // use PHPMailer\PHPMailerPHPMailer\Exception;

        // require 'vendor/phpmailer/phpmailer/src/Exception.php';
        // require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
        // require 'vendor/phpmailer/phpmailer/src/SMTP.php';

        // Load Composer's autoloader
        require 'vendor/autoload.php';
        //Create a new PHPMailer instance

        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 0;
        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        $mail->Host = "smtp.gmail.com";
        //Set the SMTP port number - likely to be 25, 465 or 587
        $mail->Port = 587;
        //Set the encryption system to use - ssl (deprecated) or tls
        $mail->SMTPSecure = 'tls';
        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;
        //Username to use for SMTP authentication
        $mail->Username = "srms.send.mail1412@gmail.com";
        //Password to use for SMTP authentication
        $mail->Password = "mec@muk1412";
        //Set who the message is to be sent from
        $mail->setFrom('srms.send.mail1412@gmail.com', 'MEC@SRMS');
        //Set who the message is to be sent to
        $mail->addAddress($email, 'ถึงอาจารย์ '.$name);
        //Set the subject line
        $mail->Subject = 'Forgot Password : MEC@SRMS';
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        //$mail->msgHTML(file_get_contents('content.html'), dirname(__FILE__));
        $content = "ตั้งค่ารหัสผ่านใหม่<br/>";
        $content .= "ระบบจัดตารางสอนออนไลน์ วิทยาลัยการอาชีพนวมินทราชินีมุกดาหาร<br/>";
        $content .= "รหัสอาจารย์(ชื่อผู้ใช้) : ".$username." <br/>";
        $content .= "ชื่ออาจารย์ : ".$name." <br/>";
        $content .= "ตั้งค่ารหัสผ่านใหม่ของอาจารย์ อาจารย์ต้องเข้าที่ลิงค์ต่อไปนี้ <br/>";
        $content .= "<a href=\"http://localhost/SR/?view=RecoverPassword&token=$token\">คลิกที่นี่ </a> หรือ <a href=\"http://localhost/SR/?view=RecoverPassword&token=$token\">http://localhost/SR/?view=RecoverPassword&token=$token</a> <br/>";
        $content .= "ถ้านี้เป็นความผิดพลาด ก็ไม่ต้องสนใจอีเมลนี้และจะไม่มีอะไรเกิดขึ้น<br/>";
        $content .= "อีเมลฉบับนี้ส่งจากระบบอัตโนมัติ กรุณาอย่าตอบกลับ";
        $mail->msgHTML($content);

        //send the message, check for errors
        if (!$mail->send()) {
            return array("type"=>"error", "title"=>"เกิดข้อผิดพลาด ".$mail->ErrorInfo);
        } else {
            return array("type"=>"success", "title"=>"ส่งอีเมลสำเร็จ กรุณาตรวจสอบอยู่กล่องข้อความ");
        }
    }


    if(isset($_POST['action'])){
        $action = $_POST['action'];
    }else{
        $action = null;
    }

    if($action === "Login"){
        $username = filter_input(INPUT_POST,'username');
        $password = filter_input(INPUT_POST,'password');

        $sql = "SELECT * FROM tb_admin 
                WHERE username = :username AND password = :password";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('username',$username,PDO::PARAM_STR);
        $stmt->bindValue('password',md5($password),PDO::PARAM_STR);
        $stmt->execute();
        if($stmt->rowCount() > 0){
            $result = $stmt->fetch();
            $stmt = null;
            $_SESSION['teacher_id'] = $result['admin_id'];
            $_SESSION['gender'] = $result['gender'];
            $_SESSION['first_name'] = $result['first_name'];
            $_SESSION['last_name'] = $result['last_name'];
            $_SESSION['dept_code'] = "ADMIN";
            $_SESSION['Role'] = "ADMIN";
            echo json_encode($result,JSON_UNESCAPED_UNICODE);
            exit;
        }else{
            $sql = "SELECT * FROM tb_teacher 
                    WHERE teacher_code = :username";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('username',$username,PDO::PARAM_STR);
            $stmt->execute();
            if($stmt->rowCount() > 0){
                $sql = "SELECT *,(SELECT teacher_code FROM tb_dept WHERE teacher_code = :checkRole )as Role FROM tb_teacher WHERE teacher_code = :username AND teacher_password = :password";
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue('checkRole',$username,PDO::PARAM_STR);
                $stmt->bindValue('username',$username,PDO::PARAM_STR);
                $stmt->bindValue('password',$password,PDO::PARAM_STR);
                $stmt->execute();
                if($stmt->rowCount() > 0){
                    $result = $stmt->fetch();
                    $stmt = null;
                    $_SESSION['teacher_id'] = $result['teacher_id'];
                    $_SESSION['gender'] = $result['gender'];
                    $_SESSION['first_name'] = $result['first_name'];
                    $_SESSION['last_name'] = $result['last_name'];
                    $_SESSION['dept_code'] = $result['dept_code'];
                    if($result['Role'] === null){
                        $_SESSION['Role'] = "Teacher";
                    }else{
                        $_SESSION['Role'] = "ChiefDEP";
                    }
                    echo json_encode($result,JSON_UNESCAPED_UNICODE);
                    exit;
                }else{
                    $alert = array("type"=>"warning", "title"=>"รหัสผ่านผิดกรุณากรอกใหม่อีกครั้ง", "status"=>"password"); 
                    echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                    exit;
                }
            }else{
                $alert = array("type"=>"warning", "title"=>"ไม่พบรหัสอาจารย์ท่านนี้กรุณากรอกให้อีกครั้ง", "status"=>"username"); 
                echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
                exit;
            }
        }
    }

    if($action === "ForgotPassword"){
        $username = filter_input(INPUT_POST,'username');
        $email = filter_input(INPUT_POST,'email');

        $sql = "SELECT * FROM tb_teacher 
                WHERE teacher_code = :username";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('username',$username,PDO::PARAM_STR);
        $stmt->execute();
        if($stmt->rowCount() > 0){
            $data = $stmt->fetch();
            $name = $data['first_name']." ".$data['last_name'];
            $username = $data['teacher_code'];
            $token = md5($data['teacher_code'].date("Y-m-d H:i:s"));

            $sql = "UPDATE tb_teacher SET  teacher_token = :teacher_token
                    WHERE teacher_code = :teacher_code";
            $stmt = $pdo->prepare($sql);
            $stmt->bindValue('teacher_token',$token,PDO::PARAM_INT);
            $stmt->bindValue('teacher_code',$username,PDO::PARAM_STR);
            if($stmt->execute()){
                $alert = sendMail($email,$username,$name,$token);
            }


            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $alert = array("type"=>"warning", "title"=>"ไม่พบรหัสอาจารย์ท่านนี้"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }

    if($action === "GetRecoverPassword"){
        $token = filter_input(INPUT_POST,'token');

        $sql = "SELECT * FROM tb_teacher
                WHERE teacher_token = :teacher_token";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('teacher_token',$token,PDO::PARAM_STR);
        $stmt->execute();
        if($stmt->rowCount() > 0){
            $data = $stmt->fetch();
            $alert = array("type"=>"success", "title"=>"แก้ไขรหัสผ่านสำเร็จ"); 
            echo json_encode(array("alert" => $alert,"data" => $data),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }

    if($action === "RecoverPassword"){
        $teacher_code = filter_input(INPUT_POST,'username');
        $token = filter_input(INPUT_POST,'token');
        $confirm_password = filter_input(INPUT_POST,'confirm_password');

        $sql = "UPDATE tb_teacher SET teacher_password = :teacher_password, teacher_token = NULL
                WHERE teacher_token = :teacher_token";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue('teacher_password',$confirm_password,PDO::PARAM_STR);
        $stmt->bindValue('teacher_token',$token,PDO::PARAM_STR);
        if($stmt->execute()){
            $alert = array("type"=>"success", "title"=>"เปลี่ยนรหัสผ่านสำเร็จ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }else{
            $alert = array("type"=>"error", "title"=>"เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ"); 
            echo json_encode(array("alert" => $alert),JSON_UNESCAPED_UNICODE ); 
            exit;
        }
    }

?>