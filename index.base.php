<?php
    if (!isset($_SESSION)) session_start();
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> <?php echo $Title; ?> </title>

    <!-- Favicon -->
    <link rel="icon" href="dist/img/AdminLTELogo.png" type="image/x-icon">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- pace-progress -->
    <link rel="stylesheet" href="plugins/pace-progress/themes/black/pace-theme-flat-top.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,400i,700&display=swap" rel="stylesheet">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- My Style CSS -->
    <link rel="stylesheet" href="dist/css/SR.CSS.css">
    <link rel="stylesheet" href="dist/css/ScrollBar.css">
    <link rel="stylesheet" href="dist/css/SR.HOME.css">
    <?php include("dist/css/SR.CSS.php"); ?>
    <style>
            .activity {
            background-color: #C6EEC3;
        }

        .danger {
            background-color: #ff6a6a;
        }

        table {
            border-collapse: collapse;
            text-align: center;
            vertical-align: middle !important;
        }

        .report-schedule, .report-schedule th, .report-schedule td ,
        .report-detail-subject, .report-detail-subject th, .report-detail-subject td {
            border: 1px solid black;
        }

        .report-schedule, .report-schedule th {
            padding: 5px;
        }

        .report-schedule, .report-schedule td {
            height: 61px;
        }

        .report-detail-subject td{
            padding: 5px;
        }
        @media only screen and (min-width: 768px) {
            .print-schedule{
                bottom: 5.5rem;
                right: 2rem;
                margin-bottom: 100px;
            }
            .btn-show-tutorial{
                margin-bottom: 50px;
            }
        }

        @media only screen and (max-width: 767px) {
            .print-schedule{
                margin-bottom: 50px;
            }
        }
        
        @media print{
            @page{
                /* size: landscape;
                margin: 0; */
                size: 16.5in 11.7in;
                margin-top: 0cm;
                margin-right: .5cm;
                margin-bottom: 0;
                margin-left: .5cm;
                
            }
            body { margin-top: .5cm; font-size: 16px;}
            /* .report{ display: none; } */

            .schedule-select{ display: none; }
            .schedule{ display: none; }
            .print-schedule{ display: none; }
            .main-footer { display: none; }
        }
        .print-schedule{
            bottom: 1.25rem;
            position: fixed;
            right: 1.25rem;
            z-index: 1032;
        }

        .btn-show-tutorial{
            bottom: 1.25rem;
            position: fixed;
            right: 1.25rem;
            z-index: 1032;
        }
    
    </style>
</head>

<body class="hold-transition layout-top-nav pace-primary">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
            <div class="container">
                <a href="<?php echo $_SERVER_NAME; ?>" class="navbar-brand">
                    <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">MEC@SRMS</span>
                </a>

                <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse order-3" id="navbarCollapse" style="font-family: 'Kanit', sans-serif;">
                    <!-- Left navbar links -->
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="<?php echo $_SERVER_NAME; ?>" class="nav-link">หน้าหลัก</a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=StudentSchedule" class="nav-link">ตารางเรียน</a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=TeacherSchedule" class="nav-link">ตารางสอน</a>
                        </li>
                        <li class="nav-item">
                            <a href="?view=RoomSchedule" class="nav-link">ตารางการใช้ห้อง</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <!-- <li class="nav-item">
                            <a href="<?php echo $_SERVER_NAME; ?>?view=Login" class="nav-link">เข้าสู่ระบบ</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo $_SERVER_NAME; ?>?view=ForgotPassword" class="nav-link">ลืมรหัสผ่าน</a>
                        </li> -->
                        <li class="nav-item dropdown">
                            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">ตัวเลือก</a>
                            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow dropdown-menu-right">
                                <li><a href="<?php echo $_SERVER_NAME; ?>?view=Login" class="dropdown-item">เข้าสู่ระบบ <i class="fa fa-sign-in-alt float-right" style="min-height: 100%;display: flex;align-items: center;"></i></a></li>
                                <li class="dropdown-divider"></li>
                                <li><a href="<?php echo $_SERVER_NAME; ?>?view=ForgotPassword" class="dropdown-item">ลืมรหัสผ่าน <i class="fa fa-question-circle float-right" style="min-height: 100%;display: flex;align-items: center;"></i></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
        </nav>
        <!-- /.navbar -->

        <!-- Content Wrapper. Contains page content -->
        <?php include( $MainContent ); ?>
        <!-- /.content-wrapper -->
        <a id="print-schedule" href="#" class="btn btn-primary print-schedule invisible">
            <i class="fas fa-print"></i>
        </a>
        <a id="print-schedule" href="#" class="btn btn-primary btn-show-tutorial">
            <i class="fa fa-list"> วิธีการใช้งาน</i>
        </a>
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                [MEC@SRMS] SIRI Management System | <!--<a href="https://github.com/ColorlibHQ/AdminLTE">Version 3.0.1</a>-->
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io/themes/dev/AdminLTE/">AdminLTE.io</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Cookie -->
    <script src="dist/js/js.cookie.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- pace-progress -->
    <script src="plugins/pace-progress/pace.min.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/js/select2.full.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- My Style JS -->
    <script src="dist/js/SR.JS.js"></script>
    <?php include("dist/js/SR.JS.php"); ?>
</body>

</html>